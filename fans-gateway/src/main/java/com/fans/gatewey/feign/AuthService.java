package com.fans.gatewey.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fans.gatewey.feign.fallback.AuthServiceFallbackImpl;
import com.fans.gatewey.util.AjaxResult;

/**
 * @ClassName: AuthService
 * @Description: jwt 认证
 * @author fanhaohao
 * @date 2020年1月30日 上午11:18:00
 */
@FeignClient(name = "fans-jwt-auth-provider", fallback = AuthServiceFallbackImpl.class)
public interface AuthService {
	/**
	 * token 校验
	 * 
	 * @param authToken tokenid
	 * @return AjaxResult
	 */
	@RequestMapping(value = { "/auth/checkToken" }, method = RequestMethod.POST)
	public AjaxResult checkToken(@RequestParam(value = "authToken") String authToken);

}
