package com.fans.gatewey.feign.fallback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.fans.gatewey.feign.AuthService;
import com.fans.gatewey.util.AjaxResult;

/**
 * @ClassName: AuthServiceFallbackImpl
 * @Description: jwt 认证服务的fallback
 * @author fanhaohao
 * @date 2020年1月30日 上午11:18:18
 */
@Service
public class AuthServiceFallbackImpl implements AuthService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public AjaxResult checkToken(@RequestParam(value = "authToken") String authToken) {
		logger.error("调用{}异常:{}", "checkToken", authToken);
		return AjaxResult.error();
	}


}
