package com.fans.gatewey.util;

import java.util.HashMap;

/**
 * 操作消息提醒
 * 
 * @author fanhaohao
 */
public class AjaxResult extends HashMap<String, Object>
{
    private static final long serialVersionUID = 1L;

    /**
     * 初始化一个新创建的 Message 对象
     */
    public AjaxResult()
    {
    }

    /**
     * 返回错误消息
     * 
     * @return 错误消息
     */
    public static AjaxResult error()
    {
        return error(1, "操作失败");
    }

    /**
     * 返回错误消息
     * 
     * @param msg 内容
     * @return 错误消息
     */
    public static AjaxResult error(String msg)
    {
        return error(500, msg);
    }

    /**
     * 返回错误消息
     * 
     * @param code 错误码
     * @param msg 内容
     * @return 错误消息
     */
    public static AjaxResult error(int code, String msg)
    {
        AjaxResult json = new AjaxResult();
        json.put("code", code);
        json.put("msg", msg);
        return json;
    }
    
    /**
     * @Title：successData  
     * @Description: 返回错误消息并携带数据
     * @author: fanhaohao
     * @date 2019年1月24日 下午9:47:06 
     * @param @param datas
     * @param @return 
     * @return AjaxResult 
     * @throws
     */
    public static AjaxResult errorData(Object datas) {
    	AjaxResult r = AjaxResult.error();
		r.put("datas",datas);
		return r;
	}

    /**
     * 返回成功消息
     * 
     * @param msg 内容
     * @return 成功消息
     */
    public static AjaxResult success(String msg)
    {
        AjaxResult json = new AjaxResult();
        json.put("msg", msg);
        json.put("code", 0);
        return json;
    }
    
    /**
     * 返回成功消息
     * 
     * @return 成功消息
     */
    public static AjaxResult success()
    {
        return AjaxResult.success("操作成功");
    }

    /**
     * @Title：successData  
     * @Description: 返回成功消息并携带数据
     * @author: fanhaohao
     * @date 2019年1月24日 下午9:47:06 
     * @param @param datas
     * @param @return 
     * @return AjaxResult 
     * @throws
     */
    public static AjaxResult successData(Object datas) {
    	AjaxResult r = AjaxResult.success();
		r.put("datas",datas);
		return r;
	}
    
    /**
     * 返回成功消息
     * 
     * @param key 键值
     * @param value 内容
     * @return 成功消息
     */
    @Override
    public AjaxResult put(String key, Object value)
    {
        super.put(key, value);
        return this;
    }
}
