/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50556
Source Host           : localhost:3306
Source Database       : fans_cloud

Target Server Type    : MYSQL
Target Server Version : 50556
File Encoding         : 65001

Date: 2020-04-18 20:41:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `fans_test_dict`
-- ----------------------------
DROP TABLE IF EXISTS `fans_test_dict`;
CREATE TABLE `fans_test_dict` (
  `id` varchar(36) NOT NULL COMMENT '字典id',
  `code` varchar(50) DEFAULT NULL COMMENT '编号',
  `name` varchar(50) NOT NULL COMMENT '名称',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父节点',
  `weight` int(11) DEFAULT NULL COMMENT '排序',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  KEY `AK_uk_dict` (`code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='测试表';

-- ----------------------------
-- Records of fans_test_dict
-- ----------------------------
INSERT INTO `fans_test_dict` VALUES ('2', 'sex', '性别', '0', null, null, null, null, null, null, '0');
INSERT INTO `fans_test_dict` VALUES ('3', '1', '男', '0', '1', null, null, '1', '2020-04-15 15:20:18', 'xxx', '0');
INSERT INTO `fans_test_dict` VALUES ('4', '2', '女', '0', '2', null, null, '1', '2020-04-15 15:20:38', 'xxx', '0');
INSERT INTO `fans_test_dict` VALUES ('5', '1', '可用', '0', null, null, null, '1', '2020-04-15 15:23:37', '', '0');
INSERT INTO `fans_test_dict` VALUES ('6', '2', '不可用', '0', '1', null, null, '1', '2020-04-15 15:23:48', 'sss', '0');

-- ----------------------------
-- Table structure for `oauth_access_token`
-- ----------------------------
DROP TABLE IF EXISTS `oauth_access_token`;
CREATE TABLE `oauth_access_token` (
  `token_id` varchar(256) DEFAULT NULL,
  `token` blob,
  `authentication_id` varchar(48) NOT NULL,
  `user_name` varchar(256) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  `authentication` blob,
  `refresh_token` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`authentication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oauth_access_token
-- ----------------------------
INSERT INTO `oauth_access_token` VALUES ('430bacf367268cc5e7ba9ff4d9367cc3', 0xACED0005737200436F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E636F6D6D6F6E2E44656661756C744F4175746832416363657373546F6B656E0CB29E361B24FACE0200064C00156164646974696F6E616C496E666F726D6174696F6E74000F4C6A6176612F7574696C2F4D61703B4C000A65787069726174696F6E7400104C6A6176612F7574696C2F446174653B4C000C72656672657368546F6B656E74003F4C6F72672F737072696E676672616D65776F726B2F73656375726974792F6F61757468322F636F6D6D6F6E2F4F417574683252656672657368546F6B656E3B4C000573636F706574000F4C6A6176612F7574696C2F5365743B4C0009746F6B656E547970657400124C6A6176612F6C616E672F537472696E673B4C000576616C756571007E000578707372001E6A6176612E7574696C2E436F6C6C656374696F6E7324456D7074794D6170593614855ADCE7D002000078707372000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000016847FBE234787372004C6F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E636F6D6D6F6E2E44656661756C744578706972696E674F417574683252656672657368546F6B656E2FDF47639DD0C9B70200014C000A65787069726174696F6E71007E0002787200446F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E636F6D6D6F6E2E44656661756C744F417574683252656672657368546F6B656E73E10E0A6354D45E0200014C000576616C756571007E0005787074002436333066646161632D313333392D343333352D623039312D3938323237663137323765317371007E000977080000016847FBE23078737200256A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C65536574801D92D18F9B80550200007872002C6A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C65436F6C6C656374696F6E19420080CB5EF71E0200014C0001637400164C6A6176612F7574696C2F436F6C6C656374696F6E3B7870737200176A6176612E7574696C2E4C696E6B656448617368536574D86CD75A95DD2A1E020000787200116A6176612E7574696C2E48617368536574BA44859596B8B7340300007870770C000000103F400000000000017400036170707874000662656172657274002461656563346436362D346230622D346431302D623066352D623835633934643832626638, 'b9cdd625a781099755dfb47c3de1e150', 'admin', 'app', 0xACED0005737200416F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E70726F76696465722E4F417574683241757468656E7469636174696F6EBD400B02166252130200024C000D73746F7265645265717565737474003C4C6F72672F737072696E676672616D65776F726B2F73656375726974792F6F61757468322F70726F76696465722F4F4175746832526571756573743B4C00127573657241757468656E7469636174696F6E7400324C6F72672F737072696E676672616D65776F726B2F73656375726974792F636F72652F41757468656E7469636174696F6E3B787200476F72672E737072696E676672616D65776F726B2E73656375726974792E61757468656E7469636174696F6E2E416273747261637441757468656E7469636174696F6E546F6B656ED3AA287E6E47640E0200035A000D61757468656E746963617465644C000B617574686F7269746965737400164C6A6176612F7574696C2F436F6C6C656374696F6E3B4C000764657461696C737400124C6A6176612F6C616E672F4F626A6563743B787000737200266A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C654C697374FC0F2531B5EC8E100200014C00046C6973747400104C6A6176612F7574696C2F4C6973743B7872002C6A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C65436F6C6C656374696F6E19420080CB5EF71E0200014C00016371007E00047870737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A65787000000002770400000002737200426F72672E737072696E676672616D65776F726B2E73656375726974792E636F72652E617574686F726974792E53696D706C654772616E746564417574686F7269747900000000000001A40200014C0004726F6C657400124C6A6176612F6C616E672F537472696E673B7870740009524F4C455F555345527371007E000D74000B5355525045525F555345527871007E000C707372003A6F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E70726F76696465722E4F41757468325265717565737400000000000000010200075A0008617070726F7665644C000B617574686F72697469657371007E00044C000A657874656E73696F6E7374000F4C6A6176612F7574696C2F4D61703B4C000B726564697265637455726971007E000E4C00077265667265736874003B4C6F72672F737072696E676672616D65776F726B2F73656375726974792F6F61757468322F70726F76696465722F546F6B656E526571756573743B4C000B7265736F7572636549647374000F4C6A6176612F7574696C2F5365743B4C000D726573706F6E7365547970657371007E0016787200386F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E70726F76696465722E426173655265717565737436287A3EA37169BD0200034C0008636C69656E74496471007E000E4C001172657175657374506172616D657465727371007E00144C000573636F706571007E00167870740003617070737200256A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C654D6170F1A5A8FE74F507420200014C00016D71007E00147870737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F400000000000017708000000020000000174000A6772616E745F7479706574000870617373776F726478737200256A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C65536574801D92D18F9B80550200007871007E0009737200176A6176612E7574696C2E4C696E6B656448617368536574D86CD75A95DD2A1E020000787200116A6176612E7574696C2E48617368536574BA44859596B8B7340300007870770C000000103F4000000000000174000361707078017371007E0023770C000000103F40000000000000787371007E001C3F40000000000000770800000010000000007870707371007E0023770C000000103F40000000000000787371007E0023770C000000103F40000000000000787372004F6F72672E737072696E676672616D65776F726B2E73656375726974792E61757468656E7469636174696F6E2E557365726E616D6550617373776F726441757468656E7469636174696F6E546F6B656E00000000000001A40200024C000B63726564656E7469616C7371007E00054C00097072696E636970616C71007E00057871007E0003017371007E00077371007E000B0000000277040000000271007E000F71007E00117871007E002D7070737200326F72672E737072696E676672616D65776F726B2E73656375726974792E636F72652E7573657264657461696C732E5573657200000000000001A40200075A00116163636F756E744E6F6E457870697265645A00106163636F756E744E6F6E4C6F636B65645A001563726564656E7469616C734E6F6E457870697265645A0007656E61626C65644C000B617574686F72697469657371007E00164C000870617373776F726471007E000E4C0008757365726E616D6571007E000E7870010101017371007E0020737200116A6176612E7574696C2E54726565536574DD98509395ED875B0300007870737200466F72672E737072696E676672616D65776F726B2E73656375726974792E636F72652E7573657264657461696C732E5573657224417574686F72697479436F6D70617261746F7200000000000001A4020000787077040000000271007E000F71007E0011787074000561646D696E, 'b2516de0d6851ddf31ee41c21a1a3f89');

-- ----------------------------
-- Table structure for `oauth_approvals`
-- ----------------------------
DROP TABLE IF EXISTS `oauth_approvals`;
CREATE TABLE `oauth_approvals` (
  `userId` varchar(256) DEFAULT NULL,
  `clientId` varchar(256) DEFAULT NULL,
  `scope` varchar(256) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `expiresAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lastModifiedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oauth_approvals
-- ----------------------------

-- ----------------------------
-- Table structure for `oauth_client_details`
-- ----------------------------
DROP TABLE IF EXISTS `oauth_client_details`;
CREATE TABLE `oauth_client_details` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `client_id` varchar(48) NOT NULL COMMENT '客户端ID',
  `resource_ids` varchar(256) DEFAULT NULL COMMENT '资源id集合',
  `client_secret` varchar(256) DEFAULT NULL COMMENT '客户端(client)的访问密匙',
  `scope` varchar(256) DEFAULT NULL COMMENT '客户端申请的权限范围',
  `authorized_grant_types` varchar(256) DEFAULT NULL COMMENT '指定客户端支持的grant_type,可选值包括authorization_code,password,refresh_token,implicit,client_credentials',
  `web_server_redirect_uri` varchar(256) DEFAULT NULL COMMENT '客户端的重定向URI',
  `authorities` varchar(256) DEFAULT NULL COMMENT '指定客户端所拥有的Spring Security的权限值',
  `access_token_validity` int(11) DEFAULT NULL COMMENT '设定客户端的access_token的有效时间值',
  `refresh_token_validity` int(11) DEFAULT NULL COMMENT '设定客户端的refresh_token的有效时间值',
  `additional_information` varchar(4096) DEFAULT NULL COMMENT '预留字段',
  `autoapprove` varchar(256) DEFAULT NULL COMMENT '设置用户是否自动Approval操作, 默认值为 ''false'', 可选值包括 ''true'',''false'', ''read'',''write''. ',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户端详情';

-- ----------------------------
-- Records of oauth_client_details
-- ----------------------------
INSERT INTO `oauth_client_details` VALUES ('1', 'app', '', 'app', 'app', 'password,refresh_token', '', '', '180000', '180000', '{}', 'true', null, null, '1', '2019-01-11 16:00:46', null, '0');
INSERT INTO `oauth_client_details` VALUES ('2', 'mobile', 'mobile,test', 'mobile', 'all', 'password,refresh_token', null, null, '180000', '180000', '{}', 'true', null, null, null, null, null, '0');
INSERT INTO `oauth_client_details` VALUES ('4', 'webApp', null, 'webApp', 'app', 'authorization_code,password,refresh_token,client_credentials', null, null, '180000', '180000', '{}', 'true', null, null, null, null, null, '0');
INSERT INTO `oauth_client_details` VALUES ('52a4492c19234f7a822867e2b87f32b9', 'fhh', null, 'fhh', 'fhh', 'password,refresh_token', 'http://www.baidu.com', null, '999999999', '2592000', '1', 'true', '1', '2019-01-11 21:17:18', '1', '2019-01-11 22:08:33', null, '0');
INSERT INTO `oauth_client_details` VALUES ('6', 'owen', null, 'owen', 'app', 'authorization_code,password,refresh_token,client_credentials,implicit', 'http://127.0.0.1:9997/clientOne/login', null, '180000', '180000', '{}', 'true', null, null, '1', '2019-01-11 21:53:30', null, '0');

-- ----------------------------
-- Table structure for `oauth_client_token`
-- ----------------------------
DROP TABLE IF EXISTS `oauth_client_token`;
CREATE TABLE `oauth_client_token` (
  `token_id` varchar(256) DEFAULT NULL,
  `token` blob,
  `authentication_id` varchar(48) NOT NULL,
  `user_name` varchar(256) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`authentication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oauth_client_token
-- ----------------------------

-- ----------------------------
-- Table structure for `oauth_code`
-- ----------------------------
DROP TABLE IF EXISTS `oauth_code`;
CREATE TABLE `oauth_code` (
  `code` varchar(256) DEFAULT NULL,
  `authentication` blob
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oauth_code
-- ----------------------------

-- ----------------------------
-- Table structure for `oauth_refresh_token`
-- ----------------------------
DROP TABLE IF EXISTS `oauth_refresh_token`;
CREATE TABLE `oauth_refresh_token` (
  `token_id` varchar(256) DEFAULT NULL,
  `token` blob,
  `authentication` blob
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oauth_refresh_token
-- ----------------------------
INSERT INTO `oauth_refresh_token` VALUES ('b2516de0d6851ddf31ee41c21a1a3f89', 0xACED00057372004C6F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E636F6D6D6F6E2E44656661756C744578706972696E674F417574683252656672657368546F6B656E2FDF47639DD0C9B70200014C000A65787069726174696F6E7400104C6A6176612F7574696C2F446174653B787200446F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E636F6D6D6F6E2E44656661756C744F417574683252656672657368546F6B656E73E10E0A6354D45E0200014C000576616C75657400124C6A6176612F6C616E672F537472696E673B787074002436333066646161632D313333392D343333352D623039312D3938323237663137323765317372000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000016847FBE23078, 0xACED0005737200416F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E70726F76696465722E4F417574683241757468656E7469636174696F6EBD400B02166252130200024C000D73746F7265645265717565737474003C4C6F72672F737072696E676672616D65776F726B2F73656375726974792F6F61757468322F70726F76696465722F4F4175746832526571756573743B4C00127573657241757468656E7469636174696F6E7400324C6F72672F737072696E676672616D65776F726B2F73656375726974792F636F72652F41757468656E7469636174696F6E3B787200476F72672E737072696E676672616D65776F726B2E73656375726974792E61757468656E7469636174696F6E2E416273747261637441757468656E7469636174696F6E546F6B656ED3AA287E6E47640E0200035A000D61757468656E746963617465644C000B617574686F7269746965737400164C6A6176612F7574696C2F436F6C6C656374696F6E3B4C000764657461696C737400124C6A6176612F6C616E672F4F626A6563743B787000737200266A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C654C697374FC0F2531B5EC8E100200014C00046C6973747400104C6A6176612F7574696C2F4C6973743B7872002C6A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C65436F6C6C656374696F6E19420080CB5EF71E0200014C00016371007E00047870737200136A6176612E7574696C2E41727261794C6973747881D21D99C7619D03000149000473697A65787000000002770400000002737200426F72672E737072696E676672616D65776F726B2E73656375726974792E636F72652E617574686F726974792E53696D706C654772616E746564417574686F7269747900000000000001A40200014C0004726F6C657400124C6A6176612F6C616E672F537472696E673B7870740009524F4C455F555345527371007E000D74000B5355525045525F555345527871007E000C707372003A6F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E70726F76696465722E4F41757468325265717565737400000000000000010200075A0008617070726F7665644C000B617574686F72697469657371007E00044C000A657874656E73696F6E7374000F4C6A6176612F7574696C2F4D61703B4C000B726564697265637455726971007E000E4C00077265667265736874003B4C6F72672F737072696E676672616D65776F726B2F73656375726974792F6F61757468322F70726F76696465722F546F6B656E526571756573743B4C000B7265736F7572636549647374000F4C6A6176612F7574696C2F5365743B4C000D726573706F6E7365547970657371007E0016787200386F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E70726F76696465722E426173655265717565737436287A3EA37169BD0200034C0008636C69656E74496471007E000E4C001172657175657374506172616D657465727371007E00144C000573636F706571007E00167870740003617070737200256A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C654D6170F1A5A8FE74F507420200014C00016D71007E00147870737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F400000000000017708000000020000000174000A6772616E745F7479706574000870617373776F726478737200256A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C65536574801D92D18F9B80550200007871007E0009737200176A6176612E7574696C2E4C696E6B656448617368536574D86CD75A95DD2A1E020000787200116A6176612E7574696C2E48617368536574BA44859596B8B7340300007870770C000000103F4000000000000174000361707078017371007E0023770C000000103F40000000000000787371007E001C3F40000000000000770800000010000000007870707371007E0023770C000000103F40000000000000787371007E0023770C000000103F40000000000000787372004F6F72672E737072696E676672616D65776F726B2E73656375726974792E61757468656E7469636174696F6E2E557365726E616D6550617373776F726441757468656E7469636174696F6E546F6B656E00000000000001A40200024C000B63726564656E7469616C7371007E00054C00097072696E636970616C71007E00057871007E0003017371007E00077371007E000B0000000277040000000271007E000F71007E00117871007E002D7070737200326F72672E737072696E676672616D65776F726B2E73656375726974792E636F72652E7573657264657461696C732E5573657200000000000001A40200075A00116163636F756E744E6F6E457870697265645A00106163636F756E744E6F6E4C6F636B65645A001563726564656E7469616C734E6F6E457870697265645A0007656E61626C65644C000B617574686F72697469657371007E00164C000870617373776F726471007E000E4C0008757365726E616D6571007E000E7870010101017371007E0020737200116A6176612E7574696C2E54726565536574DD98509395ED875B0300007870737200466F72672E737072696E676672616D65776F726B2E73656375726974792E636F72652E7573657264657461696C732E5573657224417574686F72697479436F6D70617261746F7200000000000001A4020000787077040000000271007E000F71007E0011787074000561646D696E);

-- ----------------------------
-- Table structure for `qrtz_blob_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `BLOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_calendars`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `CALENDAR_NAME` varchar(200) NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`CALENDAR_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_cron_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `CRON_EXPRESSION` varchar(200) NOT NULL,
  `TIME_ZONE_ID` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('fansScheduler', 'fca5c473-b4a9-455c-bfee-37f19e3a39e0', 'DEFAULT', '0 13 15 13 * ? 2018-2020', 'GMT+08:00');

-- ----------------------------
-- Table structure for `qrtz_fired_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `ENTRY_ID` varchar(95) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `SCHED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) NOT NULL,
  `JOB_NAME` varchar(200) DEFAULT NULL,
  `JOB_GROUP` varchar(200) DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`ENTRY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_job_details`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) NOT NULL,
  `IS_DURABLE` varchar(1) NOT NULL,
  `IS_NONCONCURRENT` varchar(1) NOT NULL,
  `IS_UPDATE_DATA` varchar(1) NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) NOT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('fansScheduler', 'fca5c473-b4a9-455c-bfee-37f19e3a39e0', 'DEFAULT', null, 'com.fans.admin.quartz.job.QuartzJobFactory', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787000737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F40000000000010770800000010000000007800);

-- ----------------------------
-- Table structure for `qrtz_locks`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `LOCK_NAME` varchar(40) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`LOCK_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('fansScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for `qrtz_paused_trigger_grps`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_scheduler_state`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`INSTANCE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_simple_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_simprop_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `STR_PROP_1` varchar(512) DEFAULT NULL,
  `STR_PROP_2` varchar(512) DEFAULT NULL,
  `STR_PROP_3` varchar(512) DEFAULT NULL,
  `INT_PROP_1` int(11) DEFAULT NULL,
  `INT_PROP_2` int(11) DEFAULT NULL,
  `LONG_PROP_1` bigint(20) DEFAULT NULL,
  `LONG_PROP_2` bigint(20) DEFAULT NULL,
  `DEC_PROP_1` decimal(13,4) DEFAULT NULL,
  `DEC_PROP_2` decimal(13,4) DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) NOT NULL,
  `TRIGGER_TYPE` varchar(8) NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) DEFAULT NULL,
  `MISFIRE_INSTR` smallint(2) DEFAULT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `SCHED_NAME` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `qrtz_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('fansScheduler', 'fca5c473-b4a9-455c-bfee-37f19e3a39e0', 'DEFAULT', 'fca5c473-b4a9-455c-bfee-37f19e3a39e0', 'DEFAULT', null, '1589353980000', '1586851113461', '5', 'WAITING', 'CRON', '1547629192000', '0', null, '0', '');

-- ----------------------------
-- Table structure for `quartz_schedule_job`
-- ----------------------------
DROP TABLE IF EXISTS `quartz_schedule_job`;
CREATE TABLE `quartz_schedule_job` (
  `id` varchar(36) NOT NULL COMMENT '表ID',
  `job_name` varchar(255) NOT NULL COMMENT '任务名称',
  `job_status` varchar(20) NOT NULL DEFAULT '1' COMMENT '任务状态（0：禁用；1：启用；2：删除）',
  `cron_expression` varchar(100) NOT NULL COMMENT '任务运行时间表达式',
  `job_class_is_bean_name` smallint(1) NOT NULL DEFAULT '0' COMMENT '指定jobClass是否是Spring Bean；（0：否；1：是）',
  `job_class` varchar(255) NOT NULL COMMENT '任务执行的类名或者Bean名',
  `job_method` varchar(255) NOT NULL COMMENT '任务执行的方法',
  `job_exec_count` bigint(20) DEFAULT '0' COMMENT '任务执行总数',
  `job_used_time` bigint(20) DEFAULT '0' COMMENT '任务执行耗时，毫秒',
  `job_exception_count` bigint(20) DEFAULT '0' COMMENT '任务异常总数',
  `last_exec_time` datetime DEFAULT NULL COMMENT '最后执行时间',
  `next_exec_time` datetime DEFAULT NULL COMMENT '下次执行时间',
  `status` varchar(100) DEFAULT NULL COMMENT '任务执行状态',
  `last_exception_time` datetime DEFAULT NULL COMMENT '最后异常时间',
  `remarks` varchar(1000) DEFAULT NULL COMMENT '任务描述',
  `create_by` varchar(60) DEFAULT NULL COMMENT '创建人ID',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(60) DEFAULT NULL COMMENT '最后修改人ID',
  `update_date` datetime DEFAULT NULL COMMENT '最后修改时间',
  `del_flag` char(2) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT ' 删除标记（0：正常；1：删除）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='定时任务表';

-- ----------------------------
-- Records of quartz_schedule_job
-- ----------------------------
INSERT INTO `quartz_schedule_job` VALUES ('01b4108410ad4f88a54ad689a576c09a', 't', '1', '* * * * * ? *', '0', 't', 't', '7', '0', '7', '2020-01-17 13:41:22', '2020-01-17 13:41:23', '0', '2020-01-17 13:41:22', null, '1', '2020-01-17 13:41:17', '1', '2020-01-17 13:41:22', '1');
INSERT INTO `quartz_schedule_job` VALUES ('8d960a318b6545e386078d433082a0ec', '552', '1', '3/25 * * * * ? *', '0', '552', '552', '2', '0', '2', '2020-01-17 13:42:07', '2020-01-17 13:42:28', '0', '2020-01-17 13:42:07', null, '1', '2020-01-17 13:41:57', '1', '2020-01-17 13:42:19', '1');
INSERT INTO `quartz_schedule_job` VALUES ('fca5c473-b4a9-455c-bfee-37f19e3a39e0', 'SpringQtz', '1', '0 13 15 13 * ? 2018-2020', '0', 'qzTest', 'execute', '2406', '18', '37', '2020-04-14 15:58:37', '2020-05-13 15:13:00', '0', '2017-10-18 10:31:05', '测试', '1', '2019-01-16 16:21:18', '4028802f3fe01630013fe67349050002', '2020-04-14 15:58:37', '0');

-- ----------------------------
-- Table structure for `quartz_schedule_log`
-- ----------------------------
DROP TABLE IF EXISTS `quartz_schedule_log`;
CREATE TABLE `quartz_schedule_log` (
  `id` varchar(60) NOT NULL COMMENT '表ID',
  `schedule_job_id` varchar(60) NOT NULL COMMENT '任务表ID',
  `exec_time` datetime NOT NULL COMMENT '执行时间（创建时间）',
  `end_time` datetime NOT NULL COMMENT '执行结束时间',
  `status` varchar(100) NOT NULL COMMENT '状态（SUCCESS、FAIL等）',
  `exception_stack_trace` varchar(4000) DEFAULT NULL COMMENT '异常文本',
  `remarks` varchar(1000) DEFAULT NULL COMMENT '任务描述',
  `create_by` varchar(60) DEFAULT NULL COMMENT '创建人ID',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(60) DEFAULT NULL COMMENT '最后修改人ID',
  `update_date` datetime DEFAULT NULL COMMENT '最后修改时间',
  `del_flag` char(2) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT ' 删除标记（0：正常；1：删除）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='定时任务执行日志表';

-- ----------------------------
-- Records of quartz_schedule_log
-- ----------------------------
INSERT INTO `quartz_schedule_log` VALUES ('0092f680f2bb4ab1abc88b29c5688a91', 'fca5c473-b4a9-455c-bfee-37f19e3a39e0', '2019-01-15 17:37:57', '2019-01-15 17:37:57', '1', null, null, null, '2019-01-15 17:37:57', null, '2019-01-15 17:37:57', '0');
INSERT INTO `quartz_schedule_log` VALUES ('125986df1d884735ab07c78370d674e5', '01b4108410ad4f88a54ad689a576c09a', '2020-01-17 13:41:21', '2020-01-17 13:41:21', '2', 'org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named \'t\' available\r\n	at org.springframework.beans.factory.support.DefaultListableBeanFactory.getBeanDefinition(DefaultListableBeanFactory.java:772)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.getMergedLocalBeanDefinition(AbstractBeanFactory.java:1221)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:294)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:273)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:199)\r\n	at org.springframework.context.support.AbstractApplicationContext.getBean(AbstractApplicationContext.java:1083)\r\n	at com.fans.common.utils.SpringUtil.getBean(SpringUtil.java:44)\r\n	at com.fans.admin.quartz.job.QuartzJobFactory.execute(QuartzJobFactory.java:76)\r\n	at com.fans.admin.quartz.job.QuartzJobFactory.execute(QuartzJobFactory.java:111)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\n', null, null, '2020-01-17 13:41:21', null, '2020-01-17 13:41:21', '0');
INSERT INTO `quartz_schedule_log` VALUES ('1dd7bb7829eb487fb0ae8b1b00dec6c5', 'fca5c473-b4a9-455c-bfee-37f19e3a39e0', '2020-01-13 15:13:00', '2020-01-13 15:13:00', '1', null, null, null, '2020-01-13 15:13:00', null, '2020-01-13 15:13:00', '0');
INSERT INTO `quartz_schedule_log` VALUES ('4603204000414219b98e1bbdb464fb01', '01b4108410ad4f88a54ad689a576c09a', '2020-01-17 13:41:18', '2020-01-17 13:41:18', '2', 'org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named \'t\' available\r\n	at org.springframework.beans.factory.support.DefaultListableBeanFactory.getBeanDefinition(DefaultListableBeanFactory.java:772)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.getMergedLocalBeanDefinition(AbstractBeanFactory.java:1221)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:294)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:273)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:199)\r\n	at org.springframework.context.support.AbstractApplicationContext.getBean(AbstractApplicationContext.java:1083)\r\n	at com.fans.common.utils.SpringUtil.getBean(SpringUtil.java:44)\r\n	at com.fans.admin.quartz.job.QuartzJobFactory.execute(QuartzJobFactory.java:76)\r\n	at com.fans.admin.quartz.job.QuartzJobFactory.execute(QuartzJobFactory.java:111)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\n', null, null, '2020-01-17 13:41:18', null, '2020-01-17 13:41:18', '0');
INSERT INTO `quartz_schedule_log` VALUES ('477766fc991d47a2bfeeede96206dcac', 'fca5c473-b4a9-455c-bfee-37f19e3a39e0', '2020-02-20 16:35:58', '2020-02-20 16:35:58', '1', null, null, null, '2020-02-20 16:35:58', null, '2020-02-20 16:35:58', '0');
INSERT INTO `quartz_schedule_log` VALUES ('524531fc6a85484999845de9060efde5', '01b4108410ad4f88a54ad689a576c09a', '2020-01-17 13:41:20', '2020-01-17 13:41:20', '2', 'org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named \'t\' available\r\n	at org.springframework.beans.factory.support.DefaultListableBeanFactory.getBeanDefinition(DefaultListableBeanFactory.java:772)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.getMergedLocalBeanDefinition(AbstractBeanFactory.java:1221)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:294)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:273)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:199)\r\n	at org.springframework.context.support.AbstractApplicationContext.getBean(AbstractApplicationContext.java:1083)\r\n	at com.fans.common.utils.SpringUtil.getBean(SpringUtil.java:44)\r\n	at com.fans.admin.quartz.job.QuartzJobFactory.execute(QuartzJobFactory.java:76)\r\n	at com.fans.admin.quartz.job.QuartzJobFactory.execute(QuartzJobFactory.java:111)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\n', null, null, '2020-01-17 13:41:20', null, '2020-01-17 13:41:20', '0');
INSERT INTO `quartz_schedule_log` VALUES ('60a1c1bb01a641ebadb54eab90d380e3', 'fca5c473-b4a9-455c-bfee-37f19e3a39e0', '2020-01-17 14:00:26', '2020-01-17 14:00:26', '1', null, null, null, '2020-01-17 14:00:26', null, '2020-01-17 14:00:26', '0');
INSERT INTO `quartz_schedule_log` VALUES ('60b60ac4b3e149b4af109d9360d029d4', 'fca5c473-b4a9-455c-bfee-37f19e3a39e0', '2020-04-14 15:58:37', '2020-04-14 15:58:37', '1', null, null, null, '2020-04-14 15:58:37', null, '2020-04-14 15:58:37', '0');
INSERT INTO `quartz_schedule_log` VALUES ('6a61da7980e74813b957cdd39e2f6e5f', '01b4108410ad4f88a54ad689a576c09a', '2020-01-17 13:41:22', '2020-01-17 13:41:22', '2', 'org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named \'t\' available\r\n	at org.springframework.beans.factory.support.DefaultListableBeanFactory.getBeanDefinition(DefaultListableBeanFactory.java:772)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.getMergedLocalBeanDefinition(AbstractBeanFactory.java:1221)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:294)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:273)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:199)\r\n	at org.springframework.context.support.AbstractApplicationContext.getBean(AbstractApplicationContext.java:1083)\r\n	at com.fans.common.utils.SpringUtil.getBean(SpringUtil.java:44)\r\n	at com.fans.admin.quartz.job.QuartzJobFactory.execute(QuartzJobFactory.java:76)\r\n	at com.fans.admin.quartz.job.QuartzJobFactory.execute(QuartzJobFactory.java:111)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\n', null, null, '2020-01-17 13:41:22', null, '2020-01-17 13:41:22', '0');
INSERT INTO `quartz_schedule_log` VALUES ('7cd3dff015bc4ae08e09ab9bfd452e46', '01b4108410ad4f88a54ad689a576c09a', '2020-01-17 13:41:17', '2020-01-17 13:41:17', '2', 'org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named \'t\' available\r\n	at org.springframework.beans.factory.support.DefaultListableBeanFactory.getBeanDefinition(DefaultListableBeanFactory.java:772)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.getMergedLocalBeanDefinition(AbstractBeanFactory.java:1221)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:294)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:273)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:199)\r\n	at org.springframework.context.support.AbstractApplicationContext.getBean(AbstractApplicationContext.java:1083)\r\n	at com.fans.common.utils.SpringUtil.getBean(SpringUtil.java:44)\r\n	at com.fans.admin.quartz.job.QuartzJobFactory.execute(QuartzJobFactory.java:76)\r\n	at com.fans.admin.quartz.job.QuartzJobFactory.execute(QuartzJobFactory.java:111)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\n', null, null, '2020-01-17 13:41:17', null, '2020-01-17 13:41:17', '0');
INSERT INTO `quartz_schedule_log` VALUES ('8082937ef43e4651b3b472226fe65721', 'fca5c473-b4a9-455c-bfee-37f19e3a39e0', '2019-01-17 08:54:50', '2019-01-17 08:54:50', '1', null, null, null, '2019-01-17 08:54:50', null, '2019-01-17 08:54:50', '0');
INSERT INTO `quartz_schedule_log` VALUES ('8f71f0a8363246de9e7e85612e16c0d2', 'fca5c473-b4a9-455c-bfee-37f19e3a39e0', '2019-01-16 17:40:47', '2019-01-16 17:40:47', '1', null, null, null, '2019-01-16 17:40:47', null, '2019-01-16 17:40:47', '0');
INSERT INTO `quartz_schedule_log` VALUES ('9560ffdbc02e4609af48cbb83a8e407f', '01b4108410ad4f88a54ad689a576c09a', '2020-01-17 13:41:18', '2020-01-17 13:41:18', '2', 'org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named \'t\' available\r\n	at org.springframework.beans.factory.support.DefaultListableBeanFactory.getBeanDefinition(DefaultListableBeanFactory.java:772)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.getMergedLocalBeanDefinition(AbstractBeanFactory.java:1221)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:294)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:273)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:199)\r\n	at org.springframework.context.support.AbstractApplicationContext.getBean(AbstractApplicationContext.java:1083)\r\n	at com.fans.common.utils.SpringUtil.getBean(SpringUtil.java:44)\r\n	at com.fans.admin.quartz.job.QuartzJobFactory.execute(QuartzJobFactory.java:76)\r\n	at com.fans.admin.quartz.job.QuartzJobFactory.execute(QuartzJobFactory.java:111)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\n', null, null, '2020-01-17 13:41:18', null, '2020-01-17 13:41:18', '0');
INSERT INTO `quartz_schedule_log` VALUES ('9844b0a17d1342ac9aff9d3223dde7af', 'fca5c473-b4a9-455c-bfee-37f19e3a39e0', '2019-12-28 11:39:04', '2019-12-28 11:39:04', '1', null, null, null, '2019-12-28 11:39:04', null, '2019-12-28 11:39:04', '0');
INSERT INTO `quartz_schedule_log` VALUES ('9b8e68dbbdec4a01bd44950ba11c45dd', 'fca5c473-b4a9-455c-bfee-37f19e3a39e0', '2019-01-16 17:40:39', '2019-01-16 17:40:39', '1', null, null, null, '2019-01-16 17:40:39', null, '2019-01-16 17:40:39', '0');
INSERT INTO `quartz_schedule_log` VALUES ('b2657a04897f4e21acb5e2467dbee7f0', '01b4108410ad4f88a54ad689a576c09a', '2020-01-17 13:41:19', '2020-01-17 13:41:19', '2', 'org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named \'t\' available\r\n	at org.springframework.beans.factory.support.DefaultListableBeanFactory.getBeanDefinition(DefaultListableBeanFactory.java:772)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.getMergedLocalBeanDefinition(AbstractBeanFactory.java:1221)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:294)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:273)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:199)\r\n	at org.springframework.context.support.AbstractApplicationContext.getBean(AbstractApplicationContext.java:1083)\r\n	at com.fans.common.utils.SpringUtil.getBean(SpringUtil.java:44)\r\n	at com.fans.admin.quartz.job.QuartzJobFactory.execute(QuartzJobFactory.java:76)\r\n	at com.fans.admin.quartz.job.QuartzJobFactory.execute(QuartzJobFactory.java:111)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\n', null, null, '2020-01-17 13:41:19', null, '2020-01-17 13:41:19', '0');
INSERT INTO `quartz_schedule_log` VALUES ('bd1d71c8c46842cb9639fbcaa731424d', 'fca5c473-b4a9-455c-bfee-37f19e3a39e0', '2019-01-17 08:54:59', '2019-01-17 08:54:59', '1', null, null, null, '2019-01-17 08:54:59', null, '2019-01-17 08:54:59', '0');
INSERT INTO `quartz_schedule_log` VALUES ('ccf0b418896b4cf5818bd3b58b877997', '8d960a318b6545e386078d433082a0ec', '2020-01-17 13:42:03', '2020-01-17 13:42:03', '2', 'org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named \'55\' available\r\n	at org.springframework.beans.factory.support.DefaultListableBeanFactory.getBeanDefinition(DefaultListableBeanFactory.java:772)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.getMergedLocalBeanDefinition(AbstractBeanFactory.java:1221)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:294)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:273)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:199)\r\n	at org.springframework.context.support.AbstractApplicationContext.getBean(AbstractApplicationContext.java:1083)\r\n	at com.fans.common.utils.SpringUtil.getBean(SpringUtil.java:44)\r\n	at com.fans.admin.quartz.job.QuartzJobFactory.execute(QuartzJobFactory.java:76)\r\n	at com.fans.admin.quartz.job.QuartzJobFactory.execute(QuartzJobFactory.java:111)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\n', null, null, '2020-01-17 13:42:03', null, '2020-01-17 13:42:03', '0');
INSERT INTO `quartz_schedule_log` VALUES ('ee4adc57bb0b485b91def758dab9fb65', 'fca5c473-b4a9-455c-bfee-37f19e3a39e0', '2020-01-17 14:08:25', '2020-01-17 14:08:25', '1', null, null, null, '2020-01-17 14:08:25', null, '2020-01-17 14:08:25', '0');
INSERT INTO `quartz_schedule_log` VALUES ('f02286e0fb9a42d9b3f71e1ce53cd197', '8d960a318b6545e386078d433082a0ec', '2020-01-17 13:42:07', '2020-01-17 13:42:07', '2', 'org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named \'552\' available\r\n	at org.springframework.beans.factory.support.DefaultListableBeanFactory.getBeanDefinition(DefaultListableBeanFactory.java:772)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.getMergedLocalBeanDefinition(AbstractBeanFactory.java:1221)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:294)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:273)\r\n	at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:199)\r\n	at org.springframework.context.support.AbstractApplicationContext.getBean(AbstractApplicationContext.java:1083)\r\n	at com.fans.common.utils.SpringUtil.getBean(SpringUtil.java:44)\r\n	at com.fans.admin.quartz.job.QuartzJobFactory.execute(QuartzJobFactory.java:76)\r\n	at com.fans.admin.quartz.job.QuartzJobFactory.execute(QuartzJobFactory.java:111)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\n', null, null, '2020-01-17 13:42:07', null, '2020-01-17 13:42:07', '0');

-- ----------------------------
-- Table structure for `schedule_job`
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job`;
CREATE TABLE `schedule_job` (
  `id` varchar(64) NOT NULL COMMENT '任务id',
  `name` varchar(200) DEFAULT NULL COMMENT '任务名称',
  `bean_name` varchar(200) DEFAULT NULL COMMENT 'spring bean名称',
  `method_name` varchar(100) DEFAULT NULL COMMENT '方法名',
  `params` varchar(2000) DEFAULT NULL COMMENT '参数',
  `cron_expression` varchar(100) DEFAULT NULL COMMENT 'cron表达式',
  `status` char(1) DEFAULT NULL COMMENT '任务状态  1：正常  0：暂停',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='定时任务';

-- ----------------------------
-- Records of schedule_job
-- ----------------------------
INSERT INTO `schedule_job` VALUES ('1', '测试1', 'test1', 'printHello', '定时任务 测试1 中的 printHello 执行了', '3/30 * * * * ? *', '0', null, null, null, null, null, '0');

-- ----------------------------
-- Table structure for `schedule_job_log`
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job_log`;
CREATE TABLE `schedule_job_log` (
  `id` varchar(64) NOT NULL COMMENT '任务日志id',
  `job_id` varchar(64) NOT NULL COMMENT '任务id',
  `name` varchar(200) DEFAULT NULL COMMENT '任务名称',
  `bean_name` varchar(200) DEFAULT NULL COMMENT 'spring bean名称',
  `method_name` varchar(100) DEFAULT NULL COMMENT '方法名',
  `params` varchar(2000) DEFAULT NULL COMMENT '参数',
  `status` char(1) NOT NULL COMMENT '任务状态    1：成功    0：失败',
  `error` varchar(2000) DEFAULT NULL COMMENT '失败信息',
  `times` int(11) NOT NULL COMMENT '耗时(单位：毫秒)',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='定时任务日志';

-- ----------------------------
-- Records of schedule_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_attach`
-- ----------------------------
DROP TABLE IF EXISTS `sys_attach`;
CREATE TABLE `sys_attach` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `path` varchar(200) NOT NULL COMMENT '路径',
  `name` varchar(50) NOT NULL COMMENT '附件名',
  `size` bigint(20) DEFAULT NULL COMMENT '文件大小',
  `extention` varchar(50) DEFAULT NULL COMMENT '文件后缀',
  `count` int(11) DEFAULT '0' COMMENT '引用计数',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='系统_附件';

-- ----------------------------
-- Records of sys_attach
-- ----------------------------
INSERT INTO `sys_attach` VALUES ('10', 'files/images/ffast/1543453366925.jpg', 'logo.jpg', '19759', 'jpg', '0', null, null, '1', '2019-01-07 14:10:04', null, '1');
INSERT INTO `sys_attach` VALUES ('11', 'files/images/ffast/1543453376515.jpg', 'logo1.jpg', '1788', 'jpg', '0', null, null, '1', '2019-01-07 14:10:04', null, '1');
INSERT INTO `sys_attach` VALUES ('4', 'files/images/ffast/1543453291412.jpg', 'logo_min.jpg', '2536', 'jpg', '0', null, null, null, null, null, '0');
INSERT INTO `sys_attach` VALUES ('5', 'files/images/ffast/1543453298462.jpg', 'logo_min.jpg', '2536', 'jpg', '0', null, null, null, null, null, '0');
INSERT INTO `sys_attach` VALUES ('6', 'files/images/ffast/1543453305905.jpg', 'logo.jpg', '19759', 'jpg', '0', null, null, null, null, null, '0');
INSERT INTO `sys_attach` VALUES ('7', 'files/images/ffast/1543453323831.jpg', 'logo.jpg', '19759', 'jpg', '0', null, null, null, null, null, '0');
INSERT INTO `sys_attach` VALUES ('8', 'files/images/ffast/1543453336190.jpg', 'logo_min.jpg', '2536', 'jpg', '0', null, null, null, null, null, '0');
INSERT INTO `sys_attach` VALUES ('9', 'files/images/ffast/1543453361499.jpg', 'logo_min.jpg', '2536', 'jpg', '0', null, null, '1', '2020-01-16 17:37:30', null, '1');

-- ----------------------------
-- Table structure for `sys_dict`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `id` varchar(32) NOT NULL COMMENT '字典类型id',
  `dict_code` varchar(50) NOT NULL COMMENT '数据字典编码',
  `dict_name` varchar(50) NOT NULL COMMENT '数据字典名称',
  `show_order` decimal(20,0) DEFAULT NULL COMMENT '数据字典显示顺序',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `AK_uk_dict` (`dict_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='基础字典类型';

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('0745b349a04c4ac383abab84e3d48773', 'role_status', '角色状态', '0', '1', '2019-01-08 13:28:40', '1', '2020-01-16 16:54:38', '角色状态（0,禁用，1启用）', '0');
INSERT INTO `sys_dict` VALUES ('1c438d3aa34a40d481d51460e508bc8b', 'menu_status', '按钮状态', '0', '1', '2019-01-09 10:42:23', null, '2019-01-09 10:42:23', '按钮状态（0，隐藏；1，显示）', '0');
INSERT INTO `sys_dict` VALUES ('2', 'sex', '性别', '2', null, null, '1', '2018-12-30 18:57:00', '性别:1,男；2，女', '0');
INSERT INTO `sys_dict` VALUES ('23aa656ab31948c2bb86bbc5ab1221ec', 'menu_type', '菜单类型', '0', '1', '2019-01-09 10:33:12', null, '2019-01-09 10:33:12', '菜单类型（1，菜单；2，权限）', '0');
INSERT INTO `sys_dict` VALUES ('2d7ebacc20c846c08c853bb27e50ffb4', 'qrt_execution_state', '任务执行状态', '0', '1', '2019-01-16 16:12:47', null, '2019-01-16 16:12:47', '任务执行状态（0，正常，1，暂停）', '0');
INSERT INTO `sys_dict` VALUES ('3', 'user_status', '用户状态', '1', null, null, '1', '2019-01-02 08:40:55', '用户状态', '0');
INSERT INTO `sys_dict` VALUES ('35d7f2afd0e54b988b3c8594b8dcc50c', 'log_type', '日志类型', '0', '1', '2019-01-09 15:13:43', null, '2019-01-09 15:13:43', '日志类型（1：正常日志；2：异常日志）', '0');
INSERT INTO `sys_dict` VALUES ('61ed4d8e89bd4696b16e8aa77a2e7138', 'out_link', '外部链接', '0', '1', '2019-01-03 15:12:30', '1', '2019-01-08 11:22:53', '外部链接（0，否，1是）', '0');
INSERT INTO `sys_dict` VALUES ('6f0dd1d6141a43fdb6e94d514d9f7508', 'nation', '民族', '0', '1', '2018-12-30 22:11:46', '1', '2019-01-02 16:56:29', '民族', '0');
INSERT INTO `sys_dict` VALUES ('95f3a9d001bd4002af579b92d1d08da3', 'fans_test', 'fans_test', '0', '1', '2020-01-16 16:54:56', '1', '2020-01-16 16:55:51', 'fans_test11', '1');
INSERT INTO `sys_dict` VALUES ('ac9a88a3a85a40d391c1d05ee04023b4', 'qrt_job_status', '定时任务：任务状态', '0', '1', '2019-01-16 11:30:02', null, '2019-01-16 11:30:02', '任务状态（0：禁用；1：启用；2：删除）', '0');
INSERT INTO `sys_dict` VALUES ('af48cecbb9ac402f8a8d1857c558a1cc', 'grant_types', 'oauth2授权类型', '0', '1', '2019-01-11 17:25:43', null, '2019-01-11 17:25:43', 'oauth2授权类型（authorization_code   password   implicit   client_credentials   refresh_token）', '0');
INSERT INTO `sys_dict` VALUES ('cb5e9aca839a4007b56f749c283b1cae', 'auto_approve', '是否让用户同意授权', '0', '1', '2019-01-11 21:58:23', '1', '2019-01-11 22:05:32', '是否让用户同意授权（true,是；false，否）', '0');
INSERT INTO `sys_dict` VALUES ('ce1dcef3abff40eb94514083090206cb', 'log_op_type', '登录日志操作类型', '0', '1', '2019-01-10 17:37:29', '1', '2019-01-11 10:33:53', '登录日志操作类型(1，登录;2退出)', '0');
INSERT INTO `sys_dict` VALUES ('f06afb69e7094c9f950a6efd09735c85', 'display_mode', '显示方式', '0', '1', '2019-01-03 15:13:43', null, '2019-01-03 15:13:43', '显示方式（0，普通内嵌，1，内嵌iframe，2，外窗口）', '0');
INSERT INTO `sys_dict` VALUES ('f2de3c7dbd7044feb5397dbe607ce77c', 'log_result_type', '登录日志：结果类型', '0', '1', '2019-01-11 10:35:05', null, '2019-01-11 10:35:05', '结果类型：（1，成功；2，失败）', '0');
INSERT INTO `sys_dict` VALUES ('f3b39ac8f8344fbf9d834e409f70d7c2', 'qrt_log_status', '定时任务日志状态', '0', '1', '2019-01-15 17:35:53', null, '2019-01-15 17:35:53', '定时任务日志状态（1，成功；2，失败）', '0');

-- ----------------------------
-- Table structure for `sys_dict_item`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_item`;
CREATE TABLE `sys_dict_item` (
  `id` varchar(32) NOT NULL COMMENT '字典id',
  `dict_id` varchar(32) NOT NULL COMMENT '数据字典ID',
  `item_code` varchar(50) DEFAULT NULL COMMENT '数据字典项编码',
  `item_value` varchar(200) NOT NULL COMMENT '数据字典项值',
  `show_order` decimal(20,0) DEFAULT NULL COMMENT '字典项显示顺序',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `AK_uk_dict` (`dict_id`,`item_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='字典';

-- ----------------------------
-- Records of sys_dict_item
-- ----------------------------
INSERT INTO `sys_dict_item` VALUES ('0c5d546e12cd42dba39b72fbac113efe', '35d7f2afd0e54b988b3c8594b8dcc50c', '2', '异常日志', '2', '1', '2019-01-09 15:14:09', null, '2019-01-09 15:14:09', '日志类型（1：正常日志；2：异常日志）', '0');
INSERT INTO `sys_dict_item` VALUES ('10420f3aacaa4db2bb8549961961f811', '6f0dd1d6141a43fdb6e94d514d9f7508', '7', '彝族', '7', '1', '2018-12-30 23:24:27', null, '2018-12-30 23:24:27', '7，彝族', '0');
INSERT INTO `sys_dict_item` VALUES ('11ca8c285fa64576957e370338fa407b', '6f0dd1d6141a43fdb6e94d514d9f7508', '5', '维吾尔族1', '5', '1', '2018-12-30 23:23:58', '1', '2019-01-02 08:58:40', '5，维吾尔族', '0');
INSERT INTO `sys_dict_item` VALUES ('15263d9af11e4d47a33ce5eca1465a0e', '6f0dd1d6141a43fdb6e94d514d9f7508', '8', ' 蒙古族', '8', '1', '2018-12-30 23:24:47', null, '2018-12-30 23:24:47', '8， 蒙古族', '0');
INSERT INTO `sys_dict_item` VALUES ('1ebaf7d88591413880b55ccca7305d8e', 'cb5e9aca839a4007b56f749c283b1cae', 'false', '否', '2', '1', '2019-01-11 21:58:46', '1', '2019-01-11 22:04:06', '是否让用户同意授权（true,是；false，否）', '0');
INSERT INTO `sys_dict_item` VALUES ('1f01fb7e3aa146f2bbf996403861211e', '6f0dd1d6141a43fdb6e94d514d9f7508', '10', ' 布依族', '10', '1', '2018-12-30 23:25:38', null, '2018-12-30 23:25:38', '10  布依族', '0');
INSERT INTO `sys_dict_item` VALUES ('25ea9dc060834bb083140e0cfa27c0ad', '6f0dd1d6141a43fdb6e94d514d9f7508', '12', '瑶族', '12', '1', '2018-12-30 23:26:10', null, '2018-12-30 23:26:10', '12 瑶族', '0');
INSERT INTO `sys_dict_item` VALUES ('26ba904d976143ffb19eb23d2cf29d41', '6f0dd1d6141a43fdb6e94d514d9f7508', '1', '壮族', '1', '1', '2018-12-30 23:22:38', '1', '2019-01-02 08:48:04', ' 1，壮族', '0');
INSERT INTO `sys_dict_item` VALUES ('29501fcf170946f6861a49e00ec10389', '2d7ebacc20c846c08c853bb27e50ffb4', '1', '暂停', '1', '1', '2019-01-16 16:13:28', null, '2019-01-16 16:13:28', '任务执行状态（0，正常，1，暂停）', '0');
INSERT INTO `sys_dict_item` VALUES ('2f7453cf28aa4bcdbf673fe6a0e911b5', 'af48cecbb9ac402f8a8d1857c558a1cc', 'authorization_code', 'authorization_code', '1', '1', '2019-01-11 17:25:58', null, '2019-01-11 17:25:58', 'oauth2授权类型（authorization_code   password   implicit   client_credentials   refresh_token）', '0');
INSERT INTO `sys_dict_item` VALUES ('2fa9065632884718a2f65504a654da3c', 'ac9a88a3a85a40d391c1d05ee04023b4', '2', '删除', '2', '1', '2019-01-16 11:30:42', null, '2019-01-16 11:30:42', '任务状态（0：禁用；1：启用；2：删除）', '0');
INSERT INTO `sys_dict_item` VALUES ('3', '2', '1', '男', '2', null, null, '1', '2018-12-30 20:59:44', '性别：1，男', '0');
INSERT INTO `sys_dict_item` VALUES ('323d955f75464af2a1f6294efa94c837', 'f06afb69e7094c9f950a6efd09735c85', '0', '普通内嵌', '0', '1', '2019-01-03 15:14:02', null, '2019-01-03 15:14:02', '显示方式（0，普通内嵌，1，内嵌iframe，2，外窗口）', '0');
INSERT INTO `sys_dict_item` VALUES ('386f1f940dc84c5bb07a1d91d03f4218', '95f3a9d001bd4002af579b92d1d08da3', '111', '11', '0', '1', '2020-01-16 16:55:17', '1', '2020-01-16 16:55:45', '1', '1');
INSERT INTO `sys_dict_item` VALUES ('38b999eac3a64cd397e70b57707685f1', 'af48cecbb9ac402f8a8d1857c558a1cc', 'implicit', 'implicit', '3', '1', '2019-01-11 17:26:18', null, '2019-01-11 17:26:18', 'oauth2授权类型（authorization_code   password   implicit   client_credentials   refresh_token）', '0');
INSERT INTO `sys_dict_item` VALUES ('3bd33dd11312470fa98c389999d8995c', '6f0dd1d6141a43fdb6e94d514d9f7508', '4', ' 苗族', '4', '1', '2018-12-30 23:23:38', null, '2018-12-30 23:23:38', '4， 苗族', '0');
INSERT INTO `sys_dict_item` VALUES ('4', '2', '2', '女', '3', null, null, '1', '2018-12-30 20:59:47', '性别：2，女', '0');
INSERT INTO `sys_dict_item` VALUES ('499f31ef0d134318b83ed2a2bb8deb57', 'af48cecbb9ac402f8a8d1857c558a1cc', 'password', 'password', '2', '1', '2019-01-11 17:26:08', null, '2019-01-11 17:26:08', 'oauth2授权类型（authorization_code   password   implicit   client_credentials   refresh_token）', '0');
INSERT INTO `sys_dict_item` VALUES ('4b3046e313264c8dae33b8349ea13a2b', '61ed4d8e89bd4696b16e8aa77a2e7138', '1', '是', '1', '1', '2019-01-03 15:13:13', null, '2019-01-03 15:13:13', '外部链接（0，否，1是）', '0');
INSERT INTO `sys_dict_item` VALUES ('5', '3', '1', '可用', '1', null, null, '1', '2018-12-30 20:59:41', '用户状态：1可用', '0');
INSERT INTO `sys_dict_item` VALUES ('53c846aa8c964afebccfeccd5819cccd', '2d7ebacc20c846c08c853bb27e50ffb4', '0', '正常', '0', '1', '2019-01-16 16:13:20', null, '2019-01-16 16:13:20', '任务执行状态（0，正常，1，暂停）', '0');
INSERT INTO `sys_dict_item` VALUES ('5f81a744cf744fbdad4476878e9b20fb', '6f0dd1d6141a43fdb6e94d514d9f7508', '11', '侗族', '11', '1', '2018-12-30 23:25:58', null, '2018-12-30 23:25:58', '11 侗族', '0');
INSERT INTO `sys_dict_item` VALUES ('6', '3', '2', '不可用', '0', null, null, '1', '2018-12-30 21:56:27', '用户状态：2不可用', '0');
INSERT INTO `sys_dict_item` VALUES ('670646b077d140b8a3894c27fb3909ad', '61ed4d8e89bd4696b16e8aa77a2e7138', '0', '否', '0', '1', '2019-01-03 15:12:59', null, '2019-01-03 15:12:59', '外部链接（0，否，1是）', '0');
INSERT INTO `sys_dict_item` VALUES ('6a0967d8d209421daefb39b3698a2369', '0745b349a04c4ac383abab84e3d48773', '1', '启用', '1', '1', '2019-01-08 13:29:11', '1', '2019-01-08 13:46:49', '角色状态（0,禁用，1启用）', '0');
INSERT INTO `sys_dict_item` VALUES ('6ffb3232a28940509a16e67baaddddee', '95f3a9d001bd4002af579b92d1d08da3', '22', '3', '0', '1', '2020-01-16 16:55:23', '1', '2020-01-16 16:55:42', '22', '1');
INSERT INTO `sys_dict_item` VALUES ('7846a85047724ac18f5813b21540edb3', 'af48cecbb9ac402f8a8d1857c558a1cc', 'refresh_token', 'refresh_token', '5', '1', '2019-01-11 17:26:37', null, '2019-01-11 17:26:37', 'oauth2授权类型（authorization_code   password   implicit   client_credentials   refresh_token）', '0');
INSERT INTO `sys_dict_item` VALUES ('832398acbfb649b2a4eea0bcd7880155', '6f0dd1d6141a43fdb6e94d514d9f7508', '0', '汉族', '0', '1', '2018-12-30 23:20:13', '1', '2019-01-02 16:56:35', '0，汉族', '0');
INSERT INTO `sys_dict_item` VALUES ('862f5677794e4e3ba0057b8b2c4318f1', '1c438d3aa34a40d481d51460e508bc8b', '0', '隐藏', '0', '1', '2019-01-09 10:43:06', null, '2019-01-09 10:43:06', '按钮状态（0，隐藏；1，显示）', '0');
INSERT INTO `sys_dict_item` VALUES ('8b6d435f2ab84e1cb783b3d6490f36d7', '6f0dd1d6141a43fdb6e94d514d9f7508', '14', '白族', '14', '1', '2018-12-30 23:26:51', null, '2018-12-30 23:26:51', '14 白族', '0');
INSERT INTO `sys_dict_item` VALUES ('92f45027f7dd4ef4b34dd1eb14b230b4', 'ac9a88a3a85a40d391c1d05ee04023b4', '0', '禁用', '0', '1', '2019-01-16 11:30:23', null, '2019-01-16 11:30:23', '任务状态（0：禁用；1：启用；2：删除）', '0');
INSERT INTO `sys_dict_item` VALUES ('940305602db74217b7b74d343ddc7161', 'af48cecbb9ac402f8a8d1857c558a1cc', 'client_credentials', 'client_credentials', '4', '1', '2019-01-11 17:26:26', null, '2019-01-11 17:26:26', 'oauth2授权类型（authorization_code   password   implicit   client_credentials   refresh_token）', '0');
INSERT INTO `sys_dict_item` VALUES ('94f8e88dee614a669b3712ba80b29b19', '23aa656ab31948c2bb86bbc5ab1221ec', '1', '菜单', '1', '1', '2019-01-09 10:33:27', null, '2019-01-09 10:33:27', '菜单类型（1，菜单；2，权限）', '0');
INSERT INTO `sys_dict_item` VALUES ('9966af0248804c199e96f863136bfd84', 'ce1dcef3abff40eb94514083090206cb', '2', '退出', '2', '1', '2019-01-10 17:39:31', '1', '2019-01-11 10:34:12', '登录日志操作类型(1，登录;2退出)', '0');
INSERT INTO `sys_dict_item` VALUES ('9cbdad41b1df466695d183f56f54b675', 'f2de3c7dbd7044feb5397dbe607ce77c', '1', '成功', '1', '1', '2019-01-11 10:35:28', null, '2019-01-11 10:35:28', '结果类型：（1，成功；2，失败）', '0');
INSERT INTO `sys_dict_item` VALUES ('9e75af5c31ab4f918e737b6c6bbb7119', 'f3b39ac8f8344fbf9d834e409f70d7c2', '1', '成功', '1', '1', '2019-01-15 17:36:16', null, '2019-01-15 17:36:16', '定时任务日志状态（1，成功；2，失败）', '0');
INSERT INTO `sys_dict_item` VALUES ('a27e5055d3c24e3b9418cd24b52d11e0', 'ce1dcef3abff40eb94514083090206cb', '1', '登录', '1', '1', '2019-01-10 17:39:18', '1', '2019-01-11 10:34:06', '登录日志操作类型(1，登录;2退出)', '0');
INSERT INTO `sys_dict_item` VALUES ('a891eb16688047e1a1ba4d37ab52b23b', 'cb5e9aca839a4007b56f749c283b1cae', 'true', '是', '1', '1', '2019-01-11 21:58:37', '1', '2019-01-11 22:04:02', '是否让用户同意授权（true,是；false，否）', '0');
INSERT INTO `sys_dict_item` VALUES ('ade8a67b30df4712a6d93bd69950243a', '6f0dd1d6141a43fdb6e94d514d9f7508', '9', '藏族', '9', '1', '2018-12-30 23:25:15', null, '2018-12-30 23:25:15', '9，藏族', '0');
INSERT INTO `sys_dict_item` VALUES ('b26f478139de47d5ab702dcdb5addb5a', '23aa656ab31948c2bb86bbc5ab1221ec', '2', '权限', '2', '1', '2019-01-09 10:33:36', null, '2019-01-09 10:33:36', '菜单类型（1，菜单；2，权限）', '0');
INSERT INTO `sys_dict_item` VALUES ('bb8ba1b5b36c40e8a47827a3557c7505', '35d7f2afd0e54b988b3c8594b8dcc50c', '1', '正常日志', '1', '1', '2019-01-09 15:13:59', null, '2019-01-09 15:13:59', '日志类型（1：正常日志；2：异常日志）', '0');
INSERT INTO `sys_dict_item` VALUES ('bc30396cabd44ee6b9c5f30b1a4f7a70', '6f0dd1d6141a43fdb6e94d514d9f7508', '6', '土家族', '6', '1', '2018-12-30 23:24:12', null, '2018-12-30 23:24:12', '6，土家族', '0');
INSERT INTO `sys_dict_item` VALUES ('bf53293ec251474ea30a838284b1ca6e', '0745b349a04c4ac383abab84e3d48773', '0', '禁用', '0', '1', '2019-01-08 13:29:03', '1', '2019-01-11 14:01:53', '角色状态（0,禁用，1启用）', '0');
INSERT INTO `sys_dict_item` VALUES ('c2f40188572e4cb4aa25a79bb3d8a83d', '6f0dd1d6141a43fdb6e94d514d9f7508', '2', '满族', '2', '1', '2018-12-30 23:23:01', null, '2018-12-30 23:23:01', '2，满族', '0');
INSERT INTO `sys_dict_item` VALUES ('c3e13ca77e264f4e9bb3c1eb1f712fa4', 'f2de3c7dbd7044feb5397dbe607ce77c', '2', '失败', '2', '1', '2019-01-11 10:35:37', null, '2019-01-11 10:35:37', '结果类型：（1，成功；2，失败）', '0');
INSERT INTO `sys_dict_item` VALUES ('c4f572257ca243c48cd00cf0c1aa9fd9', '6f0dd1d6141a43fdb6e94d514d9f7508', '13', '朝鲜族', '13', '1', '2018-12-30 23:26:27', '1', '2018-12-30 23:30:44', '13，朝鲜族', '0');
INSERT INTO `sys_dict_item` VALUES ('cae312c2293a448080c356dca7de3fb0', 'ac9a88a3a85a40d391c1d05ee04023b4', '1', '启用', '1', '1', '2019-01-16 11:30:32', null, '2019-01-16 11:30:32', '任务状态（0：禁用；1：启用；2：删除）', '0');
INSERT INTO `sys_dict_item` VALUES ('cec8ef37887644598ed3c1caff3f28ce', 'f3b39ac8f8344fbf9d834e409f70d7c2', '2', '失败', '2', '1', '2019-01-15 17:36:23', null, '2019-01-15 17:36:23', '定时任务日志状态（1，成功；2，失败）', '0');
INSERT INTO `sys_dict_item` VALUES ('d59c7028fa5d4d53ac688547805407e0', 'f06afb69e7094c9f950a6efd09735c85', '1', '内嵌iframe', '1', '1', '2019-01-03 15:14:16', '1', '2019-01-09 09:42:33', '显示方式（0，普通内嵌，1，内嵌iframe，2，外窗口）', '0');
INSERT INTO `sys_dict_item` VALUES ('dd3f6657db5b4114b0b24ab8fd09c4fb', 'f06afb69e7094c9f950a6efd09735c85', '2', '外窗口', '2', '1', '2019-01-03 15:14:32', null, '2019-01-03 15:14:32', '显示方式（0，普通内嵌，1，内嵌iframe，2，外窗口）', '0');
INSERT INTO `sys_dict_item` VALUES ('eeb4589a6db846afb9d9a71ceda5a1cf', '6f0dd1d6141a43fdb6e94d514d9f7508', '3', '回族', '3', '1', '2018-12-30 23:23:20', '1', '2019-01-02 08:48:13', '3，回族', '0');
INSERT INTO `sys_dict_item` VALUES ('f595beb8401a40f1805dc667d5f81504', '6f0dd1d6141a43fdb6e94d514d9f7508', '15', '哈尼族', '15', '1', '2018-12-30 23:27:08', null, '2018-12-30 23:27:08', '15 哈尼族 ', '0');
INSERT INTO `sys_dict_item` VALUES ('fd926708d15444bda6625fdfc9d1bb3c', '1c438d3aa34a40d481d51460e508bc8b', '1', '显示', '1', '1', '2019-01-09 10:43:14', null, '2019-01-09 10:43:14', '按钮状态（0，隐藏；1，显示）', '0');

-- ----------------------------
-- Table structure for `sys_log`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `operation` varchar(250) CHARACTER SET utf8 DEFAULT NULL COMMENT '用户操作',
  `content` varchar(5000) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '日志内容',
  `ip` varchar(255) DEFAULT NULL COMMENT '操作IP地址',
  `method` varchar(6) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '操作方式',
  `type` char(1) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '1' COMMENT '日志类型(1：正常   2：异常)',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='系统日志';

-- ----------------------------
-- Records of sys_log
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_login_log`
-- ----------------------------
DROP TABLE IF EXISTS `sys_login_log`;
CREATE TABLE `sys_login_log` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `oper_type` char(1) DEFAULT NULL COMMENT '操作类型（1,登录;2,登出）',
  `result_type` char(1) DEFAULT NULL COMMENT '登录结果类型(1,成功；2失败)',
  `remote_addr` varchar(255) DEFAULT NULL COMMENT '操作IP地址',
  `user_agent` varchar(255) DEFAULT NULL COMMENT '用户代理',
  `oper_time` datetime DEFAULT NULL COMMENT '操作时间（登录/退出的时间）',
  `address` varchar(255) DEFAULT NULL COMMENT '地点',
  `browser_type` varchar(255) DEFAULT NULL COMMENT '浏览器',
  `login_type` varchar(50) DEFAULT NULL COMMENT '登录类型',
  `equipment` varchar(50) DEFAULT NULL COMMENT '设备',
  `logon_id` varchar(50) DEFAULT NULL COMMENT '登录帐号',
  `create_by` varchar(50) DEFAULT NULL COMMENT '用户ID',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户登录日志';

-- ----------------------------
-- Records of sys_login_log
-- ----------------------------
INSERT INTO `sys_login_log` VALUES ('2513df2279d84303a84adf9d9f959052', '1', '1', '192.168.189.1', '其它', '2020-04-18 18:35:26', null, '其它', '电脑登录', '电脑', 'admin', null, '2020-04-18 18:35:26', null, '2020-04-18 18:35:26', '登录成功！！', '0');
INSERT INTO `sys_login_log` VALUES ('c3bf18aa4e184b4aa2801ddf91d927ff', '1', '1', '192.168.189.1', '其它', '2020-04-18 18:35:57', null, '其它', '电脑登录', '电脑', 'admin', null, '2020-04-18 18:35:57', null, '2020-04-18 18:35:57', '登录成功！！', '0');
INSERT INTO `sys_login_log` VALUES ('8317c2001e7848dfaeb5b616dfde036d', '1', '1', '192.168.189.1', '其它', '2020-04-18 18:39:27', null, '其它', '电脑登录', '电脑', 'admin', null, '2020-04-18 18:39:27', null, '2020-04-18 18:39:27', '登录成功！！', '0');
INSERT INTO `sys_login_log` VALUES ('40663b84b5d4449eb026296577f941e0', '1', '1', '192.168.189.1', '其它', '2020-04-18 18:42:55', null, '其它', '电脑登录', '电脑', 'admin', null, '2020-04-18 18:42:55', null, '2020-04-18 18:42:55', '登录成功！！', '0');
INSERT INTO `sys_login_log` VALUES ('995e67f15878439abf726f44370561c2', '2', '1', '192.168.189.1', '其它', '2020-04-18 18:45:08', null, '其它', '电脑登录', '电脑', 'admin', null, '2020-04-18 18:45:08', null, '2020-04-18 18:45:08', '用户退出登录成功！', '0');
INSERT INTO `sys_login_log` VALUES ('f2f23256ac4846e6bbdc7a8303dc5a3f', '1', '1', '192.168.189.1', '其它', '2020-04-18 18:45:52', null, '其它', '电脑登录', '电脑', 'admin', null, '2020-04-18 18:45:52', null, '2020-04-18 18:45:52', '登录成功！！', '0');
INSERT INTO `sys_login_log` VALUES ('54689006db0c4771aff07dffc4ae29dd', '1', '1', '192.168.189.1', '其它', '2020-04-18 18:48:53', null, '其它', '电脑登录', '电脑', 'admin', null, '2020-04-18 18:48:53', null, '2020-04-18 18:48:53', '登录成功！！', '0');
INSERT INTO `sys_login_log` VALUES ('38d932274f6045c3a455554bb5de9ca6', '2', '1', '192.168.189.1', '其它', '2020-04-18 18:57:18', null, '其它', '电脑登录', '电脑', 'admin', null, '2020-04-18 18:57:18', null, '2020-04-18 18:57:18', '用户退出登录成功！', '0');
INSERT INTO `sys_login_log` VALUES ('42d5c8fa129c4cbb881cf31e7c157474', '1', '1', '192.168.189.1', '其它', '2020-04-18 18:57:27', null, '其它', '电脑登录', '电脑', 'admin', null, '2020-04-18 18:57:27', null, '2020-04-18 18:57:27', '登录成功！！', '0');
INSERT INTO `sys_login_log` VALUES ('b942eb5e29864fe689031fe65a14b180', '2', '1', '192.168.189.1', '其它', '2020-04-18 19:02:27', null, '其它', '电脑登录', '电脑', 'admin', null, '2020-04-18 19:02:27', null, '2020-04-18 19:02:27', '用户退出登录成功！', '0');
INSERT INTO `sys_login_log` VALUES ('87dcc56da14c4364b62f8ba1763b83ea', '1', '1', '192.168.189.1', '其它', '2020-04-18 19:02:37', null, '其它', '电脑登录', '电脑', 'admin', null, '2020-04-18 19:02:37', null, '2020-04-18 19:02:37', '登录成功！！', '0');
INSERT INTO `sys_login_log` VALUES ('18b07f516cd94193ab50033802a811d4', '2', '1', '192.168.189.1', '其它', '2020-04-18 19:07:39', null, '其它', '电脑登录', '电脑', 'admin', null, '2020-04-18 19:07:39', null, '2020-04-18 19:07:39', '用户退出登录成功！', '0');
INSERT INTO `sys_login_log` VALUES ('1a68d42217fb493abb193db7abbeed0f', '1', '1', '192.168.189.1', '其它', '2020-04-18 19:07:46', null, '其它', '电脑登录', '电脑', 'admin', null, '2020-04-18 19:07:46', null, '2020-04-18 19:07:46', '登录成功！！', '0');
INSERT INTO `sys_login_log` VALUES ('1a822a28f43f4944b90448c082d7db3b', '2', '1', '192.168.189.1', '其它', '2020-04-18 20:08:51', null, '其它', '电脑登录', '电脑', 'admin', null, '2020-04-18 20:08:51', null, '2020-04-18 20:08:51', '用户退出登录成功！', '0');
INSERT INTO `sys_login_log` VALUES ('1ad544b007d0478b9bc0767663f884fe', '1', '1', '192.168.189.1', '其它', '2020-04-18 20:08:58', null, '其它', '电脑登录', '电脑', 'admin', null, '2020-04-18 20:08:58', null, '2020-04-18 20:08:58', '登录成功！！', '0');
INSERT INTO `sys_login_log` VALUES ('af48eb4010d2419f9801589ae7234bae', '2', '1', '192.168.189.1', '其它', '2020-04-18 20:09:04', null, '其它', '电脑登录', '电脑', 'admin', null, '2020-04-18 20:09:04', null, '2020-04-18 20:09:04', '用户退出登录成功！', '0');
INSERT INTO `sys_login_log` VALUES ('30df999bec5d4a1fa109a9ab28783224', '1', '1', '192.168.189.1', '其它', '2020-04-18 20:09:14', null, '其它', '电脑登录', '电脑', 'admin', null, '2020-04-18 20:09:14', null, '2020-04-18 20:09:14', '登录成功！！', '0');
INSERT INTO `sys_login_log` VALUES ('35626357546d425ab799f43f4dac06a7', '2', '1', '192.168.189.1', '其它', '2020-04-18 20:16:12', null, '其它', '电脑登录', '电脑', 'admin', null, '2020-04-18 20:16:12', null, '2020-04-18 20:16:12', '用户退出登录成功！', '0');
INSERT INTO `sys_login_log` VALUES ('1939b40909014affa59895ebbef6f987', '1', '1', '192.168.189.1', '其它', '2020-04-18 20:16:20', null, '其它', '电脑登录', '电脑', 'admin', null, '2020-04-18 20:16:20', null, '2020-04-18 20:16:20', '登录成功！！', '0');
INSERT INTO `sys_login_log` VALUES ('747d2970522a4affb8d1610652d00626', '2', '1', '192.168.189.1', '其它', '2020-04-18 20:18:09', null, '其它', '电脑登录', '电脑', 'admin', null, '2020-04-18 20:18:09', null, '2020-04-18 20:18:09', '用户退出登录成功！', '0');
INSERT INTO `sys_login_log` VALUES ('a25ccb32e4b3414e8a7f352579abb8e9', '1', '1', '192.168.189.1', '其它', '2020-04-18 20:18:17', null, '其它', '电脑登录', '电脑', 'admin', null, '2020-04-18 20:18:17', null, '2020-04-18 20:18:17', '登录成功！！', '0');
INSERT INTO `sys_login_log` VALUES ('c6cfc04b23f349849bedcf73a10edef7', '2', '1', '192.168.189.1', '其它', '2020-04-18 20:24:33', null, '其它', '电脑登录', '电脑', 'admin', null, '2020-04-18 20:24:33', null, '2020-04-18 20:24:33', '用户退出登录成功！', '0');
INSERT INTO `sys_login_log` VALUES ('6366a11e836040378bd69baeda15bfad', '1', '1', '192.168.189.1', '其它', '2020-04-18 20:24:41', null, '其它', '电脑登录', '电脑', 'admin', null, '2020-04-18 20:24:41', null, '2020-04-18 20:24:41', '登录成功！！', '0');

-- ----------------------------
-- Table structure for `sys_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `name` varchar(50) NOT NULL COMMENT '标题',
  `identity` varchar(50) DEFAULT NULL COMMENT '资源标识符',
  `url` varchar(200) DEFAULT NULL COMMENT '菜单url',
  `parent_id` varchar(32) DEFAULT NULL COMMENT '父资源',
  `parent_ids` varchar(500) DEFAULT NULL COMMENT '父路径',
  `weight` int(11) DEFAULT NULL COMMENT '菜单权重',
  `icon` varchar(50) DEFAULT NULL COMMENT '菜单图标',
  `status` char(1) DEFAULT NULL COMMENT '资源类型（1=显示2禁止0=隐藏）',
  `menu_type` char(1) NOT NULL COMMENT '资源类型（1=菜单2=权限）',
  `out_link` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '外部链接（0，否，1是）',
  `display_mode` char(1) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '0' COMMENT '显示方式（0，普通内嵌，1，内嵌iframe，2，外窗口）',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `AK_U` (`identity`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='系统_资源';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('00c2a9b405204e168b81ec5f4faeaecb', '数据字典项:添加', 'sysDictItem:create', '/sys/sysDictItem/save', '7', null, '1', '', '1', '2', '0', '0', '1', '2018-12-30 20:53:59', '1', '2019-01-02 15:53:44', null, '0');
INSERT INTO `sys_menu` VALUES ('05b3d90607a443748949f279489409fd', '数据字典项:删除', 'sysDictItem:delete', '/sys/sysDictItem/delete', '7', null, '3', '', '1', '2', '0', '0', '1', '2018-12-30 20:56:27', '1', '2019-01-02 15:53:23', null, '0');
INSERT INTO `sys_menu` VALUES ('06e9f7c71b334cb1ab2f6285eb385aaa', 'doc接口文档', 'doc', 'http://localhost:9001/doc.html', '4d514d86422b4dc9a5511f4e1e9842d1', null, '10', 'md-book', '1', '1', '1', '2', '1', '2020-01-19 13:36:20', '1', '2020-01-19 13:39:11', null, '0');
INSERT INTO `sys_menu` VALUES ('07ff4b87a8374a2cba8bdd8619a1e3ca', '定时任务：执行', 'scheduleJob:run', '/quartz/scheduleJob/run', '29', null, '6', '', '1', '2', '0', '0', '1', '2019-01-15 13:42:26', null, '2019-01-15 13:42:26', null, '0');
INSERT INTO `sys_menu` VALUES ('1', '系统管理', 'sys', '', '', '', '0', 'md-cog', '1', '1', '0', '0', null, null, '1', '2019-01-10 10:26:04', null, '0');
INSERT INTO `sys_menu` VALUES ('10', '数据字典:更新', 'sysDict:update', '/sys/sysDict/update', '7', '', null, '', '1', '2', '0', '0', null, null, '1', '2019-01-02 15:53:08', null, '0');
INSERT INTO `sys_menu` VALUES ('1065e003054440b89e497c534080ace5', 'vue教程', 'vue', 'http://www.runoob.com/vue2/vue-tutorial.html', '232', null, '11', 'md-analytics', '1', '1', '1', '2', '1', '2019-01-03 13:28:20', '1', '2019-01-09 15:52:05', null, '0');
INSERT INTO `sys_menu` VALUES ('11', '数据字典:删除', 'sysDict:delete', '/sys/sysDict/delete', '7', '', null, '', '1', '2', '0', '0', null, null, '1', '2019-01-02 15:52:47', null, '0');
INSERT INTO `sys_menu` VALUES ('12', '角色管理', 'sysRole', '', '1', '', '3', 'ios-people', '1', '1', '0', '0', null, null, '1', '2019-01-07 09:03:44', null, '0');
INSERT INTO `sys_menu` VALUES ('13', '角色管理:列表显示', 'sysRole:list', '/sys/sysRole/list', '12', '', null, '', '1', '2', '0', '0', null, null, '1', '2019-01-02 16:45:26', null, '0');
INSERT INTO `sys_menu` VALUES ('14', '角色管理:添加', 'sysRole:create', '/sys/role/create', '12', '', null, '', '1', '2', '0', '0', null, null, '1', '2019-01-02 15:48:55', null, '0');
INSERT INTO `sys_menu` VALUES ('145ac6f04703492188cf9b9d17a3e66f', '测试查询', 'testDict:list', '#', 'eeb8337985134f2c89af24727d81e88e', null, '1', '#', '1', '2', '0', '0', '1', '2020-04-15 11:13:03', '1', '2020-04-15 11:13:03', '测试查询', '0');
INSERT INTO `sys_menu` VALUES ('15', '角色管理:更新', 'sysRole:update', '/sys/sysRole/update', '12', '', null, '', '1', '2', '0', '0', null, null, '1', '2019-01-02 15:48:32', null, '0');
INSERT INTO `sys_menu` VALUES ('16', '角色管理:删除', 'sysRole:delete', '/sys/sysRole/delete', '12', '', null, '', '1', '2', '0', '0', null, null, '1', '2019-01-02 16:38:50', null, '0');
INSERT INTO `sys_menu` VALUES ('17', '用户管理', 'sysUser', '', '1', '', '0', 'md-person-add', '1', '1', '0', '0', null, null, '1', '2019-01-10 10:14:07', null, '0');
INSERT INTO `sys_menu` VALUES ('18', '用户管理:列表显示', 'sysUser:list', '/sys/sysUser/list', '17', '', null, '', '1', '2', '0', '0', null, null, '1', '2019-01-02 16:45:34', null, '0');
INSERT INTO `sys_menu` VALUES ('19', '用户管理:添加', 'sysUser:create', '/sys/sysUser/save', '17', '', null, '', '1', '2', '0', '0', null, null, '1', '2019-01-02 15:50:09', null, '0');
INSERT INTO `sys_menu` VALUES ('1b676f49a44844bfbd280dc9666a2766', '定时任务：定时日志', 'scheduleLog:list', '/quartz/scheduleLog:/list', '29', null, '5', '', '1', '2', '0', '0', '1', '2019-01-15 10:38:11', '1', '2019-01-15 10:39:01', null, '0');
INSERT INTO `sys_menu` VALUES ('1ecda09bb9e04cf5a30202233cfab310', '认证用户:认证信息', 'sysOauthUsers:listByUsername', '/sys/sysOauthUsers/listByUsername', '89409ba5530b4aa29479ed6cc1ab0989', null, '2', '', '1', '2', '0', '0', '1', '2019-01-12 21:36:01', '1', '2020-01-17 10:05:18', null, '1');
INSERT INTO `sys_menu` VALUES ('2', '菜单配置', 'sysMenu', '', '1', '', '1', 'md-list', '1', '1', '0', '0', null, null, '1', '2019-01-07 09:01:37', null, '0');
INSERT INTO `sys_menu` VALUES ('20', '用户管理:更新', 'sysUser:update', '/sys/sysUser/update', '17', '', null, '', '1', '2', '0', '0', null, null, '1', '2019-01-02 15:49:57', null, '0');
INSERT INTO `sys_menu` VALUES ('21', '用户管理:删除', 'sysUser:delete', '/sys/sysUser/delete', '17', '', null, '', '1', '2', '0', '0', null, null, '1', '2019-01-02 15:49:41', null, '0');
INSERT INTO `sys_menu` VALUES ('22', '操作日志', 'sysLog', '', '300', null, '1', 'md-ice-cream', '1', '1', '0', '0', null, null, '1', '2019-01-11 14:13:28', null, '0');
INSERT INTO `sys_menu` VALUES ('23', '操作日志:列表显示', 'sysLog:list', '/sys/sysLog/list', '22', null, '0', '', '1', '2', '0', '0', null, null, '1', '2019-01-11 14:13:34', null, '0');
INSERT INTO `sys_menu` VALUES ('231', '代码生成', 'generator', '', 'aa129466c26f4632864e9fc4b258d969', null, '1', 'md-color-wand', '1', '1', '0', '0', null, null, '1', '2020-04-15 11:18:22', null, '0');
INSERT INTO `sys_menu` VALUES ('232', '开发组件', 'component', '', '', null, '5', 'md-analytics', '1', '1', '0', '0', null, null, '1', '2020-01-19 10:11:44', null, '0');
INSERT INTO `sys_menu` VALUES ('233', 'FormDynamic', 'FormDynamic', '', '232', null, '1', 'ios-american-football', '1', '1', '0', '0', null, null, '1', '2018-12-28 17:10:03', null, '0');
INSERT INTO `sys_menu` VALUES ('234', 'CrudTreeViewPage', 'CrudTreeViewPage', '', '232', null, '2', 'ios-at', '1', '1', '0', '0', null, null, '1', '2018-12-28 17:09:58', null, '0');
INSERT INTO `sys_menu` VALUES ('235', 'CrudTablePage', 'CrudTablePage', '', '232', null, '3', 'ios-american-football', '1', '1', '0', '0', null, null, '1', '2018-12-28 17:09:52', null, '0');
INSERT INTO `sys_menu` VALUES ('236', 'CrudTreePage', 'CrudTreePage', '', '232', null, '4', 'ios-american-football', '1', '1', '0', '0', null, null, '1', '2018-12-28 17:09:46', null, '0');
INSERT INTO `sys_menu` VALUES ('237', 'PermsValidPage', 'PermsValidPage', '', '232', null, '5', 'ios-american-football', '1', '1', '0', '0', null, null, '1', '2018-12-28 17:09:42', null, '0');
INSERT INTO `sys_menu` VALUES ('238', 'DataSelectPage', 'DataSelectPage', '', '232', null, '6', 'ios-american-football', '1', '1', '0', '0', null, null, '1', '2018-12-29 11:41:20', null, '0');
INSERT INTO `sys_menu` VALUES ('239', 'CrudViewPage', 'CrudViewPage', '', '232', null, '7', 'md-analytics', '1', '1', '0', '0', null, null, '1', '2020-04-15 14:59:16', null, '1');
INSERT INTO `sys_menu` VALUES ('26', '操作日志:清除日志', 'sysLog:deleteAll', '/sys/sysLog/deleteAll', '22', null, '2', '', '1', '2', '0', '0', null, null, '1', '2019-01-11 14:13:48', null, '0');
INSERT INTO `sys_menu` VALUES ('27', 'swagger接口文档', 'swagger', 'http://localhost:9001/swagger-ui.html', '4d514d86422b4dc9a5511f4e1e9842d1', null, '11', 'ios-book', '1', '1', '1', '1', null, null, '1', '2020-01-19 13:38:29', null, '0');
INSERT INTO `sys_menu` VALUES ('28', '附件管理', 'sysAttach', '', '1', null, '8', 'ios-folder', '1', '1', '0', '0', null, null, '1', '2019-01-07 08:57:25', null, '0');
INSERT INTO `sys_menu` VALUES ('29', '定时任务', 'scheduleJob', '', 'ce98d4369a1d442897a1e660800e1ed3', null, '10', 'ios-timer', '1', '1', '0', '0', null, null, '1', '2019-01-07 09:05:29', null, '0');
INSERT INTO `sys_menu` VALUES ('2cc7e25ed8d64063bdd030788b99ab10', '角色管理:显示所有', 'sysRole:listAll', '/sys/sysRole/listAll', '12', null, '5', '', '1', '2', '0', '0', '1', '2019-01-10 11:02:29', '1', '2019-01-10 11:05:03', null, '0');
INSERT INTO `sys_menu` VALUES ('3', '菜单配置菜单:列表显示', 'sysMenu:list', '/sys/sysMenu/show', '2', '', '1', '', '1', '2', '0', '0', null, null, '1', '2019-01-04 17:10:07', null, '0');
INSERT INTO `sys_menu` VALUES ('30', '定时任务:列表显示', 'scheduleJob:list', '/quartz/scheduleJob/list', '29', null, '1', '', '1', '2', '0', '0', null, null, '1', '2019-01-15 10:38:33', null, '0');
INSERT INTO `sys_menu` VALUES ('300', '系统监控', 'sysMonitor', '', '', null, '1', 'md-analytics', '1', '1', '0', '0', '1', '2018-12-26 13:37:01', '1', '2019-01-11 14:00:07', null, '0');
INSERT INTO `sys_menu` VALUES ('303c0de619dc44cba357189d847c62cc', '菜单配置菜单:显示所有', 'sysMenu:listAll', '/sys/sysMenu/listAll', '2', null, '5', '', '1', '2', '0', '0', '1', '2019-01-10 09:05:07', null, '2019-01-10 09:05:07', null, '0');
INSERT INTO `sys_menu` VALUES ('31', '定时任务:添加', 'scheduleJob:create', '/quartz/scheduleJob/create', '29', null, '2', '', '1', '2', '0', '0', null, null, '1', '2019-01-15 10:39:23', null, '0');
INSERT INTO `sys_menu` VALUES ('32', '定时任务:修改', 'scheduleJob:update', '/quartz/scheduleJob/update', '29', null, '3', '', '1', '2', '0', '0', null, null, '1', '2019-01-15 10:39:18', null, '0');
INSERT INTO `sys_menu` VALUES ('33', '定时任务:删除', 'scheduleJob:delete', '/quartz/scheduleJob/delete', '29', null, '4', '', '1', '2', '0', '0', null, null, '1', '2019-01-15 10:39:12', null, '0');
INSERT INTO `sys_menu` VALUES ('379277e2f43843a39cdd912ad473259b', '定时任务：暂停', 'scheduleJob:pause', '/quartz/scheduleJob/pause', '29', null, '7', '', '1', '2', '0', '0', '1', '2019-01-15 16:32:09', null, '2019-01-15 16:32:09', null, '0');
INSERT INTO `sys_menu` VALUES ('387a566375bc459a975c637af31d7a69', '数据字典项:更新', 'sysDictItem:update', '/sys/sysDictItem/update', '7', null, null, '', '1', '2', '0', '0', '1', '2018-12-30 20:57:17', '1', '2019-01-02 15:52:26', null, '0');
INSERT INTO `sys_menu` VALUES ('4', '菜单配置:添加', 'sysMenu:create', '/sys/sysMenu/save', '2', '', '1', '', '1', '2', '0', '0', null, null, '1', '2019-01-04 17:10:21', null, '0');
INSERT INTO `sys_menu` VALUES ('434346b93071441c9b2a56ed8a42967e', '定时任务：运行', 'scheduleJob:resume', '/quartz/scheduleJob/resume', '29', null, '8', '', '1', '2', '0', '0', '1', '2019-01-15 16:39:52', null, '2019-01-15 16:39:52', null, '0');
INSERT INTO `sys_menu` VALUES ('4d514d86422b4dc9a5511f4e1e9842d1', '研发工具', 'tool', '', '', null, '4', 'md-build', '1', '1', '0', '0', '1', '2020-01-19 10:13:34', null, '2020-01-19 10:13:34', null, '0');
INSERT INTO `sys_menu` VALUES ('5', '菜单配置:更新', 'sysMenu:update', '/sys/sysMenu/update', '2', '', '1', '', '1', '2', '0', '0', null, null, '1', '2019-01-04 17:10:48', null, '0');
INSERT INTO `sys_menu` VALUES ('5785c2567ec34430a0742b7ddea1a7f1', '测试修改', 'testDict:update', '#', 'eeb8337985134f2c89af24727d81e88e', null, '3', '#', '1', '2', '0', '0', '1', '2020-04-15 11:13:03', '1', '2020-04-15 11:13:03', '测试修改', '0');
INSERT INTO `sys_menu` VALUES ('5f9dcbbeb8fd4248afc97bacee4b78c0', '测试删除', 'testDict:delete', '#', 'eeb8337985134f2c89af24727d81e88e', null, '4', '#', '1', '2', '0', '0', '1', '2020-04-15 11:13:03', '1', '2020-04-15 11:13:03', '测试删除', '0');
INSERT INTO `sys_menu` VALUES ('6', '菜单配置:删除', 'sysMenu:delete', '/sys/sysMenu/delete', '2', '', null, '', '1', '2', '0', '0', null, null, '1', '2019-01-04 17:10:53', null, '0');
INSERT INTO `sys_menu` VALUES ('6a7d49103285467cb21781cb7aaf8131', '数据字典项:显示', 'sysDictItem:list', '/sys/sysDictItem/list', '7', null, '2', '', '1', '2', '0', '0', '1', '2018-12-30 20:55:14', '1', '2019-01-02 15:52:09', null, '0');
INSERT INTO `sys_menu` VALUES ('7', '数据字典', 'sysDict', '', '1', '', '5', 'md-map', '1', '1', '0', '0', null, null, '1', '2019-01-07 08:58:11', null, '0');
INSERT INTO `sys_menu` VALUES ('71c8f4a4913840768c275dc91a4161f8', '测试新增', 'testDict:create', '#', 'eeb8337985134f2c89af24727d81e88e', null, '2', '#', '1', '2', '0', '0', '1', '2020-04-15 11:13:03', '1', '2020-04-15 11:13:03', '测试新增', '0');
INSERT INTO `sys_menu` VALUES ('8', '数据字典:列表显示', 'sysDict:list', '/sys/sysDict/list', '7', '', '2', '', '1', '2', '0', '0', null, null, '1', '2019-01-03 15:39:56', null, '0');
INSERT INTO `sys_menu` VALUES ('81a2f4eabce84a6e89e5809bb82ef458', '操作日志:日志内容', 'sysLog:showContent', '', '22', null, '0', '', '1', '2', '0', '0', '1', '2019-01-09 17:05:19', '1', '2019-01-11 14:13:41', null, '0');
INSERT INTO `sys_menu` VALUES ('885f4aacc9d24d17988886db64f35dba', 'sentinel控制台', 'sentinel', 'http://localhost:8080/#/login', '4d514d86422b4dc9a5511f4e1e9842d1', null, '2', 'logo-freebsd-devil', '1', '1', '0', '2', '1', '2020-01-19 09:50:37', '1', '2020-01-19 10:14:09', null, '0');
INSERT INTO `sys_menu` VALUES ('89409ba5530b4aa29479ed6cc1ab0989', '认证用户', 'sysOauthUsers', '', '300', null, '3', 'ios-bonfire', '1', '1', '0', '0', '1', '2019-01-11 23:05:22', '1', '2020-01-17 10:05:23', null, '1');
INSERT INTO `sys_menu` VALUES ('9', '数据字典:添加', 'sysDict:create', '/sys/sysDict/create', '7', '', '2', '', '1', '2', '0', '0', null, null, '1', '2019-01-03 15:39:24', null, '0');
INSERT INTO `sys_menu` VALUES ('96d227465f7f47e7b7f3a461d6791b8c', '附件垃圾删除', 'sysAttach:delete', '/sys/sysAttach/delete', '28', null, '0', '', '1', '2', '0', '0', '1', '2019-01-07 14:09:17', '1', '2019-01-07 14:15:54', null, '0');
INSERT INTO `sys_menu` VALUES ('aa129466c26f4632864e9fc4b258d969', '开发配置', '', '', '', null, '3', 'md-aperture', '1', '1', '0', '0', '1', '2019-01-09 16:46:28', null, '2019-01-09 16:46:28', null, '0');
INSERT INTO `sys_menu` VALUES ('ce98d4369a1d442897a1e660800e1ed3', '任务管理', 'schedule', '', '', null, '2', 'ios-speedometer', '1', '1', '0', '0', '1', '2019-01-02 17:06:44', '1', '2019-01-09 17:33:48', null, '0');
INSERT INTO `sys_menu` VALUES ('d26064fee80748c98a02b2cf6f0c2386', '认证用户:列表显示', 'sysOauthUsers:list', '/sys/sysOauthUsers/list', '89409ba5530b4aa29479ed6cc1ab0989', null, '1', '', '1', '2', '0', '0', '1', '2019-01-11 23:06:41', '1', '2020-01-17 10:05:18', null, '1');
INSERT INTO `sys_menu` VALUES ('e64af485a3cb49788193c0d6b8905025', '登录日志', 'sysLoginLog', '', '300', null, '2', 'ios-bug', '1', '1', '0', '0', '1', '2019-01-10 17:07:58', '1', '2019-01-10 17:09:07', null, '0');
INSERT INTO `sys_menu` VALUES ('e65bcaa97ada45019120c3c8a58ac218', '认证用户:移除', 'sysOauthUsers:removeOauths', '/sys/sysOauthUsers/removeOauths', '89409ba5530b4aa29479ed6cc1ab0989', null, '3', '', '1', '2', '0', '0', '1', '2019-01-12 21:36:43', '1', '2020-01-17 10:05:18', null, '1');
INSERT INTO `sys_menu` VALUES ('e776b5819fff4efba1a569d513f666e3', '登录日志:清除日志', 'sysLoginLog:deleteAll', '/sys/sysLoginLog/deleteAll', 'e64af485a3cb49788193c0d6b8905025', null, '1', '', '1', '2', '0', '0', '1', '2019-01-10 17:15:12', null, '2019-01-10 17:15:12', null, '0');
INSERT INTO `sys_menu` VALUES ('ed8e228f25dc4c9cabbd51df24d861bb', '登录日志:列表显示', 'sysLoginLog:list', '/sys/sysLoginLog/list', 'e64af485a3cb49788193c0d6b8905025', null, '0', '', '1', '2', '0', '0', '1', '2019-01-10 17:14:10', null, '2019-01-10 17:14:10', null, '0');
INSERT INTO `sys_menu` VALUES ('eeb8337985134f2c89af24727d81e88e', '测试', 'testDict', '/test/testDict', 'aa129466c26f4632864e9fc4b258d969', null, '2', 'ios-color-wand', '1', '1', '0', '0', '1', '2020-04-15 11:13:03', '1', '2020-04-15 11:17:03', '测试菜单', '0');
INSERT INTO `sys_menu` VALUES ('f0b63fca80d8462c86ea204139517623', '附件管理:列表显示', 'sysAttach:list', '/sys/sysAttach/list', '28', null, '1', '', '1', '2', '0', '0', '1', '2019-01-02 16:55:35', '1', '2019-01-02 17:02:03', null, '0');
INSERT INTO `sys_menu` VALUES ('ff48afb8b73941d49f614502aedf7bc4', 'nacos配置中心', 'nacos', 'http://localhost:8848/nacos', '4d514d86422b4dc9a5511f4e1e9842d1', null, '1', 'ios-basketball-outline', '1', '1', '1', '2', '1', '2020-01-18 17:33:13', '1', '2020-01-19 10:13:50', null, '0');

-- ----------------------------
-- Table structure for `sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `name` varchar(50) NOT NULL COMMENT '角色名称',
  `role` varchar(50) DEFAULT NULL COMMENT '角色标识',
  `description` varchar(200) NOT NULL COMMENT '角色描述',
  `status` char(1) NOT NULL COMMENT '角色状态',
  `is_sys` char(1) DEFAULT NULL COMMENT '是否内置',
  `main` varchar(20) DEFAULT NULL COMMENT '角色主页面',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `AK_U_Name` (`name`) USING BTREE,
  UNIQUE KEY `AK_U_Role` (`role`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='系统_角色';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '超级管理员', 'SURPER_USER', '超级管理员', '1', '1', null, null, null, '1', '2020-01-18 17:34:33', null, '0');
INSERT INTO `sys_role` VALUES ('6a4a9f7ecd3b4e68aa576302796e353a', '研发部人员', 'DEVELOPMENT_USER', '研发部人员', '1', null, null, '1', '2018-12-25 14:10:26', '1', '2020-01-16 15:36:41', null, '0');
INSERT INTO `sys_role` VALUES ('74965268e4994d5d94c8d7fbf19f92ee', '111', '11', '22', '1', null, null, '', '2020-01-16 09:44:04', '', '2020-01-16 09:49:06', null, '1');
INSERT INTO `sys_role` VALUES ('b2c533e760a64876ba81e11f0de9062e', '生产部人员', 'PRODUCTION_USER', '<p>生产部人员</p>', '1', null, null, '1', '2018-12-25 16:35:36', '1', '2019-01-11 13:59:48', null, '0');
INSERT INTO `sys_role` VALUES ('cc617f52ddde4b1283a32ba1f732c133', '普通管理员', 'COMMON_USER', '测试部人员', '1', null, null, '1', '2018-12-25 14:12:27', '1', '2020-01-16 15:36:45', null, '0');
INSERT INTO `sys_role` VALUES ('cffa46f3ef9b4899a2fef0224aeae985', '测试部人员', 'TEST_USER', '测试部人员', '1', null, null, '1', '2018-12-24 16:18:55', '1', '2020-01-16 16:17:52', null, '0');
INSERT INTO `sys_role` VALUES ('ea2fd02a1a0c4d6087a568374f3ea578', '财务部人员', 'FINANCE_USER', '财务部人员', '1', null, null, '1', '2018-12-25 14:10:54', '1', '2020-01-16 15:36:48', null, '0');
INSERT INTO `sys_role` VALUES ('feeaac3c53d441dd8914b8d025d2daaf', '人事部人员', 'PERSONNEL_USER', '<p>人事部人员</p>', '0', null, null, '1', '2018-12-25 14:11:23', '1', '2019-01-10 09:23:31', null, '0');

-- ----------------------------
-- Table structure for `sys_role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` varchar(32) NOT NULL,
  `role_id` varchar(32) NOT NULL,
  `menu_id` varchar(32) NOT NULL,
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `AK_U` (`role_id`,`menu_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='系统_角色资源';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('02117c69fb2844969f4204929f5b25e6', 'feeaac3c53d441dd8914b8d025d2daaf', '8', '1', '2018-12-27 17:25:18', null, '2018-12-27 17:25:18', null, '0');
INSERT INTO `sys_role_menu` VALUES ('045b5d34469c408d94fa280d3dfd00f9', 'cffa46f3ef9b4899a2fef0224aeae985', '236', '20863268736f4928b2240bd1c333f429', '2018-12-27 17:32:28', null, '2018-12-27 17:32:28', null, '0');
INSERT INTO `sys_role_menu` VALUES ('078f4a6e9a934588ae751f82cef5c7a7', '1', '96d227465f7f47e7b7f3a461d6791b8c', '1', '2019-01-07 14:09:36', null, '2019-01-07 14:09:36', null, '0');
INSERT INTO `sys_role_menu` VALUES ('0adf293872d74297a4abf5c8eee0b339', '1', '5f9dcbbeb8fd4248afc97bacee4b78c0', '1', '2020-04-15 11:17:32', null, '2020-04-15 11:17:32', null, '0');
INSERT INTO `sys_role_menu` VALUES ('1301b926b9a44781a2d5e40ebb201795', '1', 'ff48afb8b73941d49f614502aedf7bc4', '1', '2020-01-19 09:42:24', null, '2020-01-19 09:42:24', null, '0');
INSERT INTO `sys_role_menu` VALUES ('161b60fd07c74681a35137c0207a5c86', 'feeaac3c53d441dd8914b8d025d2daaf', '29', '1', '2018-12-27 17:25:18', null, '2018-12-27 17:25:18', null, '0');
INSERT INTO `sys_role_menu` VALUES ('1798bd5d8e034eefa064d89bc69dedae', '1', '00c2a9b405204e168b81ec5f4faeaecb', '1', '2018-12-30 21:02:05', null, '2018-12-30 21:02:05', null, '0');
INSERT INTO `sys_role_menu` VALUES ('1f486eee84514640959203ca1a0398d9', '1', '434346b93071441c9b2a56ed8a42967e', '1', '2019-01-15 16:40:48', null, '2019-01-15 16:40:48', null, '0');
INSERT INTO `sys_role_menu` VALUES ('222157b266fc49aa8c7e9b326c121c50', 'feeaac3c53d441dd8914b8d025d2daaf', '11', '1', '2018-12-27 17:25:18', null, '2018-12-27 17:25:18', null, '0');
INSERT INTO `sys_role_menu` VALUES ('2266c59beec14fb6bf23e7acf5efb14e', 'feeaac3c53d441dd8914b8d025d2daaf', '32', '1', '2018-12-27 17:25:18', null, '2018-12-27 17:25:18', null, '0');
INSERT INTO `sys_role_menu` VALUES ('285d96eadaeb4c50a98c6683bb24a261', 'feeaac3c53d441dd8914b8d025d2daaf', '9', '1', '2018-12-27 17:25:18', null, '2018-12-27 17:25:18', null, '0');
INSERT INTO `sys_role_menu` VALUES ('29', '1', '1', null, null, null, null, null, '0');
INSERT INTO `sys_role_menu` VALUES ('2928de766c1048d4a0416fc9f1dc491c', 'feeaac3c53d441dd8914b8d025d2daaf', '3', '1', '2018-12-27 17:25:18', null, '2018-12-27 17:25:18', null, '0');
INSERT INTO `sys_role_menu` VALUES ('29345e3396614afb8b69bf262862b44e', 'feeaac3c53d441dd8914b8d025d2daaf', '16', '1', '2018-12-27 17:25:18', null, '2018-12-27 17:25:18', null, '0');
INSERT INTO `sys_role_menu` VALUES ('2a0d46dd82574e07a8f60d1deaa828aa', 'feeaac3c53d441dd8914b8d025d2daaf', '6', '1', '2018-12-27 17:25:18', null, '2018-12-27 17:25:18', null, '0');
INSERT INTO `sys_role_menu` VALUES ('30', '1', '2', null, null, null, null, null, '0');
INSERT INTO `sys_role_menu` VALUES ('31', '1', '3', null, null, null, null, null, '0');
INSERT INTO `sys_role_menu` VALUES ('32', '1', '4', null, null, null, null, null, '0');
INSERT INTO `sys_role_menu` VALUES ('33', '1', '5', null, null, null, null, null, '0');
INSERT INTO `sys_role_menu` VALUES ('333d34edea86439ebd8f643d33298bf7', 'feeaac3c53d441dd8914b8d025d2daaf', '27', '1', '2018-12-27 17:25:18', null, '2018-12-27 17:25:18', null, '0');
INSERT INTO `sys_role_menu` VALUES ('34', '1', '6', null, null, null, null, null, '0');
INSERT INTO `sys_role_menu` VALUES ('34a27557afc4412b9931746031cd10a0', '1', '6a7d49103285467cb21781cb7aaf8131', '1', '2018-12-30 21:02:05', null, '2018-12-30 21:02:05', null, '0');
INSERT INTO `sys_role_menu` VALUES ('35', '1', '7', null, null, null, null, null, '0');
INSERT INTO `sys_role_menu` VALUES ('36', '1', '8', null, null, null, null, null, '0');
INSERT INTO `sys_role_menu` VALUES ('3668860d44f94b5fb8d53f63e9b5e391', '1', '238', '1', '2018-12-28 10:27:21', null, '2018-12-28 10:27:21', null, '0');
INSERT INTO `sys_role_menu` VALUES ('3680d344b5ff4e26b30da4eaa53dcb6c', '1', '1b676f49a44844bfbd280dc9666a2766', '1', '2019-01-15 10:39:50', null, '2019-01-15 10:39:50', null, '0');
INSERT INTO `sys_role_menu` VALUES ('37', '1', '9', null, null, null, null, null, '0');
INSERT INTO `sys_role_menu` VALUES ('37bb0a57311642cf9fb8763517090547', 'feeaac3c53d441dd8914b8d025d2daaf', '28', '1', '2018-12-27 17:25:18', null, '2018-12-27 17:25:18', null, '0');
INSERT INTO `sys_role_menu` VALUES ('38', '1', '10', null, null, null, null, null, '0');
INSERT INTO `sys_role_menu` VALUES ('39', '1', '11', null, null, null, null, null, '0');
INSERT INTO `sys_role_menu` VALUES ('3c99fe1586bd4410b41135e6be1ed299', 'feeaac3c53d441dd8914b8d025d2daaf', '238', '1', '2018-12-27 17:23:59', null, '2018-12-27 17:23:59', null, '0');
INSERT INTO `sys_role_menu` VALUES ('40', '1', '12', null, null, null, null, null, '0');
INSERT INTO `sys_role_menu` VALUES ('41', '1', '13', null, null, null, null, null, '0');
INSERT INTO `sys_role_menu` VALUES ('42', '1', '14', null, null, null, null, null, '0');
INSERT INTO `sys_role_menu` VALUES ('43', '1', '15', null, null, null, null, null, '0');
INSERT INTO `sys_role_menu` VALUES ('44', '1', '16', null, null, null, null, null, '0');
INSERT INTO `sys_role_menu` VALUES ('45', '1', '17', null, null, null, null, null, '0');
INSERT INTO `sys_role_menu` VALUES ('456e8ce09533426395d314c513d3bfe2', '1', 'f0b63fca80d8462c86ea204139517623', '1', '2019-01-02 16:59:04', null, '2019-01-02 16:59:04', null, '0');
INSERT INTO `sys_role_menu` VALUES ('46', '1', '18', null, null, null, null, null, '0');
INSERT INTO `sys_role_menu` VALUES ('463febddd89e47e488dc27d5accf85dc', 'cffa46f3ef9b4899a2fef0224aeae985', '232', '20863268736f4928b2240bd1c333f429', '2018-12-27 17:32:28', null, '2018-12-27 17:32:28', null, '0');
INSERT INTO `sys_role_menu` VALUES ('47', '1', '19', null, null, null, null, null, '0');
INSERT INTO `sys_role_menu` VALUES ('48', '1', '20', null, null, null, null, null, '0');
INSERT INTO `sys_role_menu` VALUES ('49', '1', '21', null, null, null, null, null, '0');
INSERT INTO `sys_role_menu` VALUES ('4c6f2eca108d4b2d9a0ff1b219aa66de', '1', '06e9f7c71b334cb1ab2f6285eb385aaa', '1', '2020-01-19 13:37:04', null, '2020-01-19 13:37:04', null, '0');
INSERT INTO `sys_role_menu` VALUES ('4e34fa30922a441aa8f6fc5c32205906', '1', '26', '1', '2018-12-29 14:10:07', null, '2018-12-29 14:10:07', null, '0');
INSERT INTO `sys_role_menu` VALUES ('52a84b36265644638fcf6c5a619bef3f', 'b2c533e760a64876ba81e11f0de9062e', '1', '1', '2019-01-07 14:15:23', null, '2019-01-07 14:15:23', null, '0');
INSERT INTO `sys_role_menu` VALUES ('53340e84f03941688b165f63fd3a4f5d', '1', 'eeb8337985134f2c89af24727d81e88e', '1', '2020-04-15 11:17:32', null, '2020-04-15 11:17:32', null, '0');
INSERT INTO `sys_role_menu` VALUES ('54f78004c65742f697c32552f274e9de', 'feeaac3c53d441dd8914b8d025d2daaf', '17', '1', '2018-12-27 17:25:18', null, '2018-12-27 17:25:18', null, '0');
INSERT INTO `sys_role_menu` VALUES ('55', '1', '27', null, null, null, null, null, '0');
INSERT INTO `sys_role_menu` VALUES ('56', '1', '28', null, null, null, null, null, '0');
INSERT INTO `sys_role_menu` VALUES ('56958081fa164618b13aaa579fba12d9', 'feeaac3c53d441dd8914b8d025d2daaf', '4', '1', '2018-12-27 17:25:18', null, '2018-12-27 17:25:18', null, '0');
INSERT INTO `sys_role_menu` VALUES ('57', '1', '231', null, null, null, null, null, '0');
INSERT INTO `sys_role_menu` VALUES ('5a24e97c9ce9455da0b0b9399e410adc', '1', '387a566375bc459a975c637af31d7a69', '1', '2018-12-30 21:02:05', null, '2018-12-30 21:02:05', null, '0');
INSERT INTO `sys_role_menu` VALUES ('5c8a8b5f77324afea5ada66538a18c03', 'cffa46f3ef9b4899a2fef0224aeae985', '233', '20863268736f4928b2240bd1c333f429', '2018-12-27 17:32:28', null, '2018-12-27 17:32:28', null, '0');
INSERT INTO `sys_role_menu` VALUES ('5e693a4abec842c596ef58e17893a2d4', 'feeaac3c53d441dd8914b8d025d2daaf', '31', '1', '2018-12-27 17:25:18', null, '2018-12-27 17:25:18', null, '0');
INSERT INTO `sys_role_menu` VALUES ('600cbbc832f24eed8909fcbdbd116104', 'feeaac3c53d441dd8914b8d025d2daaf', '14', '1', '2018-12-27 17:25:18', null, '2018-12-27 17:25:18', null, '0');
INSERT INTO `sys_role_menu` VALUES ('63c24ab63c4644f4bd7c3419b020084e', '1', '07ff4b87a8374a2cba8bdd8619a1e3ca', '1', '2019-01-15 13:43:07', null, '2019-01-15 13:43:07', null, '0');
INSERT INTO `sys_role_menu` VALUES ('63dd1a367d534d49bbedd75775f4545e', '1', '239', '1', '2018-12-28 10:27:21', null, '2018-12-28 10:27:21', null, '0');
INSERT INTO `sys_role_menu` VALUES ('66', '1', '29', null, null, null, null, null, '0');
INSERT INTO `sys_role_menu` VALUES ('663cdc22275a41998ac4ba4569506bd3', '1', '71c8f4a4913840768c275dc91a4161f8', '1', '2020-04-15 11:17:32', null, '2020-04-15 11:17:32', null, '0');
INSERT INTO `sys_role_menu` VALUES ('67', '1', '30', null, null, null, null, null, '0');
INSERT INTO `sys_role_menu` VALUES ('68', '1', '31', null, null, null, null, null, '0');
INSERT INTO `sys_role_menu` VALUES ('69', '1', '32', null, null, null, null, null, '0');
INSERT INTO `sys_role_menu` VALUES ('6baba35561924718bb1752ef478198ba', '1', '300', '1', '2018-12-29 14:10:07', null, '2018-12-29 14:10:07', null, '0');
INSERT INTO `sys_role_menu` VALUES ('6c9d5c3ad137497bb50721caac16b856', 'feeaac3c53d441dd8914b8d025d2daaf', '10', '1', '2018-12-27 17:25:18', null, '2018-12-27 17:25:18', null, '0');
INSERT INTO `sys_role_menu` VALUES ('70', '1', '33', null, null, null, null, null, '0');
INSERT INTO `sys_role_menu` VALUES ('71a5df54ed3b4c419b2cf18d0bd6324b', '1', '23', '1', '2018-12-29 14:10:07', null, '2018-12-29 14:10:07', null, '0');
INSERT INTO `sys_role_menu` VALUES ('71f11a0de65e455c84957faf250eae4f', '1', '379277e2f43843a39cdd912ad473259b', '1', '2019-01-15 16:40:48', null, '2019-01-15 16:40:48', null, '0');
INSERT INTO `sys_role_menu` VALUES ('72561d41280b403798493d8d1e621839', '1', 'e776b5819fff4efba1a569d513f666e3', '1', '2019-01-10 17:15:35', null, '2019-01-10 17:15:35', null, '0');
INSERT INTO `sys_role_menu` VALUES ('72f662f604b84294ac9ec785f39b5ee9', '1', '22', '1', '2018-12-29 14:10:07', null, '2018-12-29 14:10:07', null, '0');
INSERT INTO `sys_role_menu` VALUES ('786fd16b29894301949d1451927fa5a5', 'feeaac3c53d441dd8914b8d025d2daaf', '18', '1', '2018-12-27 17:25:18', null, '2018-12-27 17:25:18', null, '0');
INSERT INTO `sys_role_menu` VALUES ('7a4a16cd1a724c809e15f4479a30fa55', 'feeaac3c53d441dd8914b8d025d2daaf', '21', '1', '2018-12-27 17:25:18', null, '2018-12-27 17:25:18', null, '0');
INSERT INTO `sys_role_menu` VALUES ('7ae05c8284af4d5385f57c8922b857ec', '1', '233', '1', '2018-12-28 10:27:21', null, '2018-12-28 10:27:21', null, '0');
INSERT INTO `sys_role_menu` VALUES ('7e905842d3294c86805b8291f3a4eb50', 'cffa46f3ef9b4899a2fef0224aeae985', '238', '20863268736f4928b2240bd1c333f429', '2018-12-27 17:32:28', null, '2018-12-27 17:32:28', null, '0');
INSERT INTO `sys_role_menu` VALUES ('802f3a74b21c4eafae20231a3432d20c', 'cffa46f3ef9b4899a2fef0224aeae985', '235', '20863268736f4928b2240bd1c333f429', '2018-12-27 17:32:28', null, '2018-12-27 17:32:28', null, '0');
INSERT INTO `sys_role_menu` VALUES ('8668ba8be9ed458bbaf0d490b1c7e840', 'feeaac3c53d441dd8914b8d025d2daaf', '2', '1', '2018-12-27 17:25:18', null, '2018-12-27 17:25:18', null, '0');
INSERT INTO `sys_role_menu` VALUES ('873d1ae064ed4df49a3212170ed0932b', 'feeaac3c53d441dd8914b8d025d2daaf', '236', '1', '2018-12-27 17:23:59', null, '2018-12-27 17:23:59', null, '0');
INSERT INTO `sys_role_menu` VALUES ('87b1d6e7b44d4b8d823dc5baf935c55e', '1', '236', '1', '2018-12-28 10:27:21', null, '2018-12-28 10:27:21', null, '0');
INSERT INTO `sys_role_menu` VALUES ('8db55846b6d249f299519cf813216fd0', '1', '4d514d86422b4dc9a5511f4e1e9842d1', '1', '2020-01-19 10:14:57', null, '2020-01-19 10:14:57', null, '0');
INSERT INTO `sys_role_menu` VALUES ('9142f7bf8de24c3bb7cad0600a245bd7', '1', '1065e003054440b89e497c534080ace5', '1', '2019-01-03 13:28:57', null, '2019-01-03 13:28:57', null, '0');
INSERT INTO `sys_role_menu` VALUES ('974066bec0d34fabaadd11a84a0043d6', 'feeaac3c53d441dd8914b8d025d2daaf', '237', '1', '2018-12-27 17:23:59', null, '2018-12-27 17:23:59', null, '0');
INSERT INTO `sys_role_menu` VALUES ('9c24c95a4295439f8d08150952d6c163', 'cffa46f3ef9b4899a2fef0224aeae985', '237', '20863268736f4928b2240bd1c333f429', '2018-12-27 17:32:28', null, '2018-12-27 17:32:28', null, '0');
INSERT INTO `sys_role_menu` VALUES ('9dfa50009ce141e298c702768c1e710b', 'cffa46f3ef9b4899a2fef0224aeae985', '239', '20863268736f4928b2240bd1c333f429', '2018-12-27 17:32:28', null, '2018-12-27 17:32:28', null, '0');
INSERT INTO `sys_role_menu` VALUES ('a7bb33141e424740b413794b9c7bc528', '1', 'ce98d4369a1d442897a1e660800e1ed3', '1', '2019-01-02 17:08:18', null, '2019-01-02 17:08:18', null, '0');
INSERT INTO `sys_role_menu` VALUES ('aeac67da757d4a4d9eb788a93d1e3d10', 'feeaac3c53d441dd8914b8d025d2daaf', '13', '1', '2018-12-27 17:25:18', null, '2018-12-27 17:25:18', null, '0');
INSERT INTO `sys_role_menu` VALUES ('b1c5588289d74565a7a3910fac51d312', 'feeaac3c53d441dd8914b8d025d2daaf', '15', '1', '2018-12-27 17:25:18', null, '2018-12-27 17:25:18', null, '0');
INSERT INTO `sys_role_menu` VALUES ('b2d21c75566340c0adc9b37910e64963', '1', '885f4aacc9d24d17988886db64f35dba', '1', '2020-01-19 09:50:51', null, '2020-01-19 09:50:51', null, '0');
INSERT INTO `sys_role_menu` VALUES ('b517d828a278438d827b8873a47995ad', 'feeaac3c53d441dd8914b8d025d2daaf', '33', '1', '2018-12-27 17:25:18', null, '2018-12-27 17:25:18', null, '0');
INSERT INTO `sys_role_menu` VALUES ('b8f6db11fd6a4d369eae4c4db73a9164', 'feeaac3c53d441dd8914b8d025d2daaf', '20', '1', '2018-12-27 17:25:18', null, '2018-12-27 17:25:18', null, '0');
INSERT INTO `sys_role_menu` VALUES ('b9292264ff0548aea03f74f100e1b6cd', 'feeaac3c53d441dd8914b8d025d2daaf', '231', '1', '2018-12-27 17:25:18', null, '2018-12-27 17:25:18', null, '0');
INSERT INTO `sys_role_menu` VALUES ('bcdad8cea1d74c6791e14bbeb476bae7', 'feeaac3c53d441dd8914b8d025d2daaf', '5', '1', '2018-12-27 17:25:18', null, '2018-12-27 17:25:18', null, '0');
INSERT INTO `sys_role_menu` VALUES ('bda7728048ec4e71a754a76bf8d988e6', '1', 'e64af485a3cb49788193c0d6b8905025', '1', '2019-01-10 17:08:24', null, '2019-01-10 17:08:24', null, '0');
INSERT INTO `sys_role_menu` VALUES ('be79b63c249d46cfa18def2af4ca82ad', 'b2c533e760a64876ba81e11f0de9062e', 'f0b63fca80d8462c86ea204139517623', '1', '2019-01-07 14:15:23', null, '2019-01-07 14:15:23', null, '0');
INSERT INTO `sys_role_menu` VALUES ('bfc5c46984b94085bb51e2cb88991205', '1', '235', '1', '2018-12-28 10:27:21', null, '2018-12-28 10:27:21', null, '0');
INSERT INTO `sys_role_menu` VALUES ('c05ab0bcdbae4c2287e030cf6952effc', '1', '5785c2567ec34430a0742b7ddea1a7f1', '1', '2020-04-15 11:17:32', null, '2020-04-15 11:17:32', null, '0');
INSERT INTO `sys_role_menu` VALUES ('c0ef51b3f3264f62ad4e01430a4c9a2a', '1', '237', '1', '2018-12-28 10:27:21', null, '2018-12-28 10:27:21', null, '0');
INSERT INTO `sys_role_menu` VALUES ('c5dbbfc76b37455699e5267868e531db', 'b2c533e760a64876ba81e11f0de9062e', '28', '1', '2019-01-07 14:15:23', null, '2019-01-07 14:15:23', null, '0');
INSERT INTO `sys_role_menu` VALUES ('c61317161d3f48f6b39302986e66daf9', 'feeaac3c53d441dd8914b8d025d2daaf', '234', '1', '2018-12-27 17:23:59', null, '2018-12-27 17:23:59', null, '0');
INSERT INTO `sys_role_menu` VALUES ('cb13cc1648fe4f10b52c33cb486bf706', '1', '05b3d90607a443748949f279489409fd', '1', '2018-12-30 21:02:05', null, '2018-12-30 21:02:05', null, '0');
INSERT INTO `sys_role_menu` VALUES ('cc7ae87ed42443829345341dd6de0500', 'feeaac3c53d441dd8914b8d025d2daaf', '233', '1', '2018-12-27 17:25:18', null, '2018-12-27 17:25:18', null, '0');
INSERT INTO `sys_role_menu` VALUES ('d379f54dccd24aa6a94d1f93a9eb0970', 'feeaac3c53d441dd8914b8d025d2daaf', '1', '1', '2018-12-27 17:25:18', null, '2018-12-27 17:25:18', null, '0');
INSERT INTO `sys_role_menu` VALUES ('d396d55226554530b0912bb76a47dd0a', '1', '234', '1', '2018-12-28 10:27:21', null, '2018-12-28 10:27:21', null, '0');
INSERT INTO `sys_role_menu` VALUES ('d8afa82154b14900893f6e25a9054425', 'feeaac3c53d441dd8914b8d025d2daaf', '19', '1', '2018-12-27 17:25:18', null, '2018-12-27 17:25:18', null, '0');
INSERT INTO `sys_role_menu` VALUES ('d936c4df61204670a4f79fec0e680242', '1', '81a2f4eabce84a6e89e5809bb82ef458', '1', '2019-01-09 17:05:43', null, '2019-01-09 17:05:43', null, '0');
INSERT INTO `sys_role_menu` VALUES ('d9e2e50601d741868536fca5926d7cf6', '1', 'ed8e228f25dc4c9cabbd51df24d861bb', '1', '2019-01-10 17:15:35', null, '2019-01-10 17:15:35', null, '0');
INSERT INTO `sys_role_menu` VALUES ('dcf730c634544f858be138a47b6b855b', '1', '2cc7e25ed8d64063bdd030788b99ab10', '1', '2019-01-10 11:03:04', null, '2019-01-10 11:03:04', null, '0');
INSERT INTO `sys_role_menu` VALUES ('ddfd76a5969f4cd0bfec85a7cb368e8f', 'feeaac3c53d441dd8914b8d025d2daaf', '239', '1', '2018-12-27 17:23:59', null, '2018-12-27 17:23:59', null, '0');
INSERT INTO `sys_role_menu` VALUES ('e390571302da429aa27e332be92d1b36', '1', 'aa129466c26f4632864e9fc4b258d969', '1', '2019-01-09 16:47:13', null, '2019-01-09 16:47:13', null, '0');
INSERT INTO `sys_role_menu` VALUES ('e3ebfb1c324f4d0cbdeaea14d7310d79', '1', '232', '1', '2018-12-28 10:27:21', null, '2018-12-28 10:27:21', null, '0');
INSERT INTO `sys_role_menu` VALUES ('f0649871812c451f8a573a4ad1f46ac0', 'feeaac3c53d441dd8914b8d025d2daaf', '12', '1', '2018-12-27 17:25:18', null, '2018-12-27 17:25:18', null, '0');
INSERT INTO `sys_role_menu` VALUES ('f28ab0aeb91b493ab468dc42e448aa79', '1', '145ac6f04703492188cf9b9d17a3e66f', '1', '2020-04-15 11:17:32', null, '2020-04-15 11:17:32', null, '0');
INSERT INTO `sys_role_menu` VALUES ('f302bee05a6d422db44ffb684950b745', 'feeaac3c53d441dd8914b8d025d2daaf', '232', '1', '2018-12-27 17:23:59', null, '2018-12-27 17:23:59', null, '0');
INSERT INTO `sys_role_menu` VALUES ('fae52853ac0349e283da424f5fd7df31', 'feeaac3c53d441dd8914b8d025d2daaf', '30', '1', '2018-12-27 17:25:18', null, '2018-12-27 17:25:18', null, '0');
INSERT INTO `sys_role_menu` VALUES ('fd1c2e922bc645559e90d0658a451215', 'cffa46f3ef9b4899a2fef0224aeae985', '234', '20863268736f4928b2240bd1c333f429', '2018-12-27 17:32:28', null, '2018-12-27 17:32:28', null, '0');
INSERT INTO `sys_role_menu` VALUES ('fd6bd83d4e1943558db594bb71fdbcd4', 'feeaac3c53d441dd8914b8d025d2daaf', '7', '1', '2018-12-27 17:25:18', null, '2018-12-27 17:25:18', null, '0');
INSERT INTO `sys_role_menu` VALUES ('ff8b555fdb944d6fb83d4cd91bfb86bd', '1', '303c0de619dc44cba357189d847c62cc', '1', '2019-01-10 09:06:46', null, '2019-01-10 09:06:46', null, '0');

-- ----------------------------
-- Table structure for `sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` varchar(32) NOT NULL COMMENT '用户Id',
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `name` varchar(20) DEFAULT NULL COMMENT '姓名',
  `status` char(1) NOT NULL DEFAULT '1' COMMENT '用户状态【1启用、0禁用】',
  `password` varchar(100) NOT NULL COMMENT '密码',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(20) DEFAULT NULL COMMENT '手机号码',
  `is_lock` char(1) DEFAULT NULL COMMENT '是否锁定【1是、0否】',
  `lock_time` datetime DEFAULT NULL COMMENT '锁定时间',
  `login_count` bigint(20) DEFAULT NULL COMMENT '登录次数',
  `login_failure_count` bigint(20) DEFAULT NULL COMMENT '失败次数',
  `login_ip` varchar(100) DEFAULT NULL COMMENT '登录Ip',
  `login_time` varchar(50) DEFAULT NULL COMMENT '登录时间',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='系统_用户';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', '系统管理员', '1', 'e10adc3949ba59abbe56e057f20f883e', 'fans@163.com', '188000000', '0', null, '350', '105', null, '2020-04-18 20:24:41', '1', '2018-12-13 11:34:28', '1', '2020-01-17 10:17:20', null, '0');
INSERT INTO `sys_user` VALUES ('15efb286c56e4c74825043ebe45faeea', 'lisi', '李四', '1', 'e10adc3949ba59abbe56e057f20f883e', 'lisi@qq.com', '1850000000', '0', null, null, null, null, null, '', '2020-01-16 11:08:06', '1', '2020-01-16 15:36:26', null, '0');
INSERT INTO `sys_user` VALUES ('2', 'test1', '  测试1', '1', 'e10adc3949ba59abbe56e057f20f883e', 'test1@163.com', '188000000', '0', null, '8', '1', null, '2019-01-12 23:25:59', '1', '2018-12-15 13:53:34', '1', '2020-01-16 16:18:26', null, '0');
INSERT INTO `sys_user` VALUES ('20863268736f4928b2240bd1c333f429', 'zhangsan', '张三', '1', 'e10adc3949ba59abbe56e057f20f883e', 'zhangsan@qq.com', '18564522451', '0', null, '28', '12', null, '2019-01-13 22:52:29', '1', '2018-12-26 17:05:30', '1', '2019-01-11 20:24:04', null, '0');
INSERT INTO `sys_user` VALUES ('a7802891060c4e489e4ded12ee6c3db5', 'lisi', '李四', '1', 'e10adc3949ba59abbe56e057f20f883e', 'lisi@qq.com', '188888888', '0', null, null, null, null, null, '', '2020-01-16 11:22:23', '1', '2020-01-16 11:37:55', null, '1');
INSERT INTO `sys_user` VALUES ('bb370c873b9c42b286bc3b7aff61f0d7', 'test2', '  测试2', '1', 'e10adc3949ba59abbe56e057f20f883e', 'test2@qq.com', '145308822', '0', '2018-12-28 09:07:14', '3', '3', null, '2018-12-25 16:31:35', '1', '2018-12-24 16:29:11', '1', '2019-01-11 13:59:43', null, '0');

-- ----------------------------
-- Table structure for `sys_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `role_id` varchar(32) NOT NULL COMMENT '角色id',
  `user_id` varchar(32) NOT NULL COMMENT '用户id',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `AK_U` (`role_id`,`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='系统_用户角色';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('164ae47a351b4e38a1b355ea10bd047c', 'cc617f52ddde4b1283a32ba1f732c133', '15efb286c56e4c74825043ebe45faeea', '1', '2020-01-16 15:36:26', null, '2020-01-16 15:36:26', null, '0');
INSERT INTO `sys_user_role` VALUES ('61187d0a591b4f4181742e9e248f3067', 'cffa46f3ef9b4899a2fef0224aeae985', 'bb370c873b9c42b286bc3b7aff61f0d7', '1', '2018-12-27 17:40:59', null, '2018-12-27 17:40:59', null, '0');
INSERT INTO `sys_user_role` VALUES ('8f8e4fe7abc649daa6f38d669fc5823a', 'cffa46f3ef9b4899a2fef0224aeae985', '2', '1', '2018-12-29 14:13:25', null, '2018-12-29 14:13:25', null, '0');
INSERT INTO `sys_user_role` VALUES ('92865ee6952a4f5685ed66d69fd2d5dd', 'b2c533e760a64876ba81e11f0de9062e', '20863268736f4928b2240bd1c333f429', '1', '2018-12-29 14:13:40', null, '2018-12-29 14:13:40', null, '0');
INSERT INTO `sys_user_role` VALUES ('a812e36f79d74d14a65d5168bbc8b4e6', '1', '1', null, '2018-12-27 13:34:14', null, '2018-12-27 13:34:14', null, '0');
INSERT INTO `sys_user_role` VALUES ('c71cccbaf9f64d89970be6eb980d2874', '6a4a9f7ecd3b4e68aa576302796e353a', '15efb286c56e4c74825043ebe45faeea', '1', '2020-01-16 15:08:42', null, '2020-01-16 15:08:42', null, '0');

-- ----------------------------
-- Table structure for `sys_work_backlog`
-- ----------------------------
DROP TABLE IF EXISTS `sys_work_backlog`;
CREATE TABLE `sys_work_backlog` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `content` varchar(2000) DEFAULT NULL COMMENT '内容',
  `pictures` varchar(100) DEFAULT NULL COMMENT '图片',
  `start_time` varchar(20) DEFAULT NULL COMMENT '待办开始时间',
  `finish_time` varchar(20) DEFAULT NULL COMMENT '待办完成时间',
  `from_module` tinyint(4) DEFAULT NULL COMMENT '来源模块',
  `from_id` varchar(32) DEFAULT NULL COMMENT '来源id',
  `priority` char(1) DEFAULT '0' COMMENT '优先级（0=一般1=重要）',
  `user_ids` varchar(600) DEFAULT NULL COMMENT '待办用户',
  `status` char(1) DEFAULT '0' COMMENT '状态(0=未完成1=已完成)',
  `inform_days` int(11) DEFAULT NULL COMMENT '提前多少天提醒',
  `inform_enable` char(1) DEFAULT '1' COMMENT '开启提醒',
  `inform_type` varchar(20) DEFAULT NULL COMMENT '通知渠道',
  `inform_status` char(1) DEFAULT NULL COMMENT '通知状态（0=未通知1=已通知2=已提前通知）',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='待办事项';

-- ----------------------------
-- Records of sys_work_backlog
-- ----------------------------
INSERT INTO `sys_work_backlog` VALUES ('1', 'xxxxxxxx', 'xxxxxxxxxxxxxxxx', null, '2018-11-15 00:00:00', '', null, null, '1', '1', '0', null, '1', null, null, null, null, null, null, null, '0');
INSERT INTO `sys_work_backlog` VALUES ('2', '开学了', '开学了开学了', null, '2018-11-18 00:00:00', '2018-11-19 00:00:00', null, null, '0', '1', '0', null, '1', null, null, null, null, null, null, null, '0');
INSERT INTO `sys_work_backlog` VALUES ('3', '出事了', '出事了', null, '2018-11-20 00:00:00', '2018-11-20 21:00:00', null, null, '1', '1', '0', null, '1', null, null, null, null, null, null, null, '0');
INSERT INTO `sys_work_backlog` VALUES ('4', '出事了2', '出事了2', null, '2018-11-21 00:00:00', '2018-11-21 00:00:00', null, null, '0', '1', '0', null, '1', null, null, null, null, null, null, null, '0');
INSERT INTO `sys_work_backlog` VALUES ('5', 'xxxxxxxxxxx', 'xxxxxxxxxxxxxxxxxx', null, '2018-11-28 00:00:00', '2018-11-29 00:00:00', null, null, '0', '1', '0', null, '1', null, null, null, null, null, null, null, '0');
