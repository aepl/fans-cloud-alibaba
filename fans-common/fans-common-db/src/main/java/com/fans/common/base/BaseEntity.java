package com.fans.common.base;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.FieldFill;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @ClassName: BaseEntity
 * @Description: 实体基类
 * @author fanhaohao
 * @date 2019年12月13日 上午11:36:25
 * @param <T>
 */
public class BaseEntity<T extends Model> extends Model<T> implements Serializable {

	/**
	 * 删除标记（0：正常；1：删除；）
	 */
	public static final String DEL_FLAG_NORMAL = "0";
	public static final String DEL_FLAG_DELETE = "1";

    @TableField(exist = false)
    private static final long serialVersionUID = -34115333603863619L;

	@TableId(value = "id", type = IdType.UUID)
	protected String id; // 主键Id

	@TableField("create_by")
	protected String createBy; // 创建人

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(value = "create_date", fill = FieldFill.INSERT)
	protected Date createDate; // 创建日期

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@TableField(value = "update_date", fill = FieldFill.INSERT_UPDATE, update = "now()")
	protected Date updateDate; // 更新日期

	@TableField("update_by")
	protected String updateBy; // 更新人

	@TableField("remarks")
	protected String remarks; // 备注

	@TableField("del_flag")
	protected String delFlag; // 删除标记（0：正常；1：删除；2：审核）

	public BaseEntity() {
		super();
		this.delFlag = DEL_FLAG_NORMAL;
	}

    /**
	 * 插入之前执行方法，需要手动调用
	 */
	public void preInsert() {
		this.updateDate = new Date();
		this.createDate = this.updateDate;
	}

	/**
	 * 插入之前执行方法，需要手动调用
	 */
	public void preUpdate() {
		this.updateDate = new Date();
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getDelFlag() {
		if ("".equals(delFlag)) {
			delFlag = DEL_FLAG_NORMAL;
		}
		return delFlag;
	}

	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}

}
