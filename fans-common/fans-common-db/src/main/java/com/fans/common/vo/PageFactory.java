package com.fans.common.vo;

import com.baomidou.mybatisplus.plugins.Page;
import com.fans.common.constant.Order;
import com.fans.common.constant.SysConstant;
import com.fans.common.utils.BlankUtils;
import com.fans.common.utils.ServletUtils;

/**
 * @ClassName: PageFactory
 * @Description: Table默认的分页参数创建
 * @author fanhaohao
 * @date 2019年12月20日 上午9:23:50
 * @param <T>
 */
public class PageFactory<T> {

	public Page<T> page() {
		return page(null);
	}
	public Page<T> page(PageVo pv) {
		pv = getPageVo(pv);
		int size = BlankUtils.isBlank(pv.getPageSize()) ? 100000 : Integer.valueOf(pv.getPageSize());
		int current = BlankUtils.isBlank(pv.getPage()) ? 0 : Integer.valueOf(pv.getPage());
		String sort = pv.getSortField();
		String order = pv.getSortOrder();
		if (BlankUtils.isBlank(sort)) {
			Page<T> page = new Page<>(current, size);
			page.setOpenSort(false);
			return page;
		} else {
			Page<T> page = new Page<>(current, size, sort);
			if (Order.ASC.getDes().equals(order)) {
				page.setAsc(true);
			} else {
				page.setAsc(false);
			}
			return page;
		}
	}

	/**
	 * 从request中获取PageVo信息
	 */
	public PageVo getPageVo(PageVo pv) {
		PageVo pageVo = new PageVo();
		if(BlankUtils.isNotBlank(pv)){
			pageVo.setPageSize(BlankUtils.isBlank(pv.getPageSize())?ServletUtils.getHeaderParameterToInt(SysConstant.PAGE_SIZE):pv.getPageSize());
			pageVo.setPage(BlankUtils.isBlank(pv.getPage())?ServletUtils.getHeaderParameterToInt(SysConstant.PAGE):pv.getPage());
			pageVo.setSimple(BlankUtils.isBlank(pv.getSimple())?ServletUtils.getHeaderParameterToBoolean(SysConstant.SIMPLE):pv.getSimple());
			pageVo.setSortField(BlankUtils.isBlank(pv.getSortField())?ServletUtils.getHeaderParameter(SysConstant.SORT_FIELD):pv.getSortField());
			pageVo.setSortOrder(BlankUtils.isBlank(pv.getSortOrder())?ServletUtils.getHeaderParameter(SysConstant.SORT_ORDER):pv.getSortOrder());
		}else{
			pageVo.setPageSize(ServletUtils.getHeaderParameterToInt(SysConstant.PAGE_SIZE));
			pageVo.setPage(ServletUtils.getHeaderParameterToInt(SysConstant.PAGE));
			pageVo.setSimple(ServletUtils.getHeaderParameterToBoolean(SysConstant.SIMPLE));
			pageVo.setSortField(ServletUtils.getHeaderParameter(SysConstant.SORT_FIELD));
			pageVo.setSortOrder(ServletUtils.getHeaderParameter(SysConstant.SORT_ORDER));
		}
		return pageVo;
	}

}
