package com.fans.common.constant;

/**
 * 数据源
 * 
 * @author fanhaohao
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
