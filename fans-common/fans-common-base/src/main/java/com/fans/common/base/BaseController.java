package com.fans.common.base;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fans.common.constant.SysConstant;
import com.fans.common.utils.BlankUtils;
import com.fans.common.utils.ObjectUtils;
import com.fans.common.utils.ServletUtils;
import com.fans.common.utils.WriterUtil;

/**
 * @ClassName: BaseController
 * @Description: 基础控制器
 * @author fanhaohao
 * @date 2019年12月12日 下午2:41:10
 */

public abstract class BaseController{
	
	protected Logger logger = LoggerFactory.getLogger(getClass());
	/**
	 * 
	 * @Title：renderString  
	 * @Description: 客户端返回JSON字符串
	 * @author: fanhaohao
	 * @date 2019年12月13日 下午2:39:10 
	 * @param @param response
	 * @param @param object
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	protected String renderString(HttpServletResponse response, Object object) {
		return WriterUtil.renderString(response, object);
	}
	
	/**
	 * 
	 * @Title：renderStringDealNull  
	 * @Description: 空值时也返回属性
	 * @author: fanhaohao
	 * @date 2019年12月13日 下午2:39:24 
	 * @param @param response
	 * @param @param object
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	protected String renderStringDealNull(HttpServletResponse response, Object object) {
		return WriterUtil.renderStringDealNull(response, object);
    }
	
	/**
	 * 
	 * @Title：entityToMap  
	 * @Description: 实体转map 通过request获取condition，然后加入查询条件
	 * @author: fanhaohao
	 * @date 2020年1月14日 上午10:11:58 
	 * @param @param t
	 * @param @return 
	 * @return Map<String,Object> 
	 * @throws
	 */
	protected <T> Map<String, Object> entityToMap(T t) {
		return entityToMap(t, null);
	}
	
	/**
	 * 
	 * @Title：entityToMap  
	 * @Description: 实体转map，并加入condition通用查询条件
	 * @author: fanhaohao
	 * @date 2020年1月14日 上午10:08:29 
	 * @param @param t
	 * @param @param condition
	 * @param @return 
	 * @return Map<String,Object> 
	 * @throws
	 */
	protected <T> Map<String, Object> entityToMap(T t, String condition) {
		if (BlankUtils.isBlank(condition)) {
			condition = ServletUtils.getHeaderParameter(SysConstant.CONDITION);
		}
		Map<String, Object> map = ObjectUtils.javaBean2Map(t);
		if (BlankUtils.isNotBlank(condition)) {
			map.put(SysConstant.CONDITION, condition);
		}
		return map;
	}
	
	
}
