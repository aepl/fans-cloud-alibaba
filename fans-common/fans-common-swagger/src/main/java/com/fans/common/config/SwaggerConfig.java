package com.fans.common.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fans.common.properties.SwaggerProperties;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.DocExpansion;
import springfox.documentation.swagger.web.OperationsSorter;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger.web.UiConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * 
 * @ClassName: SwaggerConfig
 * @Description: Swagger配置
 * @author fanhaohao
 * @date 2020年1月18日 下午3:42:26
 */
@Configuration
@EnableSwagger2
@ConditionalOnProperty(name = "fans.swagger.enabled", matchIfMissing = true)
public class SwaggerConfig {

	@Autowired
	private SwaggerProperties swaggerProperties;

    @Bean
    public Docket createRestApi() {
		// 构造Authorization给测试的时候填写
		ParameterBuilder tokenPar = new ParameterBuilder();
		List<Parameter> pars = new ArrayList<Parameter>();
		tokenPar.name("Authorization").description("用户认证令牌(不需用户鉴权的不需要传)").modelRef(new ModelRef("string"))
				.parameterType("header")
				.required(false).build();
		pars.add(tokenPar.build());

        return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(defaultApiInfo())
                .select()
				.apis(RequestHandlerSelectors.basePackage(swaggerProperties.getBasePackage()))
				.apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
				.apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .paths(PathSelectors.any())
				.build().globalOperationParameters(pars);
    }

    /**
	 * 默认标题信息
	 *
	 * @return
	 */
	private ApiInfo defaultApiInfo() {
		// 大标题
		return new ApiInfoBuilder().title(swaggerProperties.getTitle())
				// 详细描述
				.description(swaggerProperties.getDescription()).contact(new Contact("", "", ""))
				// 版本
				.version(swaggerProperties.getVersion())
                .build();
    }

	@Bean
	UiConfiguration uiConfig() {
		return UiConfigurationBuilder.builder().docExpansion(DocExpansion.LIST).operationsSorter(OperationsSorter.ALPHA).build();
	}
}