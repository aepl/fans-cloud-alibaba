# fans-cloud-alibaba

#### 项目介绍
fans-cloud-alibaba是基于springcloud-Alibaba（springcloud ， springcloudalibaba，springcloud alibaba）框架，比较完善一套前后分离框架，以gateway为网关，nacos为配置中心和注册中心，sentinel流量控制、熔断降级、系统负载保护，skywalking链路追踪，性能监控等，jwt作为认证中心，缓存采用redis，数据库采用mysql，接口文档采取swagger描述，自带quartz定时器任务管理功能以及代码生成器功能

#### 前端代码地址
前端：https://gitee.com/fhh/fans-cloud-front

#### springboot版本代码地址
前端：https://gitee.com/fhh/fansbfront 后端：https://gitee.com/fhh/fans-boot

#### 架构拓扑
![微服务架构图](https://images.gitee.com/uploads/images/2020/0531/172641_8986bb4d_79358.png "微服务架构图 .png")




#### 软件模块
![模块架构图](https://images.gitee.com/uploads/images/2020/0524/184203_ad0529f9_79358.png "模块架构图.png")

#### 模块调用
![模块调用图](https://images.gitee.com/uploads/images/2020/0531/150415_d68a58e6_79358.png "模块调用图.png")

#### 功能模块
![fanscloud功能图](https://images.gitee.com/uploads/images/2020/0524/185145_8bc53cc2_79358.png "fanscloud功能图.png")

#### 安装运行教程（以下为Windows中启动方式，Linux下自行摸索）

 **1. 利用Git下载到自己的pc上** 

    - 前端下载地址：https://gitee.com/fhh/fans-cloud-front ，后端下载地址：https://gitee.com/fhh/fans-cloud-alibaba

 **2. 导入到eclipse或idea中** 
 
 **3. 数据库文件初始化** 
    
    - 找到以下数据库文件分别建库 fans_cloud ，nacos_devtest初始化数据库文件
![数据库文件](https://images.gitee.com/uploads/images/2020/0524/221452_b3eb5296_79358.png "数据库文件")

 **4. 启动nacos** 
 - 下载nacos-server-1.1.4，解压
- 修改nacos的配置文件application.properties（端口默认8848，文档尾部加入数据库配置）

```
spring.datasource.platform=mysql
db.num=1
db.url.0=jdbc:mysql://localhost:3306/nacos_devtest?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true
db.user=root
db.password=fanhaohao
```
    注：可参考博客：https://blog.csdn.net/qq_28851503/article/details/88767286

 - 运行bin下的startup.cmd
 - 浏览器中打开地址 http://localhost:8848/nacos（用户名，密码均为：nacos）

    注：具体的配置（端口，集群，数据存储位置等）参考官网文档，地址：https://nacos.io/zh-cn/docs/what-is-nacos.html

**5. 启动sentinel** 
  - 本项目中的sentinel是在Alibaba Sentinel-master基础上添加nacos数据源，sentinel操作面板更新数据同步到nacos的功能
  - 启动后访问地址 http://localhost:8080/#/login （用户名和密码均为：sentinel）
    
    注：具体的操作以及项目修改，请参考fans-sentinel-master，地址：https://gitee.com/fhh/fans-sentinel-master

**6. 启动skywalking** 
  - 到官网（http://skywalking.apache.org/downloads/）下载相应版本的sentinel，
  - 解压后打开  apache-skywalking-apm-bin\webapp 目录下的 webapp.yml  修改运行端口

```
server:
  port: 10800

collector:
  path: /graphql
  ribbon:
    ReadTimeout: 10000
    # Point to all backend's restHost:restPort, split by ,
    listOfServers: 127.0.0.1:12800
```
 - bin目录下 双击 startup.bat 启动 skywalking
 - 把 agent（探针）目录拷贝到项目中
 - 分别配置启动参数（vm arguments）如下
    
 fans-admin-consumer启动模块配置

```
-javaagent:F:\wk\fanscwk20191227\gitdown\fans-cloud-alibaba\fans-ext-skywalking\agent\skywalking-agent.jar
-Dskywalking.agent.service_name=fans-admin-consumer
-Dskywalking.collector.backend_service=localhost:11800
```
 fans-admin-provider启动模块配置

```
-javaagent:F:\wk\fanscwk20191227\gitdown\fans-cloud-alibaba\fans-ext-skywalking\agent\skywalking-agent.jar
-Dskywalking.agent.service_name=fans-admin-provider
-Dskywalking.collector.backend_service=localhost:11800
```
 fans-jwt-auth-consumer启动模块配置

```
-javaagent:F:\wk\fanscwk20191227\gitdown\fans-cloud-alibaba\fans-ext-skywalking\agent\skywalking-agent.jar
-Dskywalking.agent.service_name=fans-auth-consumer
-Dskywalking.collector.backend_service=localhost:11800
```
 fans-jwt-auth-provider启动模块配置

```
-javaagent:F:\wk\fanscwk20191227\gitdown\fans-cloud-alibaba\fans-ext-skywalking\agent\skywalking-agent.jar
-Dskywalking.agent.service_name=fans-auth-provider 
-Dskywalking.collector.backend_service=localhost:11800
```
 fans-gateway启动模块配置

```
-javaagent:F:\wk\fanscwk20191227\gitdown\fans-cloud-alibaba\fans-ext-skywalking\agent\skywalking-agent.jar
-Dskywalking.agent.service_name=fans-gateway 
-Dskywalking.collector.backend_service=localhost:11800
```
 - 启动后浏览器中http://localhost:10800/地址进行访问

    注：具体配置方法参考 https://www.jianshu.com/p/e81e35dc6406
    
**7. 启动网关** 
- 找到fans-gateway下的GatewayApplication进行启动，服务端口为：9001

**8. 启动后台接口提供者** 
- 找到fans-admin-provider下的AdminProviderApplication进行启动（可同时启动多个），服务端口为：9030

**9. 启动后台接口消费者**
- 找到fans-admin-consumer下的AdminConsumerApplication进行启动（可同时启动多个），服务端口为：9040

**10. 启动认证接口提供者**
- 找到fans-jwt-auth-provider下的AuthProviderApplication进行启动（可同时启动多个），服务端口为：9010

**11. 启动认证接口消费者**   
- 找到fans-jwt-auth-consumer下的AuthConsumerApplication进行启动（可同时启动多个），服务端口为：9020

**12. 启动前端** 
 - 前端启动（参考https://gitee.com/fhh/fans-cloud-front启动方式）
 - 登录用户名/密码：admin/123456


#### 运行效果
![登录](https://images.gitee.com/uploads/images/2020/0524/172651_cdf916ce_79358.png "登录.png")
![首页](https://images.gitee.com/uploads/images/2020/0524/174324_0fba81e7_79358.png "首页.png")
![用户管理](https://images.gitee.com/uploads/images/2020/0524/174651_7dc42de3_79358.png "用户管理.png")
![菜单管理](https://images.gitee.com/uploads/images/2020/0524/174750_21f8828e_79358.png "菜单管理.png")
![菜单图标配置](https://images.gitee.com/uploads/images/2020/0524/174920_73239bdc_79358.png "菜单图标配置.png")
![角色管理](https://images.gitee.com/uploads/images/2020/0524/174953_ba301f06_79358.png "角色管理.png")
![数据字典管理](https://images.gitee.com/uploads/images/2020/0524/175047_2ee44b71_79358.png "数据字典管理.png")
![字典项管理](https://images.gitee.com/uploads/images/2020/0524/175028_83bd3a2c_79358.png "字典项管理.png")
![操作日志](https://images.gitee.com/uploads/images/2020/0524/175130_24eb036f_79358.png "操作日志.png")
![登录日志](https://images.gitee.com/uploads/images/2020/0524/175151_129cb3e5_79358.png "登录日志.png")
![定时任务](https://images.gitee.com/uploads/images/2020/0524/175219_7cb0749c_79358.png "定时任务.png")
![代码生成](https://images.gitee.com/uploads/images/2020/0524/175312_e65e90ba_79358.png "代码生成.png")
![生成器生成的测试页面](https://images.gitee.com/uploads/images/2020/0524/175332_a3a09556_79358.png "生成器生成的测试页面.png")
![nacos管理](https://images.gitee.com/uploads/images/2020/0524/175415_fa3bef63_79358.png "nacos管理.png")
![sentinel控制台](https://images.gitee.com/uploads/images/2020/0524/175443_3eb17440_79358.png "sentinel控制台.png")
![skywalking首页](https://images.gitee.com/uploads/images/2020/0524/175528_c3e71843_79358.png "skywalking首页.png")
![skywalking网络追踪图](https://images.gitee.com/uploads/images/2020/0524/175548_53a6f4dd_79358.png "skywalking网络追踪图.png")
![skywalking网络拓扑图](https://images.gitee.com/uploads/images/2020/0531/150126_12a023df_79358.png "skywalking网络拓扑图.png")
![skywalking jvm监控](https://images.gitee.com/uploads/images/2020/0524/175644_8cd86064_79358.png "skywalkingjvm监控.png")
![接口doc文档（auth）](https://images.gitee.com/uploads/images/2020/0524/180229_c43267d9_79358.png "接口doc文档（auth）.png")
![接口doc文档（fans-admin）](https://images.gitee.com/uploads/images/2020/0524/180244_31472e0c_79358.png "接口doc文档（fans-admin）.png")
![swagger接口文档（fans-auth）](https://images.gitee.com/uploads/images/2020/0524/180313_c80d71c7_79358.png "swagger接口文档（fans-auth）.png")
![swagger接口文档（fans-admin）](https://images.gitee.com/uploads/images/2020/0524/180300_aca04333_79358.png "swagger接口文档（fans-admin）.png")

#### 前端页面参考网址
前端页面可以参考iview-admin：https://lison16.github.io/iview-admin-doc/#/

[![Fork me on Gitee](https://gitee.com/fhh/fans-cloud-alibaba/widgets/widget_3.svg)](https://gitee.com/fhh/fans-cloud-alibaba)