package com.fans.jwt.auth.auth.validator.impl;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fans.common.constant.FansRspCon;
import com.fans.common.constant.SysConstant;
import com.fans.common.redis.util.JedisUtils;
import com.fans.common.utils.BlankUtils;
import com.fans.common.utils.FansResp;
import com.fans.jwt.auth.auth.validator.IReqValidator;
import com.fans.jwt.auth.auth.vo.AuthRequestVo;
import com.fans.jwt.auth.feign.SysLoginLogService;
import com.fans.jwt.auth.feign.SysUserService;

/**
 * 账号密码验证
 *
 * @author fhh
 * @date 2019-08-23 12:34
 */
@Service
public class DbValidator implements IReqValidator {
	
	private static Logger logger = LoggerFactory.getLogger(DbValidator.class);
	
	@Autowired
	private SysUserService sysUserGssService; // 系统用户信息接口

	@Autowired
	private SysLoginLogService sysLoginLogService;// 系统用户登录日志接口

    @Override
    public FansResp validate(AuthRequestVo authRequestVo) {
    	String loginType=authRequestVo.getLogintype();
    	if(BlankUtils.isNotBlank(loginType)){//登录类型 1  后台系统用户登录
    		String userName=authRequestVo.getUserName();//用户名
    		String password=authRequestVo.getPassword();//密码
			if ("1".equals(loginType)) { // 1  后台系统用户登录
				String captcha = authRequestVo.getCaptcha();// 验证码
				String captchaId = authRequestVo.getCaptchaId();// 验证码id
				if (BlankUtils.isNotBlank(captcha) && BlankUtils.isNotBlank(captchaId)) {
					String key = SysConstant.CAPTCHA_KEY + captchaId;
					String captchaCache = (String) JedisUtils.getObject(key);
					if (captcha.equalsIgnoreCase(captchaCache)) {
						JedisUtils.deleteKeys(key);
						FansResp ar = sysUserGssService.getByLoginInfo(userName, password);
						Map<String, Object> loginDatas = (Map<String, Object>) ar.get("datas");
						// 查看登录用户和密码是否正确
						if (BlankUtils.isNotBlank(loginDatas)) {// 用户名和密码正确,登录成功
							Map<String, Object> userData = (Map<String, Object>) loginDatas.get("userData");
							if (BlankUtils.isNotBlank(userData)) {
								if (BlankUtils.isNotBlank(userData.get("password"))) {// 密码注释掉
									userData.put("password", "***");
								}
							}
							loginDatas.put("userData", userData);
							sysLoginLogService.saveLoginLog(userName, "1", "1", "登录成功！！");
							logger.info(userName + "登录成功！");
							return FansResp.successData(loginDatas);
						} else {// 登录失败
							sysLoginLogService.saveLoginLog(userName, "1", "2", "登录失败，用户名或密码错误！");
							logger.info(FansRspCon.FLAG_LOGIN_ERROR.getMsg());
							return FansResp.error(FansRspCon.FLAG_LOGIN_ERROR);
						}
					} else {
						sysLoginLogService.saveLoginLog(userName, "1", "2", "验证码错误！");
						return FansResp.error(FansRspCon.FLAG_LOGIN_ERROR);
					}
				} else {
					sysLoginLogService.saveLoginLog(userName, "1", "2", "验证码不能为空！");
					return FansResp.error(FansRspCon.FLAG_LOGIN_ERROR);
				}
			}else{//登录类型错误
				logger.info(FansRspCon.FLAG_LOGIN_TYPE_ERROR.getMsg());
				return FansResp.error(FansRspCon.FLAG_LOGIN_TYPE_ERROR);
			}
    	}else{//登录类型不能为空
    		logger.info(FansRspCon.FLAG_LOGIN_TYPE_NOTNULL.getMsg());
    		return FansResp.error(FansRspCon.FLAG_LOGIN_TYPE_NOTNULL);
    	}
    }
}
