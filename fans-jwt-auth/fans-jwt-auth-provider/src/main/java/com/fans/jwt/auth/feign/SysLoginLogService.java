package com.fans.jwt.auth.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fans.jwt.auth.feign.fallback.SysLoginLogServiceFallbackImpl;

/**
 * @ClassName: SysLoginLogService
 * @Description:用户登录日志数据
 * @author fanhaohao
 * @date 2020年1月30日 上午11:18:00
 */
@FeignClient(name = "fans-admin-provider", fallback = SysLoginLogServiceFallbackImpl.class)
public interface SysLoginLogService {
	
	/**
	 * 
	 * @Title：saveLoginLog
	 * @Description: 登录日志保存
	 * @author: fanhaohao
	 * @date 2020年1月17日 上午10:48:12 
	 * @param @param logonId
	 * @param @param operType
	 * @param @param resultType
	 * @param @param msg 
	 * @return void 
	 * @throws
	 */
	@RequestMapping(value = "/sys/sysLoginLog/saveLoginLog", method = RequestMethod.POST)
	public void saveLoginLog(@RequestParam(value = "logonId") String logonId, @RequestParam(value = "operType") String operType,
			@RequestParam(value = "resultType") String resultType,@RequestParam(value = "msg") String msg);
	
}
