package com.fans.jwt.auth.feign.fallback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.fans.common.utils.FansResp;
import com.fans.jwt.auth.feign.SysUserService;

/**
 * @ClassName: SysUserGssServiceFallbackImpl
 * @Description: 系统用户信息服务的fallback
 * @author fanhaohao
 * @date 2019年1月30日 上午11:18:18
 */
@Service
public class SysUserServiceFallbackImpl implements SysUserService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public FansResp getByLoginInfo(@RequestParam(value = "username") String username,@RequestParam(value = "password") String password) {
		logger.error("系统用户jwt登录调用{}异常:{}", "getByLoginInfo", username);
		return FansResp.error();
	}


}
