package com.fans.jwt.auth.feign.fallback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fans.jwt.auth.feign.SysLoginLogService;

/**
 * @ClassName: SysLoginLogServiceFallbackImpl
 * @Description: 用户登录日志数据服务的fallback
 * @author fanhaohao
 * @date 2020年1月30日 上午11:18:18
 */
@Service
public class SysLoginLogServiceFallbackImpl implements SysLoginLogService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public void saveLoginLog(String logonId, String operType, String resultType, String msg) {
		logger.error("用户登录日志保存{}异常，参数:logonId:{},operType:{},resultType:{},msg:{}", "saveLoginLog", logonId, operType,
				resultType, msg);
	}

}
