package com.fans.jwt.auth.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fans.common.utils.FansResp;
import com.fans.jwt.auth.feign.fallback.SysUserServiceFallbackImpl;

/**
 * @ClassName: SysUserGssService
 * @Description:系统用户信息
 * @author fanhaohao
 * @date 2019年1月30日 上午11:18:00
 */
@FeignClient(name = "fans-admin-provider", fallback = SysUserServiceFallbackImpl.class)
public interface SysUserService {
	/**
	 * 系统用户登录
	 * 
	 * @param username 登录账号或者手机号或者邮箱  password：登录密码
	 * @return 系统用户信息
	 */
	@RequestMapping(value = { "/sys/sysUser/getByLoginInfo" }, method = RequestMethod.GET)
	public FansResp getByLoginInfo(@RequestParam(value = "username") String username,@RequestParam(value = "password") String password);

}
