package com.fans.jwt.auth.auth.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 认证的请求dto
 *
 * @author fanhaohao
 * @Date 2019/8/24 14:00
 */
@ApiModel
public class AuthRequestVo{

	/**
	 * 用户名
	 */
	@ApiModelProperty("用户名")
    private String userName;
    /**
     * 密码
     */
	@ApiModelProperty("密码")
    private String password;
	
	/**
     * 验证码
     */
	@ApiModelProperty("验证码")
    private String captcha;

	/**
	 * 验证码id
	 */
	@ApiModelProperty("验证码id")
	private String captchaId;
    
    /**
	 * 登录类型 1admin 后台登录
	 */
	@ApiModelProperty("登录类型  1admin 后台登录")
    private String logintype;

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

	public String getCaptcha() {
		return captcha;
	}

	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}

	public void setLogintype(String logintype) {
		this.logintype = logintype;
	}

	public String getLogintype() {
		return logintype;
	}

	public String getCaptchaId() {
		return captchaId;
	}

	public void setCaptchaId(String captchaId) {
		this.captchaId = captchaId;
	}

}
