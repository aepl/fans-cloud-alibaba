package com.fans.jwt.auth.auth.validator.impl;

import org.springframework.stereotype.Service;

import com.fans.common.constant.FansRspCon;
import com.fans.common.utils.FansResp;
import com.fans.jwt.auth.auth.validator.IReqValidator;
import com.fans.jwt.auth.auth.vo.AuthRequestVo;

/**
 * 直接验证账号密码是不是admin
 *
 * @author fanhaohao
 * @date 2019-08-23 12:34
 */
@Service
public class SimpleValidator implements IReqValidator {

    private static String USER_NAME = "admin";

    private static String PASSWORD = "admin";

    @Override
	public FansResp validate(AuthRequestVo authRequestVo) {
        String userName = authRequestVo.getUserName();
        String password = authRequestVo.getPassword();
        if (USER_NAME.equals(userName) && PASSWORD.equals(password)) {
			return FansResp.success();
        } else {
			return FansResp.error(FansRspCon.FLAG_LOGIN_ERROR);
        }
    }
}
