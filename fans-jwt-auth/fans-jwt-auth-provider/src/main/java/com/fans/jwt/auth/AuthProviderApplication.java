package com.fans.jwt.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @ClassName: AuthServerApplication
 * @Description: jwt认证服务端
 * @author fanhaohao
 * @date 2019年1月24日 上午9:48:16
 */

@SpringBootApplication(scanBasePackages = { "com.fans" })
@EnableDiscoveryClient
@EnableFeignClients
public class AuthProviderApplication {
	public static void main(String[] args) {
		SpringApplication.run(AuthProviderApplication.class, args);
    }
}