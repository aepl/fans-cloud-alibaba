package com.fans.jwt.auth.feign;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fans.common.utils.FansResp;
import com.fans.jwt.auth.feign.fallback.AuthServiceFallbackImpl;

/**
 * @ClassName: AuthService
 * @Description:用户认证服务,退出等服务
 * @author fanhaohao
 * @date 2020年4月18日 上午11:18:00
 */
@FeignClient(name = "fans-jwt-auth-provider", fallback = AuthServiceFallbackImpl.class)
public interface AuthService {
	/**
	 * 用户登录认证
	 * 
	 * @param authRequestVo
	 * @return 用户信息以及用户认证信息
	 */
	@RequestMapping(value = { "/auth/auth" }, method = RequestMethod.POST)
	public FansResp auth(@RequestParam("sysUser") Map<String, Object> authRequestVo);
	
	/**
	 * 用户注销
	 * 
	 * @return FansResp
	 */
	@RequestMapping(value = { "/auth/logout" }, method = RequestMethod.POST)
	public FansResp logout();
	
	/**
	 * 用户是否登录
	 * 
	 * @return FansResp
	 */
	@RequestMapping(value = { "/auth/isLogined" }, method = RequestMethod.POST)
	public FansResp isLogined();

}
