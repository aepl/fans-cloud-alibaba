package com.fans.jwt.auth.feign.fallback;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.fans.common.utils.FansResp;
import com.fans.jwt.auth.feign.AuthService;

/**
 * @ClassName: AuthServiceFallbackImpl
 * @Description: 用户认证服务,退出等服务的fallback
 * @author fanhaohao
 * @date 2020年4月20日 上午11:18:18
 */
@Service
public class AuthServiceFallbackImpl implements AuthService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
	 * 用户登录认证
	 * 
	 * @param authRequestVo
	 * @return 用户信息以及用户认证信息
	 */
	@Override
	public FansResp auth(Map<String, Object> authRequestVo) {
		logger.error("用户登录认证{}异常参数:{}", "auth", JSON.toJSONString(authRequestVo));
		return FansResp.error();
	}

	/**
	 * 用户注销
	 * 
	 * @return FansResp
	 */
	@Override
	public FansResp logout() {
		logger.error("用户注销{}异常", "logout");
		return FansResp.error();
	}

	/**
	 * 用户是否登录
	 * 
	 * @return FansResp
	 */
	@Override
	public FansResp isLogined() {
		logger.error("用户是否登录{}异常", "isLogined");
		return FansResp.error();
	}


}
