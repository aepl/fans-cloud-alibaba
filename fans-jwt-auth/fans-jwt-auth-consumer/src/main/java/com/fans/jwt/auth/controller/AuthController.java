package com.fans.jwt.auth.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.fans.common.base.BaseController;
import com.fans.common.utils.FansResp;
import com.fans.jwt.auth.feign.AuthService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * @ClassName: AuthController
 * @Description: 用户认证服务接口
 * @author fanhaohao
 * @date 2020年1月16日 下午5:30:38
 */
@RestController
@RequestMapping("/auth")
@Api(value = "用户认证服务接口", description = "用户认证服务接口")
public class AuthController extends BaseController {

    @Autowired
	private AuthService authService;
    

    /**
     * 
     * @Title：auth  
     * @Description: 用户认证服务接口
     * @author: fanhaohao
     * @date 2020年4月18日 下午5:39:16 
     * @param @param entityMap
     * @param @return 
     * @return FansResp 
     * @throws
     */
	@RequestMapping(value = "/auth", method = RequestMethod.POST)
	@ApiOperation(value = "用户登录验证接口", notes = "用户登录验证接口  <br>" + " 1，登录参数 <br>"
			+ " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;logintype 登录类型: 1 为后台系统用户认证登录 ；username：用户名；password：密码；capture：验证码；captchaId：验证码ID）<br>"
			+ " 2，fans后台系统用户认证 返回值描述如下：<br>"
			+ " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{\"code\":返回状态码,\"msg\":返回消息,\"datas(返回数据)\":{\"menuData(菜单数据)\":[],\"userData(用户数据)\":{},\"permsData(按钮权限数据)\":[],\"authData(认证信息，token以及过期时间等数据)\":{}}}<br>", response = String.class)
	@ApiImplicitParams({
			@ApiImplicitParam(name = "authRequestVo",dataType="authRequestVo", value = "authRequestVo实体的json类型( fans后台系统用户认证方式)<br>{\"userName\":\"admin\",\"password\":\"123456\",\"logintype\":\"1\",\"captcha\":\"9aqd\",\"captchaId\":\"4a9cd3a2-ab45-46c6-9870-b91659c88fd51579329664992\"}") })
	public FansResp auth(@RequestBody Map<String, Object> authRequestVo) {
		return authService.auth(authRequestVo);
	}

	
	/**
	 * 
	 * @Title：logout  
	 * @Description: 用户注销
	 * @author: fanhaohao
	 * @date 2020年4月18日 下午5:50:25 
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	@ResponseBody
	@ApiOperation(value = "用户注销", notes = "用户注销")
	public FansResp logout() {
		return authService.logout();
	}

	/**
	 * 
	 * @Title：isLogined  
	 * @Description: 是否登录
	 * @author: fanhaohao
	 * @date 2020年4月18日 下午5:50:41 
	 * @param @param request
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@ResponseBody
	@RequestMapping(value = "/isLogined", method = RequestMethod.POST)
	@ApiOperation(value = "是否登录", notes = "是否登录")
	public FansResp isLogined() {
		return authService.isLogined();
	}
}
