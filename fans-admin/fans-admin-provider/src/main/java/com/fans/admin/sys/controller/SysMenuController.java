package com.fans.admin.sys.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.annotations.Log;
import com.fans.admin.annotations.RequiredPermission;
import com.fans.admin.sys.cache.SysMenuCache;
import com.fans.admin.sys.entity.SysMenu;
import com.fans.admin.sys.service.ISysMenuService;
import com.fans.admin.sys.warpper.SysMenuWarpper;
import com.fans.common.base.BaseController;
import com.fans.common.utils.BlankUtils;
import com.fans.common.utils.FansResp;
import com.fans.common.utils.jwt.TokenUtil;
import com.fans.common.vo.PageFactory;
import com.fans.common.vo.PageVo;

/**
 * 
 * @ClassName: SysMenuController
 * @Description: 系统_资源数据接口
 * @author fanhaohao
 * @date 2020年1月16日 下午5:40:31
 */
@RestController
@RequestMapping("/sys/sysMenu")
public class SysMenuController extends BaseController {

    @Autowired
    private ISysMenuService sysMenuService;
    
    /**
     * 
     * @Title：getById  
     * @Description: 根据id获取系统_资源
     * @author: fanhaohao
     * @date 2020年1月16日 下午5:40:44 
     * @param @param id
     * @param @return 
     * @return FansResp 
     * @throws
     */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public FansResp getById(String id) {
		SysMenu sysMenu = null;
		try {
			if (BlankUtils.isNotBlank(id)) {
				sysMenu = sysMenuService.selectById(id);
			}else{
				return FansResp.error("根据id获取系统_资源异常！ID不能为空");
			}
		} catch (Exception e) {
			logger.error("根据id获取系统_资源异常", e);
			return FansResp.error("根据id获取系统_资源异常！");
		}
		return FansResp.successData(sysMenu);
	}

	/**
	 * 
	 * @Title：list  
	 * @Description: 获取系统_资源列表
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午5:41:13 
	 * @param @param sysMenu
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@RequiredPermission("sysMenu:list")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public Object list(SysMenu sysMenu) {
		PageVo pv = new PageVo();
		pv.setSortField("weight");
		pv.setSortOrder("asc");
		Page<SysMenu> page = new PageFactory<SysMenu>().page(pv);
		List<Map<String, Object>> result = sysMenuService.list(page, entityToMap(sysMenu));
		page.setRecords((List<SysMenu>) new SysMenuWarpper(result).warp());
		return FansResp.successData(page);
	}

	/**
	 * 
	 * @Title：listAll  
	 * @Description: 获取系统_资源列表
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午5:41:23 
	 * @param @param response
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@RequiredPermission("sysMenu:listAll")
	@RequestMapping(value = "/listAll", method = RequestMethod.GET)
	public Object listAll(HttpServletResponse response) {
		return FansResp.successData(SysMenuCache.getAllMenuList());
	}

	/**
	 * 
	 * @Title：create  
	 * @Description: 新建系统_资源
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午5:41:32 
	 * @param @param sysMenu
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@Log(operation = "新建系统_资源")
	@RequiredPermission("sysMenu:create")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public FansResp create(SysMenu sysMenu) {
		try {
			sysMenu.preInsert();
			sysMenu.setCreateBy(TokenUtil.getUserId());
			sysMenuService.insert(sysMenu);
		} catch (Exception e) {
			logger.error("新建系统_资源异常", e);
			return FansResp.error("新建系统_资源异常！");
		}
		return FansResp.successData(sysMenu);
	}
	
	/**
	 * 
	 * @Title：update  
	 * @Description: 修改系统_资源
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午5:41:43 
	 * @param @param sysMenu
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@Log(operation = "修改系统_资源")
	@RequiredPermission("sysMenu:update")
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public FansResp update(SysMenu sysMenu) {
		try {
			if (BlankUtils.isNotBlank(sysMenu.getId())) {
				sysMenu.preUpdate();
				sysMenu.setUpdateBy(TokenUtil.getUserId());
				sysMenuService.updateById(sysMenu);
			}else{
				return FansResp.error("修改系统_资源异常！ID不能为空");
			}
		} catch (Exception e) {
			logger.error("修改系统_资源异常", e);
			return FansResp.error("修改系统_资源异常！");
		}
		return FansResp.successData(sysMenu);
	}
	
	/**
	 * 
	 * @Title：delete  
	 * @Description: 删除系统_资源
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午5:41:55 
	 * @param @param ids
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@Log(operation = "删除系统_资源")
	@RequiredPermission("sysMenu:delete")
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public FansResp delete(@PathVariable(value = "ids") String ids) {
		try {
			if (BlankUtils.isNotBlank(ids)) {
				String[] idsArr = ids.split(",");
				sysMenuService.deleteBatchByIdsLogic(Arrays.asList(idsArr));
			}else{
				return FansResp.error("删除系统_资源异常！IDS不能为空");
			}
		} catch (Exception e) {
			logger.error("删除系统_资源异常", e);
			return FansResp.error("删除系统_资源异常！");
		}
		return FansResp.success();
	}
	
	
}
