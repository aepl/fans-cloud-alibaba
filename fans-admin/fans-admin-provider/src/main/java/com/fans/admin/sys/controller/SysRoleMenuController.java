package com.fans.admin.sys.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.annotations.Log;
import com.fans.admin.annotations.RequiredPermission;
import com.fans.admin.sys.cache.SysMenuCache;
import com.fans.admin.sys.entity.SysRoleMenu;
import com.fans.admin.sys.service.ISysRoleMenuService;
import com.fans.admin.sys.warpper.SysRoleMenuWarpper;
import com.fans.common.base.BaseController;
import com.fans.common.utils.BlankUtils;
import com.fans.common.utils.FansResp;
import com.fans.common.utils.jwt.TokenUtil;
import com.fans.common.vo.PageFactory;

/**
 * 
 * @ClassName: SysRoleMenuController  
 * @Description: 系统_角色资源数据接口 
 * @author fanhaohao
 * @date 2020年1月17日 上午9:12:33
 */
@RestController
@RequestMapping("/sys/sysRoleMenu")
public class SysRoleMenuController extends BaseController {

    @Autowired
    private ISysRoleMenuService sysRoleMenuService;
    

    /**
     * 
     * @Title：getById  
     * @Description: 根据id获取系统_角色资源
     * @author: fanhaohao
     * @date 2020年1月17日 上午9:12:45 
     * @param @param id
     * @param @return 
     * @return FansResp 
     * @throws
     */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public FansResp getById(String id) {
		SysRoleMenu sysRoleMenu = null;
		try {
			if (BlankUtils.isNotBlank(id)) {
				sysRoleMenu = sysRoleMenuService.selectById(id);
			}else{
				return FansResp.error("根据id获取系统_角色资源异常！ID不能为空");
			}
		} catch (Exception e) {
			logger.error("根据id获取系统_角色资源异常", e);
			return FansResp.error("根据id获取系统_角色资源异常！");
		}
		return FansResp.successData(sysRoleMenu);
	}

	/**
	 * 
	 * @Title：list  
	 * @Description: 获取系统_角色资源列表
	 * @author: fanhaohao
	 * @date 2020年1月17日 上午9:12:54 
	 * @param @param sysRoleMenu
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@RequiredPermission("sysRoleMenu:list")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public Object list(SysRoleMenu sysRoleMenu) {
		Page<SysRoleMenu> page = new PageFactory<SysRoleMenu>().page();
		List<Map<String, Object>> result = sysRoleMenuService.list(page, entityToMap(sysRoleMenu));
		page.setRecords((List<SysRoleMenu>) new SysRoleMenuWarpper(result).warp());
		return FansResp.successData(page);
	}

	/**
	 * 
	 * @Title：save  
	 * @Description: 新建系统_角色资源
	 * @author: fanhaohao
	 * @date 2020年1月17日 上午9:13:05 
	 * @param @param sysRoleMenu
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@Log(operation = "新建系统_角色资源")
	@RequiredPermission("sysRoleMenu:create")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public FansResp save(SysRoleMenu sysRoleMenu) {
		try {
			sysRoleMenu.preInsert();
			sysRoleMenu.setCreateBy(TokenUtil.getUserId());
			sysRoleMenuService.insert(sysRoleMenu);
		} catch (Exception e) {
			logger.error("新建系统_角色资源异常", e);
			return FansResp.error("新建系统_角色资源异常！");
		}
		return FansResp.successData(sysRoleMenu);
	}
	
	/**
	 * 
	 * @Title：update  
	 * @Description: 修改系统_角色资源
	 * @author: fanhaohao
	 * @date 2020年1月17日 上午9:13:15 
	 * @param @param sysRoleMenu
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@Log(operation = "修改系统_角色资源")
	@RequiredPermission("sysRoleMenu:update")
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public FansResp update(SysRoleMenu sysRoleMenu) {
		try {
			if (BlankUtils.isNotBlank(sysRoleMenu.getId())) {
				sysRoleMenu.preUpdate();
				sysRoleMenu.setUpdateBy(TokenUtil.getUserId());
				sysRoleMenuService.updateById(sysRoleMenu);
			}else{
				return FansResp.error("修改系统_角色资源异常！ID不能为空");
			}
		} catch (Exception e) {
			logger.error("修改系统_角色资源异常", e);
			return FansResp.error("修改系统_角色资源异常！");
		}
		return FansResp.successData(sysRoleMenu);
	}
	
	/**
	 * 
	 * @Title：delete  
	 * @Description: 系统_角色资源
	 * @author: fanhaohao
	 * @date 2020年1月17日 上午9:13:29 
	 * @param @param ids
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@Log(operation = "删除系统_角色资源")
	@RequiredPermission("sysRoleMenu:delete")
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public FansResp delete(@PathVariable(value = "ids") String ids) {
		try {
			if (BlankUtils.isNotBlank(ids)) {
				String[] idsArr = ids.split(",");
				sysRoleMenuService.deleteBatchByIdsLogic(Arrays.asList(idsArr));
			}else{
				return FansResp.error("删除系统_角色资源异常！IDS不能为空");
			}
		} catch (Exception e) {
			logger.error("删除系统_角色资源异常", e);
			return FansResp.error("删除系统_角色资源异常！");
		}
		return FansResp.success();
	}
	
	/**
	 * 
	 * @Title：getRoleMenu  
	 * @Description: 获得所有资源菜单与角色所拥有的资源列表
	 * @author: fanhaohao
	 * @date 2020年1月17日 上午9:13:40 
	 * @param @param roleId
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/getRoleMenu", method = RequestMethod.GET)
	public FansResp getRoleMenu(String roleId) {
		Map<String, Object> datas = new HashMap<>();
		// 获取该角色所拥有的资源id
		List<String> menuIds = sysRoleMenuService.getMenuIdsByRoleId(roleId);
		datas.put("selected", menuIds);
		// 获取 树菜单list；
		Map<String, Object> map = new HashMap<>();
		map.put("delFlag", "0");
		List<Map<String, Object>> menuList = SysMenuCache.getAllMenuList();
		datas.put("records", menuList);
		return FansResp.successData(datas);
    }

	/**
	 * 
	 * @Title：saveRoleMenu  
	 * @Description: 保存角色资源菜单
	 * @author: fanhaohao
	 * @date 2020年1月17日 上午9:13:50 
	 * @param @param ids
	 * @param @param roleId
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/saveRoleMenu", method = RequestMethod.POST)
	public FansResp saveRoleMenu(String ids, String roleId) {
		try {
			if (BlankUtils.isNotBlank(roleId)) {
				//保存角色资源菜单
				sysRoleMenuService.saveRoleMenu(ids, roleId);
			}else{
				return FansResp.error("保存角色资源菜单异常！角色id不能为空");
			}
		} catch (Exception e) {
			logger.error("保存角色资源菜单异常", e);
			return FansResp.error("保存角色资源菜单异常！");
		}
		return FansResp.success();
	}
}
