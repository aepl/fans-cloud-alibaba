package com.fans.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * @ClassName: AdminProviderApplication
 * @Description: 启动类
 * @author fanhaohao
 * @date 2018年12月5日 下午3:13:28
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@ComponentScan(basePackages = { "com.fans" })
public class AdminProviderApplication {
	public static void main(String[] args) {
		SpringApplication.run(AdminProviderApplication.class, args);
	}

}
