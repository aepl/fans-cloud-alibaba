package com.fans.admin.sys.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.annotations.Log;
import com.fans.admin.annotations.RequiredPermission;
import com.fans.admin.sys.entity.SysUser;
import com.fans.admin.sys.service.ISysUserService;
import com.fans.admin.sys.warpper.SysUserWarpper;
import com.fans.common.base.BaseController;
import com.fans.common.constant.SysConstant;
import com.fans.common.utils.BlankUtils;
import com.fans.common.utils.FansResp;
import com.fans.common.utils.SecurityUtils;
import com.fans.common.utils.jwt.TokenUtil;
import com.fans.common.vo.PageFactory;

/**
 * @ClassName: SysUserController
 * @Description: 系统_用户数据接口
 * @author fanhaohao
 * @date 2020年1月7日 下午5:42:14
 */
@RestController
@RequestMapping("/sys/sysUser")
public class SysUserController extends BaseController {

    @Autowired
    private ISysUserService sysUserService;

	/**
	 * @Title：getById
	 * @Description: 根据id获取系统_用户
	 * @author: fanhaohao
	 * @date 2020年1月7日 下午5:43:08
	 * @param @param id
	 * @param @param request
	 * @param @param response
	 * @param @return
	 * @return String
	 * @throws
	 */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public FansResp getById(String id) {
		SysUser sysUser = null;
		try {
			if (BlankUtils.isNotBlank(id)) {
				sysUser = sysUserService.selectById(id);
			}else{
				return FansResp.error("根据id获取系统_用户异常！ID不能为空");
			}
		} catch (Exception e) {
			logger.error("根据id获取系统_用户异常", e);
			return FansResp.error("根据id获取系统_用户异常！");
		}
		return FansResp.successData(sysUser);
	}

	/**
	 * 
	 * @Title：list  
	 * @Description: 获取系统_用户列表
	 * @author: fanhaohao
	 * @date 2020年1月15日 下午4:08:59 
	 * @param @param sysUser
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@RequiredPermission("sysUser:list")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public FansResp list(SysUser sysUser) {
		Page<SysUser> page = new PageFactory<SysUser>().page();
		List<Map<String, Object>> result = sysUserService.list(page, entityToMap(sysUser));
		page.setRecords((List<SysUser>) new SysUserWarpper(result).warp());
		return FansResp.successData(page);
	}

	/**
	 * 
	 * @Title：create  
	 * @Description: 创建系统_用户
	 * @author: fanhaohao
	 * @date 2020年1月16日 上午10:51:53 
	 * @param @param sysUser
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@Log(operation = "创建系统_用户")
	@RequiredPermission("sysUser:create")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public FansResp create(SysUser sysUser) {
		try {
			if ("1".equals(sysUser.getIsLock())) {// 是否锁定，添加锁定时间
				sysUser.setLockTime(new Date());
			}
			sysUser.setPassword(SecurityUtils.MD5Encode(SysConstant.INIT_PWD));// 设置默认密码为123456
			sysUser.preInsert();
			sysUser.setCreateBy(TokenUtil.getUserId());
			sysUserService.saveUserInfo(sysUser);
		} catch (Exception e) {
			logger.error("创建系统_用户异常", e);
			return FansResp.error("创建系统_用户异常！");
		}
		return FansResp.successData(sysUser);
	}
	
	/**
	 * 
	 * @Title：update  
	 * @Description: 修改系统_用户
	 * @author: fanhaohao
	 * @date 2020年1月16日 上午10:52:08 
	 * @param @param sysUser
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@Log(operation = "修改系统_用户")
	@RequiredPermission("sysUser:update")
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public FansResp update(SysUser sysUser) {
		try {
			if (BlankUtils.isNotBlank(sysUser.getId())) {
				if ("1".equals(sysUser.getIsLock())) {// 是否锁定，锁定时更新锁定时间
					sysUser.setLockTime(new Date());
				}
				sysUser.preUpdate();
				sysUser.setUpdateBy(TokenUtil.getUserId());
				sysUserService.updateUserInfo(sysUser);
			}else{
				return FansResp.error("修改系统_用户异常！ID不能为空");
			}
		} catch (Exception e) {
			logger.error("修改系统_用户异常", e);
			return FansResp.error("修改系统_用户异常！");
		}
		return FansResp.successData(sysUser);
	}
	
	/**
	 * 
	 * @Title：delete  
	 * @Description: 删除系统_用户
	 * @author: fanhaohao
	 * @date 2020年1月16日 上午10:52:18 
	 * @param @param ids
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@Log(operation = "删除系统_用户")
	@RequiredPermission("sysUser:delete")
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public FansResp delete(@PathVariable(value = "ids") String ids) {
		try {
			if (BlankUtils.isNotBlank(ids)) {
				String[] idsArr = ids.split(",");
				sysUserService.deleteBatchByIdsLogic(Arrays.asList(idsArr));
			}else{
				return FansResp.error("删除系统_用户异常！IDS不能为空");
			}
		} catch (Exception e) {
			logger.error("删除系统_用户异常", e);
			return FansResp.error("删除系统_用户异常！");
		}
		return FansResp.success();
	}
	
	/**
	 * 
	 * @Title：resetPwd  
	 * @Description: 重置密码
	 * @author: fanhaohao
	 * @date 2020年1月16日 上午10:52:35 
	 * @param @param id
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@Log(operation = "重置密码")
	@RequestMapping(value = "/resetPwd", method = RequestMethod.PUT)
	public FansResp resetPwd(String id) {
        try {
			if (BlankUtils.isNotBlank(id)) {
				sysUserService.resetPwd(id);
				return FansResp.success();
			} else {
				return FansResp.error("系统_用户重置密码异常！IDS不能为空");
			}
		} catch (Exception e) {
			logger.error("系统_用户重置密码异常！", e);
			return FansResp.error("系统_用户重置密码异常！");
		}
    }
	
	/**
	 * 
	 * @Title：updatePwd  
	 * @Description: 修改密码
	 * @author: fanhaohao
	 * @date 2020年1月16日 上午10:52:49 
	 * @param @param pwd
	 * @param @param newpwd
	 * @param @param newpwd2
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@Log(operation = "修改密码")
	@RequestMapping(value = "/updatePwd", method = RequestMethod.PUT)
	public FansResp updatePwd(String pwd, String newpwd, String newpwd2) {
		try {
			if (BlankUtils.isBlank(pwd)) {
				return FansResp.error("系统_用户修改密码异常！原密码不能为空");
			}
			if (BlankUtils.isBlank(newpwd)) {
				return FansResp.error("系统_用户修改密码异常！新密码不能为空");
			}
			if (BlankUtils.isBlank(newpwd2)) {
				return FansResp.error("系统_用户修改密码异常！重复新密码不能为空");
			}
			if (!newpwd.equals(newpwd2)) {
				return FansResp.error("系统_用户修改密码异常！新密码和重复新密码必须一致");
			}
			String userId = TokenUtil.getUserId();
			SysUser sysUser = sysUserService.selectById(userId);
			if (SecurityUtils.MD5Encode(pwd).equals(sysUser.getPassword())) {
				sysUserService.updatePwd(userId, newpwd);
				return FansResp.success();
			}else{
				return FansResp.error("系统_用户修改密码异常！原密码不正确");
			}
		} catch (Exception e) {
			logger.error("系统_用户重置密码异常！", e);
			return FansResp.error("系统_用户重置密码异常！");
		}
	}
	
	
	/**
	 * 
	 * @Title：getByLoginInfo  
	 * @Description: 根据username和password以及验证码查询用户
	 * @author: fanhaohao
	 * @date 2020年1月12日 下午6:28:46 
	 * @param @param username
	 * @param @param password
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/getByLoginInfo", method = RequestMethod.GET)
	public FansResp getByLoginInfo(String username, String password,HttpServletRequest request) {
		Map<String, Object> loginDatas = new HashMap<>();
		try {
			SysUser sysUser = null;
			if (BlankUtils.isNotBlank(username) && BlankUtils.isNotBlank(password)) {
				// 根据用户名和密码查询
				sysUser = sysUserService.getByUsernameAndPassword(username, password);
				if (BlankUtils.isBlank(sysUser)) {// 根据邮箱和密码查询
					sysUser = sysUserService.getByEmailAndPassword(username, password);
				}
				if (BlankUtils.isBlank(sysUser)) {// 根据手机号和密码查询
					sysUser = sysUserService.getByMobileAndPassword(username, password);
				}
			}
			if (BlankUtils.isNotBlank(sysUser)) {// 不为空表示登录成功
				loginDatas = sysUserService.getLoginDatas(username, true);
			}
		} catch (Exception e) {
			logger.error("根据username和password查询用户异常", e);
			return FansResp.error("根据username和password查询用户异常");
		}
		return FansResp.successData(loginDatas);
	}
	
}
