package com.fans.admin.sys.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.annotations.Log;
import com.fans.admin.annotations.RequiredPermission;
import com.fans.admin.sys.entity.SysAttach;
import com.fans.admin.sys.service.ISysAttachService;
import com.fans.admin.sys.warpper.SysAttachWarpper;
import com.fans.common.base.BaseController;
import com.fans.common.utils.BlankUtils;
import com.fans.common.utils.FansResp;
import com.fans.common.vo.PageFactory;

/**
 * 
 * @ClassName: SysAttachController
 * @Description: 系统_附件数据接口
 * @author fanhaohao
 * @date 2020年1月16日 下午5:31:08
 */
@RestController
@RequestMapping("/sys/sysAttach")
public class SysAttachController extends BaseController {
    @Autowired
    private ISysAttachService sysAttachService;
    
    /**
     * 
     * @Title：getById  
     * @Description: 根据id获取系统_附件
     * @author: fanhaohao
     * @date 2020年1月16日 下午5:31:17 
     * @param @param id
     * @param @return 
     * @return FansResp 
     * @throws
     */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public FansResp getById(String id) {
		SysAttach sysAttach = null;
		try {
			if (BlankUtils.isNotBlank(id)) {
				sysAttach = sysAttachService.selectById(id);
			}else{
				return FansResp.error("根据id获取系统_附件异常！ID不能为空");
			}
		} catch (Exception e) {
			logger.error("根据id获取系统_附件异常", e);
			return FansResp.error("根据id获取系统_附件异常！");
		}
		return FansResp.successData(sysAttach);
	}

	/**
	 * 
	 * @Title：list  
	 * @Description: 获取系统_附件列表
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午5:31:29 
	 * @param @param sysAttach
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@RequiredPermission("sysAttach:list")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public Object list(SysAttach sysAttach) {
		Page<SysAttach> page = new PageFactory<SysAttach>().page();
		List<Map<String, Object>> result = sysAttachService.list(page, entityToMap(sysAttach));
		page.setRecords((List<SysAttach>) new SysAttachWarpper(result).warp());
		return FansResp.successData(page);
	}

	/**
	 * 
	 * @Title：delete  
	 * @Description: 删除系统_附件
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午5:31:38 
	 * @param @param ids
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@Log(operation = "删除系统_附件")
	@RequiredPermission("sysAttach:delete")
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public FansResp delete(@PathVariable(value = "ids") String ids) {
		try {
			if (BlankUtils.isNotBlank(ids)) {
				String[] idsArr = ids.split(",");
				sysAttachService.deleteBatchByIdsLogic(Arrays.asList(idsArr));
			}else{
				return FansResp.error("删除系统_附件异常！IDS不能为空");
			}
		} catch (Exception e) {
			logger.error("删除系统_附件异常", e);
			return FansResp.error("删除系统_附件异常！");
		}
		return FansResp.success();
	}
	
}
