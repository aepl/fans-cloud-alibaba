package com.fans.admin.sys.controller;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.annotations.Log;
import com.fans.admin.annotations.RequiredPermission;
import com.fans.admin.sys.entity.SysDictItem;
import com.fans.admin.sys.service.ISysDictItemService;
import com.fans.admin.sys.warpper.SysDictItemWarpper;
import com.fans.common.base.BaseController;
import com.fans.common.utils.BlankUtils;
import com.fans.common.utils.FansResp;
import com.fans.common.utils.jwt.TokenUtil;
import com.fans.common.vo.PageFactory;
import com.fans.common.vo.PageVo;

/**
 * 
 * @ClassName: SysDictItemController
 * @Description: 字典项数据接口
 * @author fanhaohao
 * @date 2020年1月16日 下午5:33:00
 */
@RestController
@RequestMapping("/sys/sysDictItem")
public class SysDictItemController extends BaseController {

    @Autowired
    private ISysDictItemService sysDictItemService;
    
    /**
     * 
     * @Title：getById  
     * @Description: 根据id获取字典项
     * @author: fanhaohao
     * @date 2020年1月16日 下午5:33:10 
     * @param @param id
     * @param @return 
     * @return FansResp 
     * @throws
     */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public FansResp getById(String id) {
		SysDictItem sysDictItem = null;
		try {
			if (BlankUtils.isNotBlank(id)) {
				sysDictItem = sysDictItemService.selectById(id);
			}else{
				return FansResp.error("根据id获取字典项异常！ID不能为空");
			}
		} catch (Exception e) {
			logger.error("根据id获取字典项异常", e);
			return FansResp.error("根据id获取字典项异常！");
		}
		return FansResp.successData(sysDictItem);
	}

	/**
	 * 
	 * @Title：list  
	 * @Description: 获取字典项列表
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午5:33:22 
	 * @param @param sysDictItem
	 * @param @param pv
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@RequiredPermission("sysDictItem:list")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public Object list(SysDictItem sysDictItem, PageVo pv) {
		pv.setSortField("showOrder");
		pv.setSortOrder("asc");
		Page<SysDictItem> page = new PageFactory<SysDictItem>().page(pv);
		List<Map<String, Object>> result = sysDictItemService.list(page, entityToMap(sysDictItem));
		page.setRecords((List<SysDictItem>) new SysDictItemWarpper(result).warp());
		return FansResp.successData(page);
	}
	
	/**
	 * 
	 * @Title：create  
	 * @Description: 新建字典项
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午5:33:33 
	 * @param @param sysDictItem
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@Log(operation = "新建字典项")
	@RequiredPermission("sysDictItem:create")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public FansResp create(SysDictItem sysDictItem) {
		try {
			if(BlankUtils.isBlank(sysDictItem.getShowOrder())){
				BigDecimal shoswOrder = new BigDecimal("0");
				sysDictItem.setShowOrder(shoswOrder);
			}
			sysDictItem.preInsert();
			sysDictItem.setCreateBy(TokenUtil.getUserId());
			sysDictItemService.insert(sysDictItem);
		} catch (Exception e) {
			logger.error("新建字典项异常", e);
			return FansResp.error("新建字典项异常！");
		}
		return FansResp.successData(sysDictItem);
	}
	
	/**
	 * 
	 * @Title：update  
	 * @Description: 修改字典项
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午5:33:46 
	 * @param @param sysDictItem
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@Log(operation = "修改字典项")
	@RequiredPermission("sysDictItem:update")
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public FansResp update(SysDictItem sysDictItem) {
		try {
			if (BlankUtils.isNotBlank(sysDictItem.getId())) {
				sysDictItem.preUpdate();
				sysDictItem.setUpdateBy(TokenUtil.getUserId());
				sysDictItemService.updateById(sysDictItem);
			}else{
				return FansResp.error("修改字典项异常！ID不能为空");
			}
		} catch (Exception e) {
			logger.error("修改字典项异常", e);
			return FansResp.error("修改字典项异常！");
		}
		return FansResp.successData(sysDictItem);
	}
	
	/**
	 * 
	 * @Title：delete  
	 * @Description: 删除字典项
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午5:33:55 
	 * @param @param ids
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@Log(operation = "删除字典项")
	@RequiredPermission("sysDictItem:delete")
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public FansResp delete(@PathVariable(value = "ids") String ids) {
		try {
			if (BlankUtils.isNotBlank(ids)) {
				String[] idsArr = ids.split(",");
				sysDictItemService.deleteBatchByIdsLogic(Arrays.asList(idsArr));
			}else{
				return FansResp.error("删除字典项异常！IDS不能为空");
			}
		} catch (Exception e) {
			logger.error("删除字典项异常", e);
			return FansResp.error("删除字典项异常！");
		}
		return FansResp.success();
	}
	
	
}
