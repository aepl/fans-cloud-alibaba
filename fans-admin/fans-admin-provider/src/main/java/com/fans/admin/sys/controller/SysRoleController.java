package com.fans.admin.sys.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.annotations.Log;
import com.fans.admin.annotations.RequiredPermission;
import com.fans.admin.sys.cache.SysRoleCache;
import com.fans.admin.sys.entity.SysRole;
import com.fans.admin.sys.service.ISysRoleService;
import com.fans.admin.sys.warpper.SysRoleWarpper;
import com.fans.common.base.BaseController;
import com.fans.common.utils.BlankUtils;
import com.fans.common.utils.FansResp;
import com.fans.common.utils.jwt.TokenUtil;
import com.fans.common.vo.PageFactory;

/**
 * 
 * @ClassName: SysRoleController  
 * @Description: 系统_角色数据接口 
 * @author fanhaohao
 * @date 2020年1月16日 下午5:42:15
 */
@RestController
@RequestMapping("/sys/sysRole")
public class SysRoleController extends BaseController {

    @Autowired
    private ISysRoleService sysRoleService;
    
    /**
     * 
     * @Title：getById  
     * @Description: 根据id获取系统_角色
     * @author: fanhaohao
     * @date 2020年1月16日 下午5:42:26 
     * @param @param id
     * @param @return 
     * @return FansResp 
     * @throws
     */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public FansResp getById(String id) {
		SysRole sysRole = null;
		try {
			if (BlankUtils.isNotBlank(id)) {
				sysRole = sysRoleService.selectById(id);
			}else{
				return FansResp.error("根据id获取系统_角色异常！ID不能为空");
			}
		} catch (Exception e) {
			logger.error("根据id获取系统_角色异常", e);
			return FansResp.error("根据id获取系统_角色异常！");
		}
		return FansResp.successData(sysRole);
	}

	/**
	 * 
	 * @Title：list  
	 * @Description: 获取系统_角色列表
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午5:42:44 
	 * @param @param sysRole
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@RequiredPermission("sysRole:list")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public FansResp list(SysRole sysRole) {
		Page<SysRole> page = new PageFactory<SysRole>().page();
		List<Map<String, Object>> result = sysRoleService.list(page, entityToMap(sysRole));
		page.setRecords((List<SysRole>) new SysRoleWarpper(result).warp());
		return FansResp.successData(page);
	}

	
	/**
	 * 
	 * @Title：listAll  
	 * @Description: 获取系统_角色列表
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午5:42:54 
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@RequiredPermission("sysRole:listAll")
	@RequestMapping(value = "/listAll", method = RequestMethod.GET)
	public Object listAll() {
		return FansResp.successData(SysRoleCache.getAllRoleList());
	}

	/**
	 * 
	 * @Title：create  
	 * @Description: 新建系统_角色
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午5:43:03 
	 * @param @param sysRole
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@Log(operation = "新建系统_角色")
	@RequiredPermission("sysRole:create")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public FansResp create(SysRole sysRole) {
		try {
			sysRole.preInsert();
			sysRole.setCreateBy(TokenUtil.getUserId());
			sysRoleService.insert(sysRole);
		} catch (Exception e) {
			logger.error("新建系统_角色异常", e);
			return FansResp.error("新建系统_角色异常！");
		}
		return FansResp.successData(sysRole);
	}
	
	/**
	 * 
	 * @Title：update  
	 * @Description: 修改系统_角色
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午5:43:13 
	 * @param @param sysRole
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@Log(operation = "修改系统_角色")
	@RequiredPermission("sysRole:update")
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public FansResp update(SysRole sysRole) {
		try {
			if (BlankUtils.isNotBlank(sysRole.getId())) {
				sysRole.preUpdate();
				sysRole.setUpdateBy(TokenUtil.getUserId());
				sysRoleService.updateById(sysRole);
			}else{
				return FansResp.error("修改系统_角色异常！ID不能为空");
			}
		} catch (Exception e) {
			logger.error("修改系统_角色异常", e);
			return FansResp.error("修改系统_角色异常！");
		}
		return FansResp.successData(sysRole);
	}
	
	/**
	 * 
	 * @Title：delete  
	 * @Description: 删除系统_角色
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午5:43:24 
	 * @param @param ids
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@Log(operation = "删除系统_角色")
	@RequiredPermission("sysRole:delete")
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public FansResp delete(@PathVariable(value = "ids") String ids) {
		try {
			if (BlankUtils.isNotBlank(ids)) {
				String[] idsArr = ids.split(",");
				sysRoleService.deleteBatchByIdsLogic(Arrays.asList(idsArr));
			}else{
				return FansResp.error("删除系统_角色异常！IDS不能为空");
			}
		} catch (Exception e) {
			logger.error("删除系统_角色异常", e);
			return FansResp.error("删除系统_角色异常！");
		}
		return FansResp.success();
	}
	
	
}
