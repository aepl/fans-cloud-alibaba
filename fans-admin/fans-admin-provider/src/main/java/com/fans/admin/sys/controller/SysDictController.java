package com.fans.admin.sys.controller;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.annotations.Log;
import com.fans.admin.annotations.RequiredPermission;
import com.fans.admin.sys.cache.SysDictCache;
import com.fans.admin.sys.entity.SysDict;
import com.fans.admin.sys.service.ISysDictService;
import com.fans.admin.sys.warpper.SysDictWarpper;
import com.fans.common.base.BaseController;
import com.fans.common.utils.BlankUtils;
import com.fans.common.utils.FansResp;
import com.fans.common.utils.jwt.TokenUtil;
import com.fans.common.vo.PageFactory;
import com.fans.common.vo.PageVo;

/**
 * 
 * @ClassName: SysDictController
 * @Description: 基础字典类型数据接口
 * @author fanhaohao
 * @date 2020年1月16日 下午5:31:58
 */
@RestController
@RequestMapping("/sys/sysDict")
public class SysDictController extends BaseController {

    @Autowired
    private ISysDictService sysDictService;
    
    /**
     * 
     * @Title：getById  
     * @Description: 根据id获取基础字典类型
     * @author: fanhaohao
     * @date 2020年1月16日 下午5:32:11 
     * @param @param id
     * @param @return 
     * @return FansResp 
     * @throws
     */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public FansResp getById(String id) {
		SysDict sysDict = null;
		try {
			if (BlankUtils.isNotBlank(id)) {
				sysDict = sysDictService.selectById(id);
			}else{
				return FansResp.error("根据id获取基础字典类型异常！ID不能为空");
			}
		} catch (Exception e) {
			logger.error("根据id获取基础字典类型异常", e);
			return FansResp.error("根据id获取基础字典类型异常！");
		}
		return FansResp.successData(sysDict);
	}

	/**
	 * 
	 * @Title：list  
	 * @Description:  获取基础字典类型列表
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午5:32:19 
	 * @param @param sysDict
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@RequiredPermission("sysDict:list")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public FansResp list(SysDict sysDict) {
		PageVo pv = new PageVo();
		pv.setSortField("showOrder");
		pv.setSortOrder("asc");
		Page<SysDict> page = new PageFactory<SysDict>().page(pv);
		List<Map<String, Object>> result = sysDictService.list(page, entityToMap(sysDict));
		page.setRecords((List<SysDict>) new SysDictWarpper(result).warp());
		return FansResp.successData(page);
	}

	/**
	 * 
	 * @Title：create  
	 * @Description: 新建基础字典类型
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:21:33 
	 * @param @param sysDict
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@Log(operation = "新建基础字典类型")
	@RequiredPermission("sysDict:create")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public FansResp create(SysDict sysDict) {
		try {
			if(BlankUtils.isBlank(sysDict.getShowOrder())){
				BigDecimal shoswOrder = new BigDecimal("0");
				sysDict.setShowOrder(shoswOrder);
			}
			sysDict.preInsert();
			sysDict.setCreateBy(TokenUtil.getUserId());
			sysDictService.insert(sysDict);
		} catch (Exception e) {
			logger.error("新建基础字典类型异常", e);
			return FansResp.error("新建基础字典类型异常！");
		}
		return FansResp.successData(sysDict);
	}
	
	/**
	 * 
	 * @Title：update  
	 * @Description: 修改基础字典类型
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:21:43 
	 * @param @param sysDict
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@Log(operation = "修改基础字典类型")
	@RequiredPermission("sysDict:update")
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public FansResp update(SysDict sysDict) {
		try {
			if (BlankUtils.isNotBlank(sysDict.getId())) {
				sysDict.preUpdate();
				sysDict.setUpdateBy(TokenUtil.getUserId());
				sysDictService.updateById(sysDict);
			}else{
				return FansResp.error("修改基础字典类型异常！ID不能为空");
			}
		} catch (Exception e) {
			logger.error("修改基础字典类型异常", e);
			return FansResp.error("修改基础字典类型异常！");
		}
		return FansResp.successData(sysDict);
	}
	
	/**
	 * 
	 * @Title：delete  
	 * @Description: 删除基础字典类型
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:21:56 
	 * @param @param ids
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@Log(operation = "删除基础字典类型")
	@RequiredPermission("sysDict:delete")
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public FansResp delete(@PathVariable(value = "ids") String ids) {
		try {
			if (BlankUtils.isNotBlank(ids)) {
				String[] idsArr = ids.split(",");
				sysDictService.deleteBatchByIdsLogic(Arrays.asList(idsArr));
			}else{
				return FansResp.error("删除基础字典类型异常！IDS不能为空");
			}
		} catch (Exception e) {
			logger.error("删除基础字典类型异常", e);
			return FansResp.error("删除基础字典类型异常！");
		}
		return FansResp.success();
	}
	
	/**
	 * 
	 * @Title：getDictListByCode  
	 * @Description: 根据code来获取字典信息
	 * @author: fanhaohao
	 * @date 2020年1月14日 下午5:09:28 
	 * @param @param code
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/getDictListByCode", method = RequestMethod.GET)
	public FansResp getDictListByCode(String code) {
		List<Map<String, Object>> rt = null;
		try {
			if (BlankUtils.isNotBlank(code)) {
				rt = SysDictCache.getDictList(code);
			}else{
				return FansResp.error("根据code来获取字典信息异常！code不能为空");
			}
		} catch (Exception e) {
			logger.error("根据code来获取字典信息异常", e);
			return FansResp.error("根据code来获取字典信息异常！");
		}
		return FansResp.successData(rt);
	}
	
	/**
	 * 
	 * @Title：getDictMapByCode  
	 * @Description: 根据code来获取字典信息
	 * @author: fanhaohao
	 * @date 2020年1月14日 下午5:09:17 
	 * @param @param code
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/getDictMapByCode", method = RequestMethod.GET)
	public FansResp getDictMapByCode(String code) {
		Map<String, Object> rt = null;
		try {
			if (BlankUtils.isNotBlank(code)) {
				rt = SysDictCache.getDictMap(code);
			}else{
				return FansResp.error("根据code来获取字典信息异常！code不能为空");
			}
		} catch (Exception e) {
			logger.error("根据code来获取字典信息异常", e);
			return FansResp.error("根据code来获取字典信息异常！");
		}
		return FansResp.successData(rt);
	}
	
	/**
	 * 
	 * @Title：getDictMapListByCodes
	 * @Description:根据codes来获取字典信息
	 * @author: fanhaohao
	 * @date 2020年1月15日 上午10:31:49 
	 * @param @param codes
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/getDictMapListByCodes", method = RequestMethod.GET)
	public FansResp getDictMapListByCodes(String codes) {
		Map<String, List<Map<String, Object>>> rt = null;
		try {
			if (BlankUtils.isNotBlank(codes)) {
				rt = SysDictCache.getDictMapList(codes);
			}else{
				return FansResp.error("根据codes来获取字典信息异常！codes不能为空");
			}
		} catch (Exception e) {
			logger.error("根据codes来获取字典信息异常", e);
			return FansResp.error("根据codes来获取字典信息异常！");
		}
		return FansResp.successData(rt);
	}
	
}
