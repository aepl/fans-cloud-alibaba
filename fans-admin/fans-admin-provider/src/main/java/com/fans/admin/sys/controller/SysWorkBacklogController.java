package com.fans.admin.sys.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.annotations.Log;
import com.fans.admin.annotations.RequiredPermission;
import com.fans.admin.sys.entity.SysWorkBacklog;
import com.fans.admin.sys.service.ISysWorkBacklogService;
import com.fans.admin.sys.warpper.SysWorkBacklogWarpper;
import com.fans.common.base.BaseController;
import com.fans.common.utils.BlankUtils;
import com.fans.common.utils.FansResp;
import com.fans.common.utils.jwt.TokenUtil;
import com.fans.common.vo.PageFactory;

/**
 * 
 * @ClassName: SysWorkBacklogController  
 * @Description: 待办事项数据接口 
 * @author fanhaohao
 * @date 2020年1月17日 上午9:15:36
 */
@RestController
@RequestMapping("/sys/sysWorkBacklog")
public class SysWorkBacklogController extends BaseController {

    @Autowired
    private ISysWorkBacklogService sysWorkBacklogService;
    /**
     * 
     * @Title：getById  
     * @Description: 根据id获取待办事项
     * @author: fanhaohao
     * @date 2020年1月17日 上午9:15:46 
     * @param @param id
     * @param @return 
     * @return FansResp 
     * @throws
     */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public FansResp getById(String id) {
		SysWorkBacklog sysWorkBacklog = null;
		try {
			if (BlankUtils.isNotBlank(id)) {
				sysWorkBacklog = sysWorkBacklogService.selectById(id);
			}else{
				return FansResp.error("根据id获取待办事项异常！ID不能为空");
			}
		} catch (Exception e) {
			logger.error("根据id获取待办事项异常", e);
			return FansResp.error("根据id获取待办事项异常！");
		}
		return FansResp.successData(sysWorkBacklog);
	}

	/**
	 * 
	 * @Title：list  
	 * @Description: 获取待办事项列表
	 * @author: fanhaohao
	 * @date 2020年1月17日 上午9:16:15 
	 * @param @param sysWorkBacklog
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@RequiredPermission("sysWorkBacklog:list")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public Object list(SysWorkBacklog sysWorkBacklog) {
		Page<SysWorkBacklog> page = new PageFactory<SysWorkBacklog>().page();
		List<Map<String, Object>> result = sysWorkBacklogService.list(page, entityToMap(sysWorkBacklog));
		page.setRecords((List<SysWorkBacklog>) new SysWorkBacklogWarpper(result).warp());
		return FansResp.successData(page);
	}

	/**
	 * 
	 * @Title：create  
	 * @Description: 新建待办事项
	 * @author: fanhaohao
	 * @date 2020年1月17日 上午9:16:26 
	 * @param @param sysWorkBacklog
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@Log(operation = "新建待办事项")
	@RequiredPermission("sysWorkBacklog:create")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public FansResp create(SysWorkBacklog sysWorkBacklog) {
		try {
			sysWorkBacklog.preInsert();
			sysWorkBacklog.setCreateBy(TokenUtil.getUserId());
			sysWorkBacklogService.insert(sysWorkBacklog);
		} catch (Exception e) {
			logger.error("新建待办事项异常", e);
			return FansResp.error("新建待办事项异常！");
		}
		return FansResp.successData(sysWorkBacklog);
	}
	
	/**
	 * 
	 * @Title：update  
	 * @Description: 修改待办事项
	 * @author: fanhaohao
	 * @date 2020年1月17日 上午9:16:36 
	 * @param @param sysWorkBacklog
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@Log(operation = "修改待办事项")
	@RequiredPermission("sysWorkBacklog:update")
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public FansResp update(SysWorkBacklog sysWorkBacklog) {
		try {
			if (BlankUtils.isNotBlank(sysWorkBacklog.getId())) {
				sysWorkBacklog.preUpdate();
				sysWorkBacklog.setUpdateBy(TokenUtil.getUserId());
				sysWorkBacklogService.updateById(sysWorkBacklog);
			}else{
				return FansResp.error("修改待办事项异常！ID不能为空");
			}
		} catch (Exception e) {
			logger.error("修改待办事项异常", e);
			return FansResp.error("修改待办事项异常！");
		}
		return FansResp.successData(sysWorkBacklog);
	}
	
	/**
	 * 
	 * @Title：delete  
	 * @Description: 删除待办事项
	 * @author: fanhaohao
	 * @date 2020年1月17日 上午9:16:45 
	 * @param @param ids
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@Log(operation = "删除待办事项")
	@RequiredPermission("sysWorkBacklog:delete")
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public FansResp delete(@PathVariable(value = "ids") String ids) {
		try {
			if (BlankUtils.isNotBlank(ids)) {
				String[] idsArr = ids.split(",");
				sysWorkBacklogService.deleteBatchByIdsLogic(Arrays.asList(idsArr));
			}else{
				return FansResp.error("删除待办事项异常！IDS不能为空");
			}
		} catch (Exception e) {
			logger.error("删除待办事项异常", e);
			return FansResp.error("删除待办事项异常！");
		}
		return FansResp.success();
	}
	
	
}
