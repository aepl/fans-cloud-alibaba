package com.fans.admin.sys.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.annotations.Log;
import com.fans.admin.annotations.RequiredPermission;
import com.fans.admin.sys.entity.SysLog;
import com.fans.admin.sys.service.ISysLogService;
import com.fans.admin.sys.warpper.SysLogWarpper;
import com.fans.common.base.BaseController;
import com.fans.common.utils.BlankUtils;
import com.fans.common.utils.FansResp;
import com.fans.common.utils.jwt.TokenUtil;
import com.fans.common.vo.PageFactory;

/**
 * 
 * @ClassName: SysLogController  
 * @Description: 系统日志接口
 * @author fanhaohao
 * @date 2020年1月16日 下午5:34:12
 */
@RestController
@RequestMapping("/sys/sysLog")
public class SysLogController extends BaseController {

    @Autowired
    private ISysLogService sysLogService;
    
    /**
     * 
     * @Title：getById  
     * @Description: 根据id获取系统日志
     * @author: fanhaohao
     * @date 2020年1月16日 下午5:34:41 
     * @param @param id
     * @param @return 
     * @return FansResp 
     * @throws
     */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public FansResp getById(String id) {
		SysLog sysLog = null;
		try {
			if (BlankUtils.isNotBlank(id)) {
				sysLog = sysLogService.selectById(id);
			}else{
				return FansResp.error("根据id获取异常！ID不能为空");
			}
		} catch (Exception e) {
			logger.error("根据id获取异常", e);
			return FansResp.error("根据id获取异常！");
		}
		return FansResp.successData(sysLog);
	}

	/**
	 * 
	 * @Title：list  
	 * @Description: 获取列表
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午5:34:59 
	 * @param @param sysLog
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@RequiredPermission("sysLog:list")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public Object list(SysLog sysLog) {
		Page<SysLog> page = new PageFactory<SysLog>().page();
		List<Map<String, Object>> result = sysLogService.list(page, entityToMap(sysLog));
		page.setRecords((List<SysLog>) new SysLogWarpper(result).warp());
		return FansResp.successData(page);
	}

	/**
	 * 
	 * @Title：create  
	 * @Description: 新建
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午5:35:11 
	 * @param @param sysLog
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@Log(operation = "新建")
	@RequiredPermission("sysLog:create")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public FansResp create(SysLog sysLog) {
		try {
			sysLog.preInsert();
			sysLog.setCreateBy(TokenUtil.getUserId());
			sysLogService.insert(sysLog);
		} catch (Exception e) {
			logger.error("新建异常", e);
			return FansResp.error("新建异常！");
		}
		return FansResp.successData(sysLog);
	}
	
	/**
	 * 
	 * @Title：update  
	 * @Description:  修改
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午5:35:19 
	 * @param @param sysLog
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@Log(operation = "修改")
	@RequiredPermission("sysLog:update")
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public FansResp update(SysLog sysLog) {
		try {
			if (BlankUtils.isNotBlank(sysLog.getId())) {
				sysLog.preUpdate();
				sysLog.setUpdateBy(TokenUtil.getUserId());
				sysLogService.updateById(sysLog);
			}else{
				return FansResp.error("修改异常！ID不能为空");
			}
		} catch (Exception e) {
			logger.error("修改异常", e);
			return FansResp.error("修改异常！");
		}
		return FansResp.successData(sysLog);
	}
	
	/**
	 * 
	 * @Title：delete  
	 * @Description: 删除系统日志
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午5:35:29 
	 * @param @param ids
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequiredPermission("sysLog:delete")
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public FansResp delete(@PathVariable(value = "ids") String ids) {
		try {
			if (BlankUtils.isNotBlank(ids)) {
				String[] idsArr = ids.split(",");
				sysLogService.deleteBatchByIdsLogic(Arrays.asList(idsArr));
			}else{
				return FansResp.error("删除异常！IDS不能为空");
			}
		} catch (Exception e) {
			logger.error("删除异常", e);
			return FansResp.error("删除异常！");
		}
		return FansResp.success();
	}
	
	/**
	 * @Title：deleteAll  
	 * @Description: 清除日志
	 * @author: fanhaohao
	 * @date 2018年12月28 09:27:28
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@RequiredPermission("sysLog:deleteAll")
	@RequestMapping(value = "/deleteAll", method = RequestMethod.DELETE)
	public FansResp deleteAll() {
		try {
			List<String> allIds = sysLogService.getAllIds();// 获取所有的日志id
			// sysLogService.deleteBatchByIdsLogic(allIds);
			sysLogService.deleteBatchIds(allIds);
		} catch (Exception e) {
			logger.error("清除日志异常", e);
			return FansResp.error("清除日志异常！");
		}
		return FansResp.success();
	}
	
	
}
