package com.fans.admin.sys.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fans.admin.captcha.CaptchaService;

/**
 * 
 * @ClassName: CaptchaController  
 * @Description: 验证码相关接口 
 * @author fanhaohao
 * @date 2020年1月12日 上午11:10:27
 */
@RestController
@RequestMapping("/sys/captcha")
public class CaptchaController {
	@Autowired
    private CaptchaService captchaService;
	/**
	 * @throws Exception 
	 * 
	 * @Title：captcha  
	 * @Description: 获取验证码
	 * @author: fanhaohao
	 * @date 2020年1月12日 上午11:15:01 
	 * @param @param request
	 * @param @param response 
	 * @return void 
	 * @throws
	 */
    @RequestMapping(value = "/captcha")
	public byte[] captcha(String captchaId, HttpServletRequest request, HttpServletResponse response) throws Exception {
		System.out.println(captchaId);
		return captchaService.buildCaptcha(captchaId);
    }
}
