package com.fans.admin.sys.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.annotations.Log;
import com.fans.admin.annotations.RequiredPermission;
import com.fans.admin.sys.entity.SysUserRole;
import com.fans.admin.sys.service.ISysUserRoleService;
import com.fans.admin.sys.warpper.SysUserRoleWarpper;
import com.fans.common.base.BaseController;
import com.fans.common.utils.BlankUtils;
import com.fans.common.utils.FansResp;
import com.fans.common.utils.jwt.TokenUtil;
import com.fans.common.vo.PageFactory;

/**
 * 
 * @ClassName: SysUserRoleController  
 * @Description: 系统_用户角色数据接口 
 * @author fanhaohao
 * @date 2020年1月17日 上午9:14:21
 */
@RestController
@RequestMapping("/sys/sysUserRole")
public class SysUserRoleController extends BaseController {

    @Autowired
    private ISysUserRoleService sysUserRoleService;
    
	/**
     * 
     * @Title：getById  
     * @Description: 根据id获取系统_用户角色
     * @author: fanhaohao
     * @date 2020年1月17日 上午9:14:31 
     * @param @param id
     * @param @return 
     * @return FansResp 
     * @throws
     */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public FansResp getById(String id) {
		SysUserRole sysUserRole = null;
		try {
			if (BlankUtils.isNotBlank(id)) {
				sysUserRole = sysUserRoleService.selectById(id);
			}else{
				return FansResp.error("根据id获取系统_用户角色异常！ID不能为空");
			}
		} catch (Exception e) {
			logger.error("根据id获取系统_用户角色异常", e);
			return FansResp.error("根据id获取系统_用户角色异常！");
		}
		return FansResp.successData(sysUserRole);
	}
	
	/**
	 * 
	 * @Title：list  
	 * @Description: 获取系统_用户角色列表
	 * @author: fanhaohao
	 * @date 2020年1月17日 上午9:14:42 
	 * @param @param sysUserRole
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@RequiredPermission("sysUserRole:list")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public Object list(SysUserRole sysUserRole) {
		Page<SysUserRole> page = new PageFactory<SysUserRole>().page();
		List<Map<String, Object>> result = sysUserRoleService.list(page, entityToMap(sysUserRole));
		page.setRecords((List<SysUserRole>) new SysUserRoleWarpper(result).warp());
		return FansResp.successData(page);
	}

	/**
	 * 
	 * @Title：create  
	 * @Description: 新建系统_用户角色
	 * @author: fanhaohao
	 * @date 2020年1月17日 上午9:14:56 
	 * @param @param sysUserRole
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@Log(operation = "新建系统_用户角色")
	@RequiredPermission("sysUserRole:create")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public FansResp create(SysUserRole sysUserRole) {
		try {
			sysUserRole.preInsert();
			sysUserRole.setCreateBy(TokenUtil.getUserId());
			sysUserRoleService.insert(sysUserRole);
		} catch (Exception e) {
			logger.error("新建系统_用户角色异常", e);
			return FansResp.error("新建系统_用户角色异常！");
		}
		return FansResp.successData(sysUserRole);
	}
	
	/**
	 * 
	 * @Title：update  
	 * @Description: 修改系统_用户角色
	 * @author: fanhaohao
	 * @date 2020年1月17日 上午9:15:05 
	 * @param @param sysUserRole
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@Log(operation = "修改系统_用户角色")
	@RequiredPermission("sysUserRole:update")
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public FansResp update(SysUserRole sysUserRole) {
		try {
			if (BlankUtils.isNotBlank(sysUserRole.getId())) {
				sysUserRole.preUpdate();
				sysUserRole.setUpdateBy(TokenUtil.getUserId());
				sysUserRoleService.updateById(sysUserRole);
			}else{
				return FansResp.error("修改系统_用户角色异常！ID不能为空");
			}
		} catch (Exception e) {
			logger.error("修改系统_用户角色异常", e);
			return FansResp.error("修改系统_用户角色异常！");
		}
		return FansResp.successData(sysUserRole);
	}
	
	/**
	 * 
	 * @Title：delete  
	 * @Description: 删除系统_用户角色
	 * @author: fanhaohao
	 * @date 2020年1月17日 上午9:15:15 
	 * @param @param ids
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@Log(operation = "删除系统_用户角色")
	@RequiredPermission("sysUserRole:delete")
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public FansResp delete(@PathVariable(value = "ids") String ids) {
		try {
			if (BlankUtils.isNotBlank(ids)) {
				String[] idsArr = ids.split(",");
				sysUserRoleService.deleteBatchByIdsLogic(Arrays.asList(idsArr));
			}else{
				return FansResp.error("删除系统_用户角色异常！IDS不能为空");
			}
		} catch (Exception e) {
			logger.error("删除系统_用户角色异常", e);
			return FansResp.error("删除系统_用户角色异常！");
		}
		return FansResp.success();
	}
	
	
}
