package com.fans.admin.sys.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.annotations.Log;
import com.fans.admin.annotations.RequiredPermission;
import com.fans.admin.sys.entity.SysLoginLog;
import com.fans.admin.sys.service.ISysLoginLogService;
import com.fans.admin.sys.warpper.SysLoginLogWarpper;
import com.fans.common.base.BaseController;
import com.fans.common.utils.BlankUtils;
import com.fans.common.utils.FansResp;
import com.fans.common.utils.jwt.TokenUtil;
import com.fans.common.vo.PageFactory;

/**
 * 
 * @ClassName: SysLoginLogController
 * @Description: 用户登录日志数据接口
 * @author fanhaohao
 * @date 2020年1月16日 下午5:40:16
 */
@RestController
@RequestMapping("/sys/sysLoginLog")
public class SysLoginLogController extends BaseController {

    @Autowired
    private ISysLoginLogService sysLoginLogService;
    
	/**
     * @Title：getById
     * @Description: 根据id获取用户登录日志
     * @author: fanhaohao
     * @date 2020年01月10 16:53:50
     * @param @param id
     * @param @param request
     * @param @param response
     * @param @return 
     * @return String 
     * @throws
     */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public FansResp getById(String id) {
		SysLoginLog sysLoginLog = null;
		try {
			if (BlankUtils.isNotBlank(id)) {
				sysLoginLog = sysLoginLogService.selectById(id);
			}else{
				return FansResp.error("根据id获取用户登录日志异常！ID不能为空");
			}
		} catch (Exception e) {
			logger.error("根据id获取用户登录日志异常", e);
			return FansResp.error("根据id获取用户登录日志异常！");
		}
		return FansResp.successData(sysLoginLog);
	}

	/**
	 * @Title：list  
	 * @Description: 获取用户登录日志列表
	 * @author: fanhaohao
	 * @date 2020年01月10 16:53:50
	 * @param @param condition
	 * @param @param response
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@RequiredPermission("sysLoginLog:list")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public Object list(SysLoginLog sysLoginLog) {
		Page<SysLoginLog> page = new PageFactory<SysLoginLog>().page();
		List<Map<String, Object>> result = sysLoginLogService.list(page, entityToMap(sysLoginLog));
		page.setRecords((List<SysLoginLog>) new SysLoginLogWarpper(result).warp());
		return FansResp.successData(page);
	}

	/**
	 * @Title：create  
	 * @Description: 新建用户登录日志
	 * @author: fanhaohao
	 * @date 2020年01月10 16:53:50
	 * @param @param sysMenu
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "新建用户登录日志")
	@RequiredPermission("sysLoginLog:create")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public FansResp create(SysLoginLog sysLoginLog) {
		try {
			sysLoginLog.preInsert();
			sysLoginLog.setCreateBy(TokenUtil.getUserId());
			sysLoginLogService.insert(sysLoginLog);
		} catch (Exception e) {
			logger.error("新建用户登录日志异常", e);
			return FansResp.error("新建用户登录日志异常！");
		}
		return FansResp.successData(sysLoginLog);
	}
	
	/**
	 * @Title：update  
	 * @Description: 修改用户登录日志
	 * @author: fanhaohao
	 * @date 2020年01月10 16:53:50
	 * @param @param sysMenu
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "修改用户登录日志")
	@RequiredPermission("sysLoginLog:update")
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public FansResp update(SysLoginLog sysLoginLog) {
		try {
			if (BlankUtils.isNotBlank(sysLoginLog.getId())) {
				sysLoginLog.preUpdate();
				sysLoginLog.setUpdateBy(TokenUtil.getUserId());
				sysLoginLogService.updateById(sysLoginLog);
			}else{
				return FansResp.error("修改用户登录日志异常！ID不能为空");
			}
		} catch (Exception e) {
			logger.error("修改用户登录日志异常", e);
			return FansResp.error("修改用户登录日志异常！");
		}
		return FansResp.successData(sysLoginLog);
	}
	
	/**
	 * @Title：delete  
	 * @Description: 删除用户登录日志
	 * @author: fanhaohao
	 * @date 2020年01月10 16:53:50
	 * @param @param ids
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "删除用户登录日志")
	@RequiredPermission("sysLoginLog:delete")
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public FansResp delete(@PathVariable(value = "ids") String ids) {
		try {
			if (BlankUtils.isNotBlank(ids)) {
				String[] idsArr = ids.split(",");
				sysLoginLogService.deleteBatchByIdsLogic(Arrays.asList(idsArr));
			}else{
				return FansResp.error("删除用户登录日志异常！IDS不能为空");
			}
		} catch (Exception e) {
			logger.error("删除用户登录日志异常", e);
			return FansResp.error("删除用户登录日志异常！");
		}
		return FansResp.success();
	}
	
	/**
	 * @Title：deleteAll  
	 * @Description: 清除日志
	 * @author: fanhaohao
	 * @date 2018年12月28 09:27:28
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@RequiredPermission("sysLoginLog:deleteAll")
	@RequestMapping(value = "/deleteAll", method = RequestMethod.DELETE)
	public FansResp deleteAll() {
		try {
			List<String> allIds = sysLoginLogService.getAllIds();// 获取所有的日志id
			// sysLogService.deleteBatchByIdsLogic(allIds);
			sysLoginLogService.deleteBatchIds(allIds);
		} catch (Exception e) {
			logger.error("清除日志异常", e);
			return FansResp.error("清除日志异常！");
		}
		return FansResp.success();
	}
	
	/**
	 * 
	 * @Title：saveLoginLog  
	 * @Description: 登录日志保存
	 * @author: fanhaohao
	 * @date 2020年1月17日 上午10:42:14 
	 * @param @param request
	 * @param @param logonId
	 * @param @param operType
	 * @param @param resultType
	 * @param @param msg 
	 * @return void 
	 * @throws
	 */
	@RequestMapping(value = "/saveLoginLog", method = RequestMethod.POST)
	public void saveLoginLog(HttpServletRequest request, String logonId, String operType,String resultType, String msg) {
		sysLoginLogService.saveLoginLog(request, logonId, operType, resultType, msg);
	}
}
