package com.fans.admin.test.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.annotations.Log;
import com.fans.admin.annotations.RequiredPermission;
import com.fans.admin.test.entity.TestDict;
import com.fans.admin.test.service.ITestDictService;
import com.fans.admin.test.warpper.TestDictWarpper;
import com.fans.common.base.BaseController;
import com.fans.common.utils.BlankUtils;
import com.fans.common.utils.FansResp;
import com.fans.common.utils.jwt.TokenUtil;
import com.fans.common.vo.PageFactory;
import com.fans.common.vo.PageVo;


/**
 * @description: 测试控制层
 * @date: 2020-04-15
 * @author: fanhaohao
 * @version: 1.0
 * @copyright: 版权所有 fans (c)2020 
 */
@RestController
@RequestMapping("/test/testDict")
public class TestDictController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(TestDictController.class);

    @Autowired
    private ITestDictService testDictService;
    
	/**
     * @Title：getById
     * @Description: 根据id获取测试
     * @author: fanhaohao
     * @date 2020-04-15
     * @param id
     * @return FansResp 
     */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	@RequiredPermission("testDict:getById")
	public FansResp getById(String id) {
		TestDict testDict = null;
		try {
			if (BlankUtils.isNotBlank(id)) {
				testDict = testDictService.selectById(id);
			}else{
				return FansResp.error("根据id获取测试异常！ID不能为空");
			}
		} catch (Exception e) {
			logger.error("根据id获取测试异常", e);
			return FansResp.error("根据id获取测试异常！");
		}
		return FansResp.successData(testDict);
	}

	/**
	 * @Title：list  
	 * @Description: 获取测试列表
	 * @author: fanhaohao
	 * @date 2020-04-15
	 * @param TestDict
	 * @return FansResp 
	 */
	@SuppressWarnings("unchecked")
	@Log(operation = "获取测试列表")
	@RequiredPermission("testDict:list")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public FansResp list(TestDict testDict) {
		PageVo pv = new PageVo();
		Page<TestDict> page = new PageFactory<TestDict>().page(pv);
		List<Map<String, Object>> result = testDictService.list(page, entityToMap(testDict));
		page.setRecords((List<TestDict>) new TestDictWarpper(result).warp());
		return FansResp.successData(page);
	}

	/**
	 * @Title：create  
	 * @Description: 新建测试
	 * @author: fanhaohao
	 * @date 2020-04-15
	 * @param testDict
	 * @return FansResp 
	 */
	@Log(operation = "新建测试")
	@RequiredPermission("testDict:create")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public FansResp create(TestDict testDict) {
		try {
			testDict.preInsert();
			testDict.setCreateBy(TokenUtil.getUserId());
			testDictService.insert(testDict);
		} catch (Exception e) {
			logger.error("新建测试异常", e);
			return FansResp.error("新建测试异常！");
		}
		return FansResp.successData(testDict);
	}
	
	/**
	 * @Title：update  
	 * @Description: 修改测试
	 * @author: fanhaohao
	 * @date 2020-04-15
	 * @param testDict
	 * @return FansResp 
	 */
	@Log(operation = "修改测试")
	@RequiredPermission("testDict:update")
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public FansResp update(TestDict testDict) {
		try {
			if (BlankUtils.isNotBlank(testDict.getId())) {
				testDict.preUpdate();
				testDict.setUpdateBy(TokenUtil.getUserId());
				testDictService.updateById(testDict);
			}else{
				return FansResp.error("修改测试异常！ID不能为空");
			}
		} catch (Exception e) {
			logger.error("修改测试异常", e);
			return FansResp.error("修改测试异常！");
		}
		return FansResp.successData(testDict);
	}
	
	/**
	 * @Title：delete  
	 * @Description: 删除测试
	 * @author: fanhaohao
	 * @date 2020-04-15
	 * @param ids
	 * @return FansResp 
	 */
	@Log(operation = "删除测试")
	@RequiredPermission("testDict:delete")
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public FansResp delete(@PathVariable(value = "ids") String ids) {
		try {
			if (BlankUtils.isNotBlank(ids)) {
				String[] idsArr = ids.split(",");
				testDictService.deleteBatchByIdsLogic(Arrays.asList(idsArr));
			}else{
				return FansResp.error("删除测试异常！IDS不能为空");
			}
		} catch (Exception e) {
			logger.error("删除测试异常", e);
			return FansResp.error("删除测试异常！");
		}
		return FansResp.success();
	}
	
	
}
