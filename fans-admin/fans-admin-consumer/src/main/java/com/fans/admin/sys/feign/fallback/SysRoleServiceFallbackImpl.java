package com.fans.admin.sys.feign.fallback;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fans.admin.sys.entity.SysRole;
import com.fans.admin.sys.feign.SysRoleService;
import com.fans.common.utils.FansResp;
import com.fans.common.utils.JSON;

/**
 * 
 * @ClassName: SysRoleServiceFallbackImpl
 * @Description: 系统_角色数据服务的fallback
 * @author fanhaohao
 * @date 2020年1月13日 下午4:14:38
 */
@Service
public class SysRoleServiceFallbackImpl implements SysRoleService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public FansResp getById(String id) {
		logger.error("根据id获取系统_角色数据{}异常参数:{}", "getById", id);
		return FansResp.error();
	}

	@Override
	public FansResp list(SysRole sysRole) {
		logger.error("分页获取系统_角色list数据{}异常参数:{}", "list", JSON.toJSONString(sysRole));
		return FansResp.error();
	}

	@Override
	public FansResp listAll() {
		logger.error("查询所有的角色列表信息 {}异常", "listAll");
		return FansResp.error();
	}

	@Override
	public FansResp create(Map<String, Object> sysRole) {
		logger.error("添加角色信息{}异常参数:{}", "create", JSON.toJSONString(sysRole));
		return FansResp.error();
	}

	@Override
	public FansResp update(Map<String, Object> sysRole) {
		logger.error("修改角色信息{}异常参数:{}", "update", JSON.toJSONString(sysRole));
		return FansResp.error();
	}

	@Override
	public FansResp delete(String ids) {
		logger.error("角色信息删除{}异常参数:{}", "update", ids);
		return FansResp.error();
	}

}
