package com.fans.admin.test.feign.fallback;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fans.admin.test.entity.TestDict;
import com.fans.admin.test.feign.TestDictService;
import com.fans.common.utils.FansResp;
import com.fans.common.utils.JSON;

/**
 * @ClassName:TestDictServiceFallbackImpl
 * @description: 测试数据服务的fallback
 * @date: 2020-04-15
 * @author: fanhaohao
 * @version: 1.0
 * @copyright: 版权所有 fans (c)2020 
 */ 
@Service
public class TestDictServiceFallbackImpl implements TestDictService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * @Title：getById   
	 * @Description: 根据id获取测试数据
	 * @author: fanhaohao
	 * @date 2020-04-15
	 * @param page
	 * @return FansResp
	 */ 
	@Override
	public FansResp getById(String id) {
		logger.error("根据id获取测试数据{}异常参数:{}", "getById", id);
		return FansResp.error();
	}

	/**
	 * 
	 * @Title：list  
	 * @Description: 分页获取测试数据
	 * @author: fanhaohao
	 * @date 2020-04-15 
	 * @param testDict
	 * @return FansResp 
	 */
	@Override
	public FansResp list(TestDict testDict) {
		logger.error("分页获取测试list数据{}异常参数:{}", "list", JSON.toJSONString(testDict));
		return FansResp.error();
	}

	/**
	 * 
	 * @Title：create  
	 * @Description: 添加测试信息
	 * @author: fanhaohao
	 * @date 2020-04-15 
	 * @param testDict
	 * @return FansResp 
	 */
	@Override
	public FansResp create(Map<String, Object> testDict) {
		logger.error("添加测试信息{}异常参数:{}", "create", JSON.toJSONString(testDict));
		return FansResp.error();
	}

	/**
	 * 
	 * @Title：update  
	 * @Description: 修改测试信息
	 * @author: fanhaohao
	 * @date 2020-04-15 
	 * @param testDict
	 * @return FansResp 
	 */
	@Override
	public FansResp update(Map<String, Object> testDict) {
		logger.error("修改测试信息{}异常参数:{}", "update", JSON.toJSONString(testDict));
		return FansResp.error();
	}
	
	/**
	 * 
	 * @Title：delete  
	 * @Description: 测试信息删除
	 * @author: fanhaohao
	 * @date 2020-04-15 
	 * @param ids
	 * @return FansResp 
	 */
	@Override
	public FansResp delete(String ids) {
		logger.error("测试信息删除{}异常参数:{}", "update", ids);
		return FansResp.error();
	}

}
