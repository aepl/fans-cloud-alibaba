package com.fans.admin.sys.feign;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fans.admin.sys.entity.SysDict;
import com.fans.admin.sys.feign.fallback.SysDictServiceFallbackImpl;
import com.fans.common.utils.FansResp;

/**
 * @ClassName: SysDictService
 * @Description:基础字典类型数据
 * @author fanhaohao
 * @date 2020年1月30日 上午11:18:00
 */
@FeignClient(name = "fans-admin-provider", fallback = SysDictServiceFallbackImpl.class)
public interface SysDictService {

	/**
	 * 
	 * @Title：getById  
	 * @Description: 根据id获取基础字典类型
	 * @author: fanhaohao
	 * @date 2020年1月13日 下午4:48:59 
	 * @param @param id
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = { "/sys/sysDict/getById" }, method = RequestMethod.GET)
	public FansResp getById(@RequestParam(value = "id") String id);
	
	/**
	 * 
	 * @Title：list  
	 * @Description: 获取基础字典类型列表
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:20:02 
	 * @param @param sysDict
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@RequestMapping(value = "/sys/sysDict/list", method = RequestMethod.GET)
	public FansResp list(@SpringQueryMap SysDict sysDict);
	
	/**
	 * 
	 * @Title：create  
	 * @Description: 添加基础字典类型
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:21:26 
	 * @param @param sysDict
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/sys/sysDict/create", method = RequestMethod.POST)
	public FansResp create(@RequestParam("sysDict") Map<String, Object> sysDict);
	
	/**
	 * 
	 * @Title：update  
	 * @Description: 修改基础字典类型
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:22:46 
	 * @param @param sysDict
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/sys/sysDict/update", method = RequestMethod.PUT)
	public FansResp update(@RequestParam("sysDict") Map<String, Object> sysDict);
	
	/**
	 * 
	 * @Title：delete  
	 * @Description: 删除基础字典类型
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:22:39 
	 * @param @param ids
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/sys/sysDict/delete/{ids}", method = RequestMethod.DELETE)
	public FansResp delete(@PathVariable(value = "ids") String ids);
	
	/**
	 * 
	 * @Title：getDictListByCode
	 * @Description: 根据code来获取字典信息
	 * @author: fanhaohao
	 * @date 2020年1月13日 下午4:48:59 
	 * @param @param id
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = { "/sys/sysDict/getDictListByCode" }, method = RequestMethod.GET)
	public FansResp getDictListByCode(@RequestParam(value = "code") String code);
	
	/**
	 * 
	 * @Title：getDictMapByCode
	 * @Description: 根据code来获取字典信息
	 * @author: fanhaohao
	 * @date 2020年1月13日 下午4:48:59 
	 * @param @param id
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = { "/sys/sysDict/getDictMapByCode" }, method = RequestMethod.GET)
	public FansResp getDictMapByCode(@RequestParam(value = "code") String code);
	
	/**
	 * 
	 * @Title：getDictMapListByCodes  
	 * @Description: 根据codes来获取字典信息
	 * @author: fanhaohao
	 * @date 2020年1月15日 上午10:36:05 
	 * @param @param codes
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = { "/sys/sysDict/getDictMapListByCodes" }, method = RequestMethod.GET)
	public FansResp getDictMapListByCodes(@RequestParam(value = "codes") String codes);


}
