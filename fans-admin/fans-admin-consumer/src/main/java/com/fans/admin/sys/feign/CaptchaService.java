package com.fans.admin.sys.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.fans.admin.sys.feign.fallback.CaptchaServiceFallbackImpl;

/**
 * 
 * @ClassName: CaptchaService  
 * @Description: 验证码相关接口 
 * @author fanhaohao
 * @date 2020年1月12日 上午11:22:20
 */
@FeignClient(name = "fans-admin-provider", fallback = CaptchaServiceFallbackImpl.class)
public interface CaptchaService {

	/**
	 * 
	 * @Title：captcha  
	 * @Description: 获取验证码
	 * @author: fanhaohao
	 * @date 2020年1月12日 上午11:21:56 
	 * @param  
	 * @return void 
	 * @throws
	 */
	@RequestMapping(value = { "/sys/captcha/captcha" })
	public byte[] captcha(@RequestParam(value = "captchaId") String captchaId) throws Exception;
	


}
