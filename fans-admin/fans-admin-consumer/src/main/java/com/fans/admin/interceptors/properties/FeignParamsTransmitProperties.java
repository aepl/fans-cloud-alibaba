package com.fans.admin.interceptors.properties;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName: FeignParamsTransmitProperties
 * @Description: 允许用FeignRequestInterceptor拦截器进行传送的参数
 * @author fanhaohao
 * @date 2020年2月7日 下午5:21:30
 */
@Configuration
@ConditionalOnExpression("!'${feign.params.transmit}'.isEmpty()")
@ConfigurationProperties(prefix = "feign.params.transmit")
public class FeignParamsTransmitProperties {

	private List<String> allows = new ArrayList<>();

	public List<String> getAllows() {
		return allows;
	}

	public void setAllows(List<String> allows) {
		this.allows = allows;
	}

}
