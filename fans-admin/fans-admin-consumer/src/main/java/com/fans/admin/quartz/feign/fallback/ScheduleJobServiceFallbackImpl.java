package com.fans.admin.quartz.feign.fallback;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fans.admin.quartz.entity.ScheduleJob;
import com.fans.admin.quartz.feign.ScheduleJobService;
import com.fans.common.utils.FansResp;
import com.fans.common.utils.JSON;

/**
 * 
 * @ClassName: ScheduleJobServiceFallbackImpl
 * @Description: 定时任务数据服务的fallback
 * @author fanhaohao
 * @date 2020年1月13日 下午4:14:38
 */
@Service
public class ScheduleJobServiceFallbackImpl implements ScheduleJobService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public FansResp getById(String id) {
		logger.error("根据id获取定时任务数据{}异常参数:{}", "getById", id);
		return FansResp.error();
	}

	@Override
	public FansResp list(ScheduleJob scheduleJob) {
		logger.error("分页获取定时任务list数据{}异常参数:{}", "list", JSON.toJSONString(scheduleJob));
		return FansResp.error();
	}

	@Override
	public FansResp create(Map<String, Object> scheduleJob) {
		logger.error("添加定时任务{}异常参数:{}", "create", JSON.toJSONString(scheduleJob));
		return FansResp.error();
	}

	@Override
	public FansResp update(Map<String, Object> scheduleJob) {
		logger.error("修改定时任务{}异常参数:{}", "update", JSON.toJSONString(scheduleJob));
		return FansResp.error();
	}

	@Override
	public FansResp delete(String ids) {
		logger.error("定时任务删除{}异常参数:{}", "update", ids);
		return FansResp.error();
	}

	@Override
	public FansResp run(Map<String, Object> entityMap) {
		logger.error("执行定时任务{}异常参数:{}", "run", JSON.toJSONString(entityMap));
		return FansResp.error();
	}

	@Override
	public FansResp pause(Map<String, Object> entityMap) {
		logger.error("暂停定时任务{}异常参数:{}", "pause", JSON.toJSONString(entityMap));
		return FansResp.error();
	}

	@Override
	public FansResp resume(Map<String, Object> entityMap) {
		logger.error("恢复定时任务{}异常参数:{}", "resume", JSON.toJSONString(entityMap));
		return FansResp.error();
	}

}
