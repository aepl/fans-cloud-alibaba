package com.fans.admin.sys.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fans.admin.sys.entity.SysLog;
import com.fans.admin.sys.feign.fallback.SysLogServiceFallbackImpl;
import com.fans.common.utils.FansResp;

/**
 * @ClassName: SysLogService
 * @Description:系统日志数据
 * @author fanhaohao
 * @date 2020年1月30日 上午11:18:00
 */
@FeignClient(name = "fans-admin-provider", fallback = SysLogServiceFallbackImpl.class)
public interface SysLogService {

	/**
	 * 
	 * @Title：getById  
	 * @Description: 根据id获取系统日志
	 * @author: fanhaohao
	 * @date 2020年1月13日 下午4:48:59 
	 * @param @param id
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = { "/sys/sysLog/getById" }, method = RequestMethod.GET)
	public FansResp getById(@RequestParam(value = "id") String id);
	
	/**
	 * 
	 * @Title：list  
	 * @Description: 获取系统日志列表
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:20:02 
	 * @param @param sysLog
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@RequestMapping(value = "/sys/sysLog/list", method = RequestMethod.GET)
	public FansResp list(@SpringQueryMap SysLog sysLog);
	
	
	/**
	 * 
	 * @Title：delete  
	 * @Description: 删除系统日志
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:22:39 
	 * @param @param ids
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/sys/sysLog/delete/{ids}", method = RequestMethod.DELETE)
	public FansResp delete(@PathVariable(value = "ids") String ids);
	
	/**
	 * 
	 * @Title：deleteAll  
	 * @Description: 清除所有系统日志
	 * @author: fanhaohao
	 * @date 2020年1月17日 上午10:07:48 
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/sys/sysLog/deleteAll", method = RequestMethod.DELETE)
	public FansResp deleteAll();
}
