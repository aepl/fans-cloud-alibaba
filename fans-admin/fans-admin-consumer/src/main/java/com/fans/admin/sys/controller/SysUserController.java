package com.fans.admin.sys.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fans.admin.sys.entity.SysUser;
import com.fans.admin.sys.feign.SysUserService;
import com.fans.common.base.BaseController;
import com.fans.common.utils.FansResp;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * @ClassName: SysUserController
 * @Description: 系统_用户数据接口
 * @author fanhaohao
 * @date 2020年1月16日 下午5:30:38
 */
@RestController
@RequestMapping("/sys/sysUser")
@Api(value = "系统用户数据接口", description = "系统用户数据接口")
public class SysUserController extends BaseController {

    @Autowired
	private SysUserService sysUserService;
    
    /**
     * 
     * @Title：getById  
     * @Description: 根据id获取系统_用户
     * @author: fanhaohao
     * @date 2020年1月8日 下午3:22:25 
     * @param @param id
     * @param @param request
     * @param @param response
     * @param @return 
     * @return String 
     * @throws
     */
    @RequestMapping(value = "/getById", method = RequestMethod.GET)
	@ApiOperation(value = "根据id获取用户信息", response = FansResp.class)
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "id", value = "id值", dataType = "String") 
	})
	public FansResp getById(String id) {
		return sysUserService.getById(id);
    }
	
    /**
     * 
     * @Title：list  
     * @Description: 分页获取系统_用户信息列表
     * @author: fanhaohao
     * @date 2020年1月15日 下午4:09:56 
     * @param @param sysUser
     * @param @return 
     * @return FansResp 
     * @throws
     */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public FansResp list(SysUser sysUser) {
		return sysUserService.list(sysUser);
	}

	/**
	 * 
	 * @Title：create  
	 * @Description: 创建系统_用户
	 * @author: fanhaohao
	 * @date 2020年1月16日 上午11:01:47 
	 * @param @param sysUser
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public FansResp create(@RequestBody Map<String, Object> entityMap) {
		return sysUserService.create(entityMap);
	}

	/**
	 * 
	 * @Title：update  
	 * @Description: 修改系统_用户
	 * @author: fanhaohao
	 * @date 2020年1月16日 上午11:01:59 
	 * @param @param sysUser
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public FansResp update(@RequestBody Map<String, Object> entityMap) {
		return sysUserService.update(entityMap);
	}

	/**
	 * 
	 * @Title：delete  
	 * @Description: 删除系统_用户
	 * @author: fanhaohao
	 * @date 2020年1月16日 上午11:02:10 
	 * @param @param ids
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public FansResp delete(@PathVariable(value = "ids") String ids) {
		return sysUserService.delete(ids);
	}

	/**
	 * 
	 * @Title：resetPwd  
	 * @Description: 重置密码
	 * @author: fanhaohao
	 * @date 2020年1月16日 上午11:02:21 
	 * @param @param id
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/resetPwd", method = RequestMethod.PUT)
	public FansResp resetPwd(@RequestBody SysUser sysUser) {
		return sysUserService.resetPwd(sysUser.getId());
	}
	
	/**
	 * 
	 * @Title：updatePwd  
	 * @Description: 修改密码
	 * @author: fanhaohao
	 * @date 2020年1月16日 上午11:02:32 
	 * @param @param pwd
	 * @param @param newpwd
	 * @param @param newpwd2
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/updatePwd", method = RequestMethod.PUT)
	public FansResp updatePwd(@RequestBody SysUser sysUser) {
		return sysUserService.updatePwd(sysUser.getPwd(), sysUser.getNewpwd(), sysUser.getNewpwd2());
	}
}
