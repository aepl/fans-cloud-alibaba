package com.fans.admin.sys.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fans.admin.sys.entity.SysRole;
import com.fans.admin.sys.feign.SysRoleService;
import com.fans.common.base.BaseController;
import com.fans.common.utils.FansResp;

/**
 * 
 * @ClassName: SysRoleController
 * @Description: 系统_角色数据接口
 * @author fanhaohao
 * @date 2020年1月13日 下午4:07:22
 */
@RestController
@RequestMapping("/sys/sysRole")
public class SysRoleController extends BaseController {
    @Autowired
	private SysRoleService sysRoleService;
    
	/**
     * 
     * @Title：getById  
     * @Description: 根据id获取系统_角色
     * @author: fanhaohao
     * @date 2020年1月13日 下午4:12:36 
     * @param @param id
     * @param @param request
     * @param @param response
     * @param @return 
     * @return FansResp 
     * @throws
     */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public FansResp getById(String id) {
		return sysRoleService.getById(id);
	}

	/**
	 * 
	 * @Title：list  
	 * @Description: 分页获取系统_角色数据
	 * @author: fanhaohao
	 * @date 2020年1月15日 下午4:03:18 
	 * @param @param sysRole
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public FansResp list(SysRole sysRole) {
		return sysRoleService.list(sysRole);
	}
	
	/**
	 * 
	 * @Title：listAll  
	 * @Description: 查询所有的角色列表信息
	 * @author: fanhaohao
	 * @date 2020年1月15日 下午4:16:10 
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/listAll", method = RequestMethod.GET)
	public FansResp listAll() {
		return sysRoleService.listAll();
	}
	
	/**
	 * 
	 * @Title：create  
	 * @Description: 添加角色信息
	 * @author: fanhaohao
	 * @date 2020年1月15日 下午4:30:52 
	 * @param @param sysRole
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public FansResp create(@RequestBody Map<String, Object> entityMap) {
		return sysRoleService.create(entityMap);
	}

	/**
	 * 
	 * @Title：update  
	 * @Description: 修改角色信息
	 * @author: fanhaohao
	 * @date 2020年1月15日 下午4:31:09 
	 * @param @param sysRole
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public FansResp update(@RequestBody Map<String, Object> entityMap) {
		return sysRoleService.update(entityMap);
	}

	/**
	 * 
	 * @Title：delete  
	 * @Description: 角色信息删除
	 * @author: fanhaohao
	 * @date 2020年1月16日 上午9:46:29 
	 * @param @param ids
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public FansResp delete(@PathVariable(value = "ids") String ids) {
		return sysRoleService.delete(ids);	
	}
}
