package com.fans.admin.sys.feign;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fans.admin.sys.entity.SysUser;
import com.fans.admin.sys.feign.fallback.SysUserServiceFallbackImpl;
import com.fans.common.utils.FansResp;

/**
 * @ClassName: SysUserGssService
 * @Description:系统用户信息
 * @author fanhaohao
 * @date 2020年1月30日 上午11:18:00
 */
@FeignClient(name = "fans-admin-provider", fallback = SysUserServiceFallbackImpl.class)
public interface SysUserService {

	/**
	 * 
	 * @Title：getById  
	 * @Description: 根据id获取系统_用户
	 * @author: fanhaohao
	 * @date 2020年1月8日 下午3:24:08 
	 * @param @param id
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@RequestMapping(value = { "/sys/sysUser/getById" }, method = RequestMethod.GET)
	public FansResp getById(@RequestParam(value = "id") String id);
	
	/**
	 * 
	 * @Title：list  
	 * @Description: 分页获取系统_用户信息列表
	 * @author: fanhaohao
	 * @date 2020年1月15日 下午4:06:48 
	 * @param @param sysUser
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = { "/sys/sysUser/list" }, method = RequestMethod.GET)
	public FansResp list(@SpringQueryMap SysUser sysUser);

	/**
	 * 
	 * @Title：create  
	 * @Description: 创建系统_用户
	 * @author: fanhaohao
	 * @date 2020年1月16日 上午10:53:21 
	 * @param @param sysUser
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/sys/sysUser/create", method = RequestMethod.POST)
	public FansResp create(@RequestParam("sysUser") Map<String, Object> sysUser);

	/**
	 * 
	 * @Title：update  
	 * @Description: 修改系统_用户
	 * @author: fanhaohao
	 * @date 2020年1月16日 上午10:53:39 
	 * @param @param sysUser
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/sys/sysUser/update", method = RequestMethod.PUT)
	public FansResp update(@RequestParam("sysUser") Map<String, Object> sysUser);

	/**
	 * 
	 * @Title：delete  
	 * @Description: 删除系统_用户
	 * @author: fanhaohao
	 * @date 2020年1月16日 上午10:53:54 
	 * @param @param ids
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/sys/sysUser/delete/{ids}", method = RequestMethod.DELETE)
	public FansResp delete(@PathVariable(value = "ids") String ids);

	/**
	 * 
	 * @Title：resetPwd  
	 * @Description: 重置密码
	 * @author: fanhaohao
	 * @date 2020年1月16日 上午10:54:11 
	 * @param @param id
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/sys/sysUser/resetPwd", method = RequestMethod.PUT)
	public FansResp resetPwd(@RequestParam(value = "id") String id);
    
	/**
	 * 
	 * @Title：updatePwd  
	 * @Description: 修改密码
	 * @author: fanhaohao
	 * @date 2020年1月16日 上午10:54:31 
	 * @param @param pwd
	 * @param @param newpwd
	 * @param @param newpwd2
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/sys/sysUser/updatePwd", method = RequestMethod.PUT)
	public FansResp updatePwd(@RequestParam(value = "pwd") String pwd, @RequestParam(value = "newpwd") String newpwd,
			@RequestParam(value = "newpwd2") String newpwd2);
}
