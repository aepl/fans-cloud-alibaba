package com.fans.admin.sys.feign;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fans.admin.sys.feign.fallback.SysRoleMenuServiceFallbackImpl;
import com.fans.common.utils.FansResp;

/**
 * @ClassName: SysRoleMenuGssService
 * @Description:系统_角色资源数据信息
 * @author fanhaohao
 * @date 2020年1月30日 上午11:18:00
 */
@FeignClient(name = "fans-admin-provider", fallback = SysRoleMenuServiceFallbackImpl.class)
public interface SysRoleMenuService {

	/**
	 * 
	 * @Title：getRoleMenu  
	 * @Description: 根据根据角色id获得所有资源菜单与角色所拥有的资源列表
	 * @author: fanhaohao
	 * @date 2020年1月19日 上午9:11:44 
	 * @param @param roleId
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = { "/sys/sysRoleMenu/getRoleMenu" }, method = RequestMethod.GET)
	public FansResp getRoleMenu(@RequestParam(value = "roleId") String roleId);
	

	/**
	 * 
	 * @Title：saveRoleMenu  
	 * @Description: 保存角色资源菜单
	 * @author: fanhaohao
	 * @date 2020年1月19日 上午9:35:53 
	 * @param @param entityMap
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/sys/sysRoleMenu/saveRoleMenu", method = RequestMethod.POST)
	public FansResp saveRoleMenu(@RequestParam("roleMenu") Map<String, Object> entityMap);


    
}
