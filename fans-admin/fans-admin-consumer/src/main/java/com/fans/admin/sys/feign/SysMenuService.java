package com.fans.admin.sys.feign;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fans.admin.sys.entity.SysMenu;
import com.fans.admin.sys.feign.fallback.SysMenuServiceFallbackImpl;
import com.fans.common.utils.FansResp;

/**
 * @ClassName: SysMenuService
 * @Description:系统_资源数据
 * @author fanhaohao
 * @date 2020年1月30日 上午11:18:00
 */
@FeignClient(name = "fans-admin-provider", fallback = SysMenuServiceFallbackImpl.class)
public interface SysMenuService {

	/**
	 * 
	 * @Title：getById  
	 * @Description: 根据id获取系统_资源
	 * @author: fanhaohao
	 * @date 2020年1月13日 下午4:48:59 
	 * @param @param id
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = { "/sys/sysMenu/getById" }, method = RequestMethod.GET)
	public FansResp getById(@RequestParam(value = "id") String id);
	
	/**
	 * 
	 * @Title：list  
	 * @Description: 获取系统_资源列表
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:20:02 
	 * @param @param sysMenu
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@RequestMapping(value = "/sys/sysMenu/list", method = RequestMethod.GET)
	public FansResp list(@SpringQueryMap SysMenu sysMenu);
	
	/**
	 * 
	 * @Title：create  
	 * @Description: 添加系统_资源
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:21:26 
	 * @param @param sysMenu
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/sys/sysMenu/create", method = RequestMethod.POST)
	public FansResp create(@RequestParam("sysMenu") Map<String, Object> sysMenu);
	
	/**
	 * 
	 * @Title：update  
	 * @Description: 修改系统_资源
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:22:46 
	 * @param @param sysMenu
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/sys/sysMenu/update", method = RequestMethod.PUT)
	public FansResp update(@RequestParam("sysMenu") Map<String, Object> sysMenu);
	
	/**
	 * 
	 * @Title：delete  
	 * @Description: 删除系统_资源
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:22:39 
	 * @param @param ids
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/sys/sysMenu/delete/{ids}", method = RequestMethod.DELETE)
	public FansResp delete(@PathVariable(value = "ids") String ids);
	

	/**
	 * 
	 * @Title：listAll  
	 * @Description: 查询所有的系统_资源
	 * @author: fanhaohao
	 * @date 2020年1月15日 下午4:14:10 
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = { "/sys/sysMenu/listAll" }, method = RequestMethod.GET)
	public FansResp listAll();
}
