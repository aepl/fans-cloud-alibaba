package com.fans.admin.sys.feign.fallback;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fans.admin.sys.entity.SysDict;
import com.fans.admin.sys.feign.SysDictService;
import com.fans.common.utils.FansResp;
import com.fans.common.utils.JSON;

/**
 * @ClassName: SysDictServiceFallbackImpl
 * @Description: 基础字典类型数据服务的fallback
 * @author fanhaohao
 * @date 2020年1月30日 上午11:18:18
 */
@Service
public class SysDictServiceFallbackImpl implements SysDictService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public FansResp getById(String id) {
		logger.error("根据id获取基础字典类型{}异常，参数:{}", "getById", id);
		return FansResp.error();
	}

	@Override
	public FansResp list(SysDict sysDict) {
		logger.error("获取基础字典类型列表{}异常，参数:{}", "list", JSON.toJSONString(sysDict));
		return FansResp.error();
	}

	@Override
	public FansResp create(Map<String, Object> sysDict) {
		logger.error("添加基础字典类型{}异常，参数:{}", "create", JSON.toJSONString(sysDict));
		return FansResp.error();
	}

	@Override
	public FansResp update(Map<String, Object> sysDict) {
		logger.error("修改基础字典类型{}异常，参数:{}", "update", JSON.toJSONString(sysDict));
		return FansResp.error();
	}

	@Override
	public FansResp delete(String ids) {
		logger.error("根据ids删除基础字典类型{}异常，参数:{}", "delete", ids);
		return FansResp.error();
	}

	@Override
	public FansResp getDictListByCode(String code) {
		logger.error("根据code来获取字典信息{}异常，参数:{}", "getDictListByCode", code);
		return FansResp.error();
	}

	@Override
	public FansResp getDictMapByCode(String code) {
		logger.error("根据code来获取字典信息{}异常，参数:{}", "getDictMapByCode", code);
		return FansResp.error();
	}

	@Override
	public FansResp getDictMapListByCodes(String codes) {
		logger.error("根据codes来获取字典信息{}异常，参数:{}", "getDictMapListByCodes", codes);
		return FansResp.error();
	}

}
