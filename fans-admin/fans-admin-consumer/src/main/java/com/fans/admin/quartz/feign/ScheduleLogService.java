package com.fans.admin.quartz.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fans.admin.quartz.entity.ScheduleLog;
import com.fans.admin.quartz.feign.fallback.ScheduleLogServiceFallbackImpl;
import com.fans.common.utils.FansResp;

/**
 * @ClassName: ScheduleLogService
 * @Description:定时任务执行日志数据
 * @author fanhaohao
 * @date 2020年1月30日 上午11:18:00
 */
@FeignClient(name = "fans-admin-provider", fallback = ScheduleLogServiceFallbackImpl.class)
public interface ScheduleLogService {

	/**
	 * 
	 * @Title：getById  
	 * @Description: 根据id获取定时任务执行日志
	 * @author: fanhaohao
	 * @date 2020年1月13日 下午4:48:59 
	 * @param @param id
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = { "/quartz/scheduleLog/getById" }, method = RequestMethod.GET)
	public FansResp getById(@RequestParam(value = "id") String id);
	
	/**
	 * 
	 * @Title：list  
	 * @Description: 获取定时任务执行日志列表
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:20:02 
	 * @param @param scheduleLog
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@RequestMapping(value = "/quartz/scheduleLog/list", method = RequestMethod.GET)
	public FansResp list(@SpringQueryMap ScheduleLog scheduleLog);
	
	
	/**
	 * 
	 * @Title：delete  
	 * @Description: 删除定时任务执行日志
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:22:39 
	 * @param @param ids
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/quartz/scheduleLog/delete/{ids}", method = RequestMethod.DELETE)
	public FansResp delete(@PathVariable(value = "ids") String ids);
	
	/**
	 * 
	 * @Title：deleteAll  
	 * @Description: 清除所有定时任务执行日志
	 * @author: fanhaohao
	 * @date 2020年1月17日 上午10:07:48 
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/quartz/scheduleLog/deleteAll", method = RequestMethod.DELETE)
	public FansResp deleteAll();
}
