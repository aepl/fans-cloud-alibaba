package com.fans.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @ClassName: AdminConsumerApplication
 * @Description: 启动类
 * @author fanhaohao
 * @date 2020年02月5日 下午3:13:28
 */
@SpringBootApplication(scanBasePackages = { "com.fans" })
@EnableDiscoveryClient
@EnableFeignClients
public class AdminConsumerApplication {
	public static void main(String[] args) {
		SpringApplication.run(AdminConsumerApplication.class, args);
	}

}
