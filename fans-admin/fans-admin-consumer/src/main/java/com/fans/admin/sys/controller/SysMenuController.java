package com.fans.admin.sys.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fans.admin.sys.entity.SysMenu;
import com.fans.admin.sys.feign.SysMenuService;
import com.fans.common.base.BaseController;
import com.fans.common.utils.FansResp;

/**
 * 
 * @ClassName: SysMenuController
 * @Description: 系统_资源数据接口
 * @author fanhaohao
 * @date 2020年1月16日 下午5:30:24
 */
@RestController
@RequestMapping("/sys/sysMenu")
public class SysMenuController extends BaseController {

    

    @Autowired
	private SysMenuService sysMenuService;
    /**
     * 
     * @Title：getById  
     * @Description: 根据id获取系统_资源
     * @author: fanhaohao
     * @date 2020年1月16日 下午5:28:27 
     * @param @param id
     * @param @return 
     * @return FansResp 
     * @throws
     */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public FansResp getById(String id) {
		return sysMenuService.getById(id);
	}
	
	/**
	 * 
	 * @Title：list  
	 * @Description: 获取系统_资源列表
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:27:47 
	 * @param @param sysMenu
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public FansResp list(SysMenu sysMenu) {
		return sysMenuService.list(sysMenu);
	}
	
	/**
	 * 
	 * @Title：create  
	 * @Description: 添加系统_资源
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:21:26 
	 * @param @param sysMenu
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public FansResp create(@RequestBody Map<String, Object> sysMenu){
		return sysMenuService.create(sysMenu);
	}

	/**
	 * 
	 * @Title：update  
	 * @Description: 修改系统_资源
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:22:46 
	 * @param @param sysMenu
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public FansResp update(@RequestBody Map<String, Object> sysMenu){
		return sysMenuService.update(sysMenu);
	}
	
	/**
	 * 
	 * @Title：delete  
	 * @Description: 删除系统_资源
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:22:39 
	 * @param @param ids
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public FansResp delete(@PathVariable(value = "ids") String ids){
		return sysMenuService.delete(ids);
	}
	
	/**
	 * 
	 * @Title：listAll  
	 * @Description: 查询所有的系统_资源
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午5:07:45 
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/listAll", method = RequestMethod.GET)
	public FansResp listAll() {
		return sysMenuService.listAll();
	}

}
