package com.fans.admin.sys.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fans.admin.sys.entity.SysDict;
import com.fans.admin.sys.feign.SysDictService;
import com.fans.common.base.BaseController;
import com.fans.common.utils.FansResp;

/**
 * 
 * @ClassName: SysDictController
 * @Description: 基础字典类型数据接口
 * @author fanhaohao
 * @date 2020年1月16日 下午5:30:02
 */
@RestController
@RequestMapping("/sys/sysDict")
public class SysDictController extends BaseController {
    @Autowired
	private SysDictService sysDictService;
    
    /**
     * 
     * @Title：getById  
     * @Description: 根据id获取基础字典类型
     * @author: fanhaohao
     * @date 2020年1月16日 下午5:27:55 
     * @param @param id
     * @param @return 
     * @return FansResp 
     * @throws
     */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public FansResp getById(String id) {
		return sysDictService.getById(id);
	}
	
	/**
	 * 
	 * @Title：list  
	 * @Description: 获取基础字典类型列表
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:27:47 
	 * @param @param sysDict
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public FansResp list(SysDict sysDict) {
		return sysDictService.list(sysDict);
	}
	
	/**
	 * 
	 * @Title：create  
	 * @Description: 添加基础字典类型
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:21:26 
	 * @param @param sysDict
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public FansResp create(@RequestBody Map<String, Object> sysDict){
		return sysDictService.create(sysDict);
	}

	/**
	 * 
	 * @Title：update  
	 * @Description: 修改基础字典类型
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:22:46 
	 * @param @param sysDict
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public FansResp update(@RequestBody Map<String, Object> sysDict){
		return sysDictService.update(sysDict);
	}
	
	/**
	 * 
	 * @Title：delete  
	 * @Description: 删除基础字典类型
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:22:39 
	 * @param @param ids
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public FansResp delete(@PathVariable(value = "ids") String ids){
		return sysDictService.delete(ids);
	}
	
	/**
     * @Title：getDictMapByCode
     * @Description: 根据code来获取字典信息
     * @author: fanhaohao
     * @date 2018年12月30 18:18:58
     * @param @param code
     * @param @param request
     * @param @param response
     * @param @return 
     * @return String 
     * @throws
     */
	@RequestMapping(value = "/getDictListByCode", method = RequestMethod.GET)
	public FansResp getDictListByCode(String code) {
		return sysDictService.getDictListByCode(code);
	}
	
	/**
     * @Title：getDictMapByCode
     * @Description: 根据code来获取字典信息
     * @author: fanhaohao
     * @date 2018年12月30 18:18:58
     * @param @param code
     * @param @param request
     * @param @param response
     * @param @return 
     * @return String 
     * @throws
     */
	@RequestMapping(value = "/getDictMapByCode", method = RequestMethod.GET)
	public FansResp getDictMapByCode(String code) {
		return sysDictService.getDictMapByCode(code);
	}
	
	/**
	 * 
	 * @Title：getDictMapListByCodes  
	 * @Description: 根据codes来获取字典信息
	 * @author: fanhaohao
	 * @date 2020年1月15日 上午10:38:21 
	 * @param @param codes
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/getDictMapListByCodes", method = RequestMethod.GET)
	public FansResp getDictMapListByCodes(String codes) {
		return sysDictService.getDictMapListByCodes(codes);
	}

}
