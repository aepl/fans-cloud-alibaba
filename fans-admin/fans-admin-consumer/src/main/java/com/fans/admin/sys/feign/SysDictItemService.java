package com.fans.admin.sys.feign;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fans.admin.sys.entity.SysDictItem;
import com.fans.admin.sys.feign.fallback.SysDictItemServiceFallbackImpl;
import com.fans.common.utils.FansResp;

/**
 * @ClassName: SysDictItemService
 * @Description:基础字典项数据
 * @author fanhaohao
 * @date 2020年1月30日 上午11:18:00
 */
@FeignClient(name = "fans-admin-provider", fallback = SysDictItemServiceFallbackImpl.class)
public interface SysDictItemService {

	/**
	 * 
	 * @Title：getById  
	 * @Description: 根据id获取基础字典项
	 * @author: fanhaohao
	 * @date 2020年1月13日 下午4:48:59 
	 * @param @param id
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = { "/sys/sysDictItem/getById" }, method = RequestMethod.GET)
	public FansResp getById(@RequestParam(value = "id") String id);
	
	/**
	 * 
	 * @Title：list  
	 * @Description: 获取基础字典项列表
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:20:02 
	 * @param @param sysDictItem
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@RequestMapping(value = "/sys/sysDictItem/list", method = RequestMethod.GET)
	public FansResp list(@SpringQueryMap SysDictItem sysDictItem);
	
	/**
	 * 
	 * @Title：create  
	 * @Description: 添加基础字典项
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:21:26 
	 * @param @param sysDictItem
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/sys/sysDictItem/create", method = RequestMethod.POST)
	public FansResp create(@RequestParam("sysDictItem") Map<String, Object> sysDictItem);
	
	/**
	 * 
	 * @Title：update  
	 * @Description: 修改基础字典项
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:22:46 
	 * @param @param sysDictItem
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/sys/sysDictItem/update", method = RequestMethod.PUT)
	public FansResp update(@RequestParam("sysDictItem") Map<String, Object> sysDictItem);
	
	/**
	 * 
	 * @Title：delete  
	 * @Description: 删除基础字典项
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:22:39 
	 * @param @param ids
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/sys/sysDictItem/delete/{ids}", method = RequestMethod.DELETE)
	public FansResp delete(@PathVariable(value = "ids") String ids);
	


}
