package com.fans.admin.test.feign;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fans.admin.test.entity.TestDict;
import com.fans.admin.test.feign.fallback.TestDictServiceFallbackImpl;
import com.fans.common.utils.FansResp;


/**
 * @ClassName:TestDict
 * @description: 测试服务类
 * @date: 2020-04-15
 * @author: fanhaohao
 * @version: 1.0
 * @copyright: 版权所有 fans (c)2020 
 */ 
@FeignClient(name = "fans-admin-provider", fallback = TestDictServiceFallbackImpl.class)
public interface TestDictService {

	/**
	 * @Title：getById   
	 * @Description: 根据id获取测试数据
	 * @author: fanhaohao
	 * @date 2020-04-15
	 * @param page
	 * @return FansResp
	 */ 
	@RequestMapping(value = { "/test/testDict/getById" }, method = RequestMethod.GET)
	public FansResp getById(@RequestParam(value = "id") String id);
	
	/**
	 * 
	 * @Title：list  
	 * @Description: 分页获取测试数据
	 * @author: fanhaohao
	 * @date 2020-04-15 
	 * @param testDict
	 * @return FansResp 
	 */
	@RequestMapping(value = { "/test/testDict/list" }, method = RequestMethod.GET)
	public FansResp list(@SpringQueryMap TestDict testDict);

	/**
	 * 
	 * @Title：create  
	 * @Description: 添加测试信息
	 * @author: fanhaohao
	 * @date 2020-04-15 
	 * @param testDict
	 * @return FansResp 
	 */
	@RequestMapping(value = "/test/testDict/create", method = RequestMethod.POST)
	public FansResp create(@RequestParam("testDict") Map<String, Object> testDict);
	
	/**
	 * 
	 * @Title：update  
	 * @Description: 修改测试信息
	 * @author: fanhaohao
	 * @date 2020-04-15 
	 * @param testDict
	 * @return FansResp 
	 */
	@RequestMapping(value = "/test/testDict/update", method = RequestMethod.PUT)
	public FansResp update(@RequestParam("testDict") Map<String, Object> testDict);
	
	/**
	 * 
	 * @Title：delete  
	 * @Description: 测试信息删除
	 * @author: fanhaohao
	 * @date 2020-04-15 
	 * @param ids
	 * @return FansResp 
	 */
	@RequestMapping(value = "/test/testDict/delete/{ids}", method = RequestMethod.DELETE)
	public FansResp delete(@PathVariable(value = "ids") String ids);

}
