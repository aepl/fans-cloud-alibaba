package com.fans.admin.test.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fans.admin.test.entity.TestDict;
import com.fans.admin.test.feign.TestDictService;
import com.fans.common.base.BaseController;
import com.fans.common.utils.FansResp;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * @ClassName TestDictController
 * @description: 测试控制层
 * @date: 2020-04-15
 * @author: fanhaohao
 * @version: 1.0
 * @copyright: 版权所有 fans (c)2020 
 */
@RestController
@RequestMapping("/test/testDict")
@Api(value = "测试接口", tags = "测试相关接口")
public class TestDictController extends BaseController {
    @Autowired
	private TestDictService testDictService;
    
    /**
     * @Title：getById
     * @Description: 根据id获取测试
     * @author: fanhaohao
     * @date 2020-04-15
     * @param id
     * @return FansResp 
     */ 
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	@ApiOperation(value = "根据id获取测试信息", response = TestDict.class)
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "id值", dataType = "String") })
	public FansResp getById(String id) {
		return testDictService.getById(id);
	}

	/**
	 * @Title：list  
	 * @Description: 获取测试列表
	 * @author: fanhaohao
	 * @date 2020-04-15
	 * @param TestDict
	 * @return FansResp 
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@ApiOperation(value = "获取测试列表信息", response = TestDict.class)
	@ApiImplicitParams({ @ApiImplicitParam(name = "testDict", value = "测试实体信息", dataType = "TestDict") })
	public FansResp list(TestDict testDict) {
		return testDictService.list(testDict);
	}
	
	/**
	 * @Title：create  
	 * @Description: 新建测试
	 * @author: fanhaohao
	 * @date 2020-04-15
	 * @param entityMap
	 * @return FansResp 
	 */ 
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ApiOperation(value = "新增测试信息", response = FansResp.class)
	@ApiImplicitParams({ @ApiImplicitParam(name = "entityMap", value = "测试实体信息", dataType = "Map") })
	public FansResp create(@RequestBody Map<String, Object> entityMap) {
		return testDictService.create(entityMap);
	}

	/**
	 * @Title：update  
	 * @Description: 修改测试
	 * @author: fanhaohao
	 * @date 2020-04-15
	 * @param entityMap
	 * @return FansResp 
	 */ 
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	@ApiOperation(value = "修改测试信息", response = FansResp.class)
	@ApiImplicitParams({ @ApiImplicitParam(name = "entityMap", value = "测试实体信息", dataType = "Map") })
	public FansResp update(@RequestBody Map<String, Object> entityMap) {
		return testDictService.update(entityMap);
	}

	/**
	 * @Title：delete  
	 * @Description: 删除测试
	 * @author: fanhaohao
	 * @date 2020-04-15
	 * @param ids
	 * @return FansResp 
	 */ 
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	@ApiOperation(value = "根据ids删除测试信息", response = FansResp.class)
	@ApiImplicitParams({ @ApiImplicitParam(name = "ids", value = "ids值", dataType = "String") })
	public FansResp delete(@PathVariable(value = "ids") String ids) {
		return testDictService.delete(ids);	
	}
}
