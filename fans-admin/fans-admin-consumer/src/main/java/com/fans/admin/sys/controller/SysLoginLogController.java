package com.fans.admin.sys.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fans.admin.sys.entity.SysLoginLog;
import com.fans.admin.sys.feign.SysLoginLogService;
import com.fans.common.base.BaseController;
import com.fans.common.utils.FansResp;

/**
 * 
 * @ClassName: SysLoginLogController
 * @Description: 用户登录日志数据接口
 * @author fanhaohao
 * @date 2020年1月16日 下午5:29:48
 */
@RestController
@RequestMapping("/sys/sysLoginLog")
public class SysLoginLogController extends BaseController {

    @Autowired
	private SysLoginLogService sysLoginLogService;
    
    /**
     * 
     * @Title：getById  
     * @Description: 根据id获取用户登录日志
     * @author: fanhaohao
     * @date 2020年1月16日 下午5:27:31 
     * @param @param id
     * @param @return 
     * @return FansResp 
     * @throws
     */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public FansResp getById(String id) {
		return sysLoginLogService.getById(id);
	}
	
	/**
	 * 
	 * @Title：list  
	 * @Description: 获取用户登录日志列表
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:27:47 
	 * @param @param sysLoginLog
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public FansResp list(SysLoginLog sysLoginLog) {
		return sysLoginLogService.list(sysLoginLog);
	}
	
	/**
	 * 
	 * @Title：delete  
	 * @Description: 删除用户登录日志
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:22:39 
	 * @param @param ids
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public FansResp delete(@PathVariable(value = "ids") String ids){
		return sysLoginLogService.delete(ids);
	}
	
	/**
	 * 
	 * @Title：deleteAll  
	 * @Description: 清除所有用户登录日志
	 * @author: fanhaohao
	 * @date 2020年1月17日 上午10:09:19 
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/deleteAll", method = RequestMethod.DELETE)
	public FansResp deleteAll(){
		return sysLoginLogService.deleteAll();
	}

}
