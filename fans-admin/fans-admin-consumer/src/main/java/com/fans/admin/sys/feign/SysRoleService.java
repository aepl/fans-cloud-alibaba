package com.fans.admin.sys.feign;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fans.admin.sys.entity.SysRole;
import com.fans.admin.sys.feign.fallback.SysRoleServiceFallbackImpl;
import com.fans.common.utils.FansResp;

/**
 * @ClassName: SysUserService
 * @Description:系统_角色数据
 * @author fanhaohao
 * @date 2019年1月30日 上午11:18:00
 */
@FeignClient(name = "fans-admin-provider", fallback = SysRoleServiceFallbackImpl.class)
public interface SysRoleService {

	/**
	 * 
	 * @Title：getById  
	 * @Description: 根据id获取系统_角色数据
	 * @author: fanhaohao
	 * @date 2020年1月8日 下午3:24:08 
	 * @param @param id
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@RequestMapping(value = { "/sys/sysRole/getById" }, method = RequestMethod.GET)
	public FansResp getById(@RequestParam(value = "id") String id);
	
	/**
	 * 
	 * @Title：list  
	 * @Description: 分页获取系统_角色数据
	 * @author: fanhaohao
	 * @date 2020年1月15日 下午3:59:21 
	 * @param @param sysRole
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = { "/sys/sysRole/list" }, method = RequestMethod.GET)
	public FansResp list(@SpringQueryMap SysRole sysRole);

	/**
	 * 
	 * @Title：listAll  
	 * @Description: 查询所有的角色列表信息
	 * @author: fanhaohao
	 * @date 2020年1月15日 下午4:14:10 
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = { "/sys/sysRole/listAll" }, method = RequestMethod.GET)
	public FansResp listAll();
	
	/**
	 * 
	 * @Title：create  
	 * @Description: 添加角色信息
	 * @author: fanhaohao
	 * @date 2020年1月15日 下午4:27:17 
	 * @param @param sysRole
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/sys/sysRole/create", method = RequestMethod.POST)
	public FansResp create(@RequestParam("sysRole") Map<String, Object> sysRole);
	
	/**
	 * 
	 * @Title：update  
	 * @Description: 修改角色信息
	 * @author: fanhaohao
	 * @date 2020年1月15日 下午4:28:53 
	 * @param @param sysRole
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/sys/sysRole/update", method = RequestMethod.PUT)
	public FansResp update(@RequestParam("sysRole") Map<String, Object> sysRole);
	
	/**
	 * 
	 * @Title：delete  
	 * @Description: 角色信息删除
	 * @author: fanhaohao
	 * @date 2020年1月16日 上午9:45:05 
	 * @param @param ids
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/sys/sysRole/delete/{ids}", method = RequestMethod.DELETE)
	public FansResp delete(@PathVariable(value = "ids") String ids);

}
