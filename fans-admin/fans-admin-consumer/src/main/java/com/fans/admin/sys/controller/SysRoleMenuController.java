package com.fans.admin.sys.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fans.admin.sys.feign.SysRoleMenuService;
import com.fans.common.base.BaseController;
import com.fans.common.utils.FansResp;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * @ClassName: SysRoleMenuController
 * @Description: 系统_角色资源数据数据接口
 * @author fanhaohao
 * @date 2020年1月16日 下午5:30:38
 */
@RestController
@RequestMapping("/sys/sysRoleMenu")
@Api(value = "系统用户数据接口", description = "系统用户数据接口")
public class SysRoleMenuController extends BaseController {

    @Autowired
	private SysRoleMenuService sysRoleMenuService;
    
   /**
    * 
    * @Title：getRoleMenu  
    * @Description: 根据根据角色id获得所有资源菜单与角色所拥有的资源列表
    * @author: fanhaohao
    * @date 2020年1月19日 上午9:16:20 
    * @param @param roleId
    * @param @return 
    * @return FansResp 
    * @throws
    */
    @RequestMapping(value = "/getRoleMenu", method = RequestMethod.GET)
	@ApiOperation(value = "根据根据角色id获得所有资源菜单与角色所拥有的资源列表", response = FansResp.class)
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "roleId", value = "roleId值", dataType = "String") 
	})
	public FansResp getRoleMenu(String roleId) {
		return sysRoleMenuService.getRoleMenu(roleId);
    }
	

	
	/**
	 * 
	 * @Title：saveRoleMenu  
	 * @Description: 保存角色资源菜单
	 * @author: fanhaohao
	 * @date 2020年1月19日 上午9:18:33 
	 * @param @param ids
	 * @param @param roleId
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
    @ApiOperation(value = "保存角色资源菜单", response = FansResp.class)
	@ApiImplicitParams({ 
			@ApiImplicitParam(name = "entityMap", value = "ids（(资源菜单ID)）,roleId(角色id)值；如{\"ids\":\"f0b63fca80d8462c86ea204139517623,28,1\",\"roleId\":\"b2c533e760a64876ba81e11f0de9062e\"}")
	})
	@RequestMapping(value = "/saveRoleMenu", method = RequestMethod.POST)
	public FansResp saveRoleMenu(@RequestBody Map<String, Object> entityMap) {
		return sysRoleMenuService.saveRoleMenu(entityMap);
	}
}
