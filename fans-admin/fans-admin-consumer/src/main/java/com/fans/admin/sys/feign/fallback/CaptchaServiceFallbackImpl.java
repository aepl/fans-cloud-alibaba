package com.fans.admin.sys.feign.fallback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fans.admin.sys.feign.CaptchaService;

/**
 * 
 * @ClassName: SysUserGssServiceFallbackImpl  
 * @Description: 验证码相关接口 的fallback
 * @author fanhaohao
 * @date 2020年1月12日 上午11:22:20
 */
@Service
public class CaptchaServiceFallbackImpl implements CaptchaService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
	 * 
	 * @Title：captcha  
	 * @Description: 获取验证码
	 * @author: fanhaohao
	 * @date 2020年1月12日 上午11:21:56 
	 * @param  
	 * @return void 
	 * @throws
	 */
	@Override
	public byte[] captcha(String captchaId) throws Exception {
		logger.error("获取验证码异常,参数{}", captchaId);
		return new byte[0];
	}

}
