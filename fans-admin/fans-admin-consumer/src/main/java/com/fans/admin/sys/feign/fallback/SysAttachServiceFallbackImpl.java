package com.fans.admin.sys.feign.fallback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fans.admin.sys.entity.SysAttach;
import com.fans.admin.sys.feign.SysAttachService;
import com.fans.common.utils.FansResp;
import com.fans.common.utils.JSON;

/**
 * @ClassName: SysAttachServiceFallbackImpl
 * @Description: 系统_附件数据服务的fallback
 * @author fanhaohao
 * @date 2020年1月30日 上午11:18:18
 */
@Service
public class SysAttachServiceFallbackImpl implements SysAttachService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public FansResp getById(String id) {
		logger.error("根据id获取系统_附件{}异常，参数:{}", "getById", id);
		return FansResp.error();
	}

	@Override
	public FansResp list(SysAttach sysAttach) {
		logger.error("获取系统_附件列表{}异常，参数:{}", "list", JSON.toJSONString(sysAttach));
		return FansResp.error();
	}

	@Override
	public FansResp delete(String ids) {
		logger.error("根据ids删除系统_附件{}异常，参数:{}", "delete", ids);
		return FansResp.error();
	}


}
