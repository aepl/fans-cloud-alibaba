package com.fans.admin.sys.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fans.admin.sys.entity.SysAttach;
import com.fans.admin.sys.feign.fallback.SysAttachServiceFallbackImpl;
import com.fans.common.utils.FansResp;

/**
 * @ClassName: SysAttachService
 * @Description:系统_附件数据
 * @author fanhaohao
 * @date 2020年1月30日 上午11:18:00
 */
@FeignClient(name = "fans-admin-provider", fallback = SysAttachServiceFallbackImpl.class)
public interface SysAttachService {

	/**
	 * 
	 * @Title：getById  
	 * @Description: 根据id获取系统_附件
	 * @author: fanhaohao
	 * @date 2020年1月13日 下午4:48:59 
	 * @param @param id
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = { "/sys/sysAttach/getById" }, method = RequestMethod.GET)
	public FansResp getById(@RequestParam(value = "id") String id);
	
	/**
	 * 
	 * @Title：list  
	 * @Description: 获取系统_附件列表
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:20:02 
	 * @param @param sysAttach
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@RequestMapping(value = "/sys/sysAttach/list", method = RequestMethod.GET)
	public FansResp list(@SpringQueryMap SysAttach sysAttach);
	
	
	/**
	 * 
	 * @Title：delete  
	 * @Description: 删除系统_附件
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:22:39 
	 * @param @param ids
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/sys/sysAttach/delete/{ids}", method = RequestMethod.DELETE)
	public FansResp delete(@PathVariable(value = "ids") String ids);
	


}
