package com.fans.admin.sys.feign.fallback;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.fans.admin.sys.entity.SysUser;
import com.fans.admin.sys.feign.SysUserService;
import com.fans.common.utils.FansResp;

/**
 * @ClassName: SysUserGssServiceFallbackImpl
 * @Description: 系统用户信息服务的fallback
 * @author fanhaohao
 * @date 2020年1月30日 上午11:18:18
 */
@Service
public class SysUserServiceFallbackImpl implements SysUserService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public FansResp getById(String id) {
		logger.error("根据id获取系统_用户{}异常参数:{}", "getById", id);
		return FansResp.error();
	}

	@Override
	public FansResp list(SysUser sysUser) {
		logger.error("分页获取系统_用户信息列表{}异常参数:{}", "list", JSON.toJSONString(sysUser));
		return FansResp.error();
	}

	@Override
	public FansResp create(Map<String, Object> sysUser) {
		logger.error("创建系统_用户信息{}异常参数:{}", "create", JSON.toJSONString(sysUser));
		return FansResp.error();
	}

	@Override
	public FansResp update(Map<String, Object> sysUser) {
		logger.error("修改系统_用户信息{}异常参数:{}", "update", JSON.toJSONString(sysUser));
		return FansResp.error();
	}

	@Override
	public FansResp delete(String ids) {
		logger.error("根据ids删除系统_用户{}异常参数:{}", "delete", ids);
		return FansResp.error();
	}

	@Override
	public FansResp resetPwd(String id) {
		logger.error("根据id重置密码{}异常参数:{}", "resetPwd", id);
		return FansResp.error();
	}

	@Override
	public FansResp updatePwd(String pwd, String newpwd, String newpwd2) {
		logger.error("修改用户密码{}异常参数:pwd{},newpwd{},newpwd2{}", "resetPwd", pwd, newpwd, newpwd2);
		return FansResp.error();
	}

}
