package com.fans.admin.sys.feign.fallback;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fans.admin.sys.entity.SysDictItem;
import com.fans.admin.sys.feign.SysDictItemService;
import com.fans.common.utils.FansResp;
import com.fans.common.utils.JSON;

/**
 * @ClassName: SysDictItemServiceFallbackImpl
 * @Description: 基础字典项数据服务的fallback
 * @author fanhaohao
 * @date 2020年1月30日 上午11:18:18
 */
@Service
public class SysDictItemServiceFallbackImpl implements SysDictItemService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public FansResp getById(String id) {
		logger.error("根据id获取基础字典项{}异常，参数:{}", "getById", id);
		return FansResp.error();
	}

	@Override
	public FansResp list(SysDictItem sysDictItem) {
		logger.error("获取基础字典项列表{}异常，参数:{}", "list", JSON.toJSONString(sysDictItem));
		return FansResp.error();
	}

	@Override
	public FansResp create(Map<String, Object> sysDictItem) {
		logger.error("添加基础字典项{}异常，参数:{}", "create", JSON.toJSONString(sysDictItem));
		return FansResp.error();
	}

	@Override
	public FansResp update(Map<String, Object> sysDictItem) {
		logger.error("修改基础字典项{}异常，参数:{}", "update", JSON.toJSONString(sysDictItem));
		return FansResp.error();
	}

	@Override
	public FansResp delete(String ids) {
		logger.error("根据ids删除基础字典项{}异常，参数:{}", "delete", ids);
		return FansResp.error();
	}


}
