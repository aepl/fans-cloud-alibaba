package com.fans.admin.sys.controller;

import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fans.admin.sys.feign.CaptchaService;
import com.fans.common.base.BaseController;
import com.fans.common.utils.ServletUtils;

/**
 * 
 * @ClassName: CaptchaController  
 * @Description: 验证码相关接口  
 * @author fanhaohao
 * @date 2020年1月12日 上午11:25:30
 */
@RestController
@RequestMapping("/sys/captcha")
public class CaptchaController extends BaseController {

    @Autowired
    private CaptchaService captchaService;
    
    
    /**
     * @throws Exception 
	 * @Title：captcha  
	 * @Description: 获取验证码
	 * @author: fanhaohao
	 * @date 2020年1月12日 上午11:15:01 
	 * @param @param request
	 * @param @param response 
	 * @return void 
	 * @throws
	 */
    @RequestMapping(value = "/captcha")
	public void captcha(String captchaId) throws Exception {
		buildImageRes(captchaService.captcha(captchaId));
    }
    /**
     * 
     * @Title：buildImageRes  
     * @Description: 创建Response
     * @author: fanhaohao
     * @date 2020年1月12日 下午12:33:06 
     * @param @param bytes
     * @param @throws IOException 
     * @return void 
     * @throws
     */
    private void buildImageRes(byte[] bytes) throws IOException {
    	HttpServletResponse response = ServletUtils.getImageRes();
        ServletOutputStream responseOutputStream = response.getOutputStream();
        responseOutputStream.write(bytes);
        responseOutputStream.flush();
        responseOutputStream.close();
    }
}
