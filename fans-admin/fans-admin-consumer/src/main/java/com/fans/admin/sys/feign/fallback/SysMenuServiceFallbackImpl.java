package com.fans.admin.sys.feign.fallback;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fans.admin.sys.entity.SysMenu;
import com.fans.admin.sys.feign.SysMenuService;
import com.fans.common.utils.FansResp;
import com.fans.common.utils.JSON;

/**
 * @ClassName: SysMenuServiceFallbackImpl
 * @Description: 系统_资源数据服务的fallback
 * @author fanhaohao
 * @date 2020年1月30日 上午11:18:18
 */
@Service
public class SysMenuServiceFallbackImpl implements SysMenuService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public FansResp getById(String id) {
		logger.error("根据id获取系统_资源{}异常，参数:{}", "getById", id);
		return FansResp.error();
	}

	@Override
	public FansResp list(SysMenu sysMenu) {
		logger.error("获取系统_资源列表{}异常，参数:{}", "list", JSON.toJSONString(sysMenu));
		return FansResp.error();
	}

	@Override
	public FansResp create(Map<String, Object> sysMenu) {
		logger.error("添加系统_资源{}异常，参数:{}", "create", JSON.toJSONString(sysMenu));
		return FansResp.error();
	}

	@Override
	public FansResp update(Map<String, Object> sysMenu) {
		logger.error("修改系统_资源{}异常，参数:{}", "update", JSON.toJSONString(sysMenu));
		return FansResp.error();
	}

	@Override
	public FansResp delete(String ids) {
		logger.error("根据ids删除系统_资源{}异常，参数:{}", "delete", ids);
		return FansResp.error();
	}

	@Override
	public FansResp listAll() {
		logger.error("查询所有的系统_资源{}异常", "listAll");
		return FansResp.error();
	}

}
