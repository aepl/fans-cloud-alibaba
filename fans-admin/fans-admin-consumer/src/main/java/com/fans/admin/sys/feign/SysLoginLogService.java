package com.fans.admin.sys.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fans.admin.sys.entity.SysLoginLog;
import com.fans.admin.sys.feign.fallback.SysLoginLogServiceFallbackImpl;
import com.fans.common.utils.FansResp;

/**
 * @ClassName: SysLoginLogService
 * @Description:用户登录日志数据
 * @author fanhaohao
 * @date 2020年1月30日 上午11:18:00
 */
@FeignClient(name = "fans-admin-provider", fallback = SysLoginLogServiceFallbackImpl.class)
public interface SysLoginLogService {

	/**
	 * 
	 * @Title：getById  
	 * @Description: 根据id获取用户登录日志
	 * @author: fanhaohao
	 * @date 2020年1月13日 下午4:48:59 
	 * @param @param id
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = { "/sys/sysLoginLog/getById" }, method = RequestMethod.GET)
	public FansResp getById(@RequestParam(value = "id") String id);
	
	/**
	 * 
	 * @Title：list  
	 * @Description: 获取用户登录日志列表
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:20:02 
	 * @param @param sysLoginLog
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@RequestMapping(value = "/sys/sysLoginLog/list", method = RequestMethod.GET)
	public FansResp list(@SpringQueryMap SysLoginLog sysLoginLog);
	
	
	/**
	 * 
	 * @Title：delete  
	 * @Description: 删除用户登录日志
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:22:39 
	 * @param @param ids
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/sys/sysLoginLog/delete/{ids}", method = RequestMethod.DELETE)
	public FansResp delete(@PathVariable(value = "ids") String ids);
	
	/**
	 * 
	 * @Title：deleteAll  
	 * @Description: 清除所有用户登录日志
	 * @author: fanhaohao
	 * @date 2020年1月17日 上午10:07:48 
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/sys/sysLoginLog/deleteAll", method = RequestMethod.DELETE)
	public FansResp deleteAll();
}
