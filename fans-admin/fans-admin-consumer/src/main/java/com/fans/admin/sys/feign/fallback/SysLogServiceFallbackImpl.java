package com.fans.admin.sys.feign.fallback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fans.admin.sys.entity.SysLog;
import com.fans.admin.sys.feign.SysLogService;
import com.fans.common.utils.FansResp;
import com.fans.common.utils.JSON;

/**
 * @ClassName: SysLogServiceFallbackImpl
 * @Description: 系统日志数据服务的fallback
 * @author fanhaohao
 * @date 2020年1月30日 上午11:18:18
 */
@Service
public class SysLogServiceFallbackImpl implements SysLogService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public FansResp getById(String id) {
		logger.error("根据id获取系统日志{}异常，参数:{}", "getById", id);
		return FansResp.error();
	}

	@Override
	public FansResp list(SysLog sysLog) {
		logger.error("获取系统日志列表{}异常，参数:{}", "list", JSON.toJSONString(sysLog));
		return FansResp.error();
	}

	@Override
	public FansResp delete(String ids) {
		logger.error("根据ids删除系统日志{}异常，参数:{}", "delete", ids);
		return FansResp.error();
	}

	@Override
	public FansResp deleteAll() {
		logger.error("清除所有系统日志{}异常", "delete");
		return FansResp.error();
	}

}
