package com.fans.admin.sys.feign.fallback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fans.admin.sys.entity.SysLoginLog;
import com.fans.admin.sys.feign.SysLoginLogService;
import com.fans.common.utils.FansResp;
import com.fans.common.utils.JSON;

/**
 * @ClassName: SysLoginLogServiceFallbackImpl
 * @Description: 用户登录日志数据服务的fallback
 * @author fanhaohao
 * @date 2020年1月30日 上午11:18:18
 */
@Service
public class SysLoginLogServiceFallbackImpl implements SysLoginLogService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public FansResp getById(String id) {
		logger.error("根据id获取用户登录日志{}异常，参数:{}", "getById", id);
		return FansResp.error();
	}

	@Override
	public FansResp list(SysLoginLog sysLoginLog) {
		logger.error("获取用户登录日志列表{}异常，参数:{}", "list", JSON.toJSONString(sysLoginLog));
		return FansResp.error();
	}

	@Override
	public FansResp delete(String ids) {
		logger.error("根据ids删除用户登录日志{}异常，参数:{}", "delete", ids);
		return FansResp.error();
	}

	@Override
	public FansResp deleteAll() {
		logger.error("清除所有用户登录日志{}异常", "delete");
		return FansResp.error();
	}

}
