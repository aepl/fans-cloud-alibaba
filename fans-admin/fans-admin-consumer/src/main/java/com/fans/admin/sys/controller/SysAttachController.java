package com.fans.admin.sys.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fans.admin.sys.entity.SysAttach;
import com.fans.admin.sys.feign.SysAttachService;
import com.fans.common.base.BaseController;
import com.fans.common.utils.FansResp;

/**
 * 
 * @ClassName: SysAttachController
 * @Description: 系统_附件数据接口
 * @author fanhaohao
 * @date 2020年1月16日 下午5:29:48
 */
@RestController
@RequestMapping("/sys/sysAttach")
public class SysAttachController extends BaseController {

    @Autowired
	private SysAttachService sysAttachService;
    
    /**
     * 
     * @Title：getById  
     * @Description: 根据id获取系统_附件
     * @author: fanhaohao
     * @date 2020年1月16日 下午5:27:31 
     * @param @param id
     * @param @return 
     * @return FansResp 
     * @throws
     */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public FansResp getById(String id) {
		return sysAttachService.getById(id);
	}
	
	/**
	 * 
	 * @Title：list  
	 * @Description: 获取系统_附件列表
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:27:47 
	 * @param @param sysAttach
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public FansResp list(SysAttach sysAttach) {
		return sysAttachService.list(sysAttach);
	}
	
	/**
	 * 
	 * @Title：delete  
	 * @Description: 删除系统_附件
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:22:39 
	 * @param @param ids
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public FansResp delete(@PathVariable(value = "ids") String ids){
		return sysAttachService.delete(ids);
	}
	

}
