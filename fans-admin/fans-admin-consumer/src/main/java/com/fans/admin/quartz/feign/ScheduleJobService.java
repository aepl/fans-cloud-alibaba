package com.fans.admin.quartz.feign;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fans.admin.quartz.entity.ScheduleJob;
import com.fans.admin.quartz.feign.fallback.ScheduleJobServiceFallbackImpl;
import com.fans.common.utils.FansResp;

/**
 * @ClassName: ScheduleJobService
 * @Description:定时任务数据
 * @author fanhaohao
 * @date 2020年1月8日 上午11:18:00
 */
@FeignClient(name = "fans-admin-provider", fallback = ScheduleJobServiceFallbackImpl.class)
public interface ScheduleJobService {

	/**
	 * 
	 * @Title：getById  
	 * @Description: 根据id获取定时任务数据
	 * @author: fanhaohao
	 * @date 2020年1月8日 下午3:24:08 
	 * @param @param id
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@RequestMapping(value = { "/quartz/scheduleJob/getById" }, method = RequestMethod.GET)
	public FansResp getById(@RequestParam(value = "id") String id);
	
	/**
	 * 
	 * @Title：list  
	 * @Description: 分页获取定时任务数据
	 * @author: fanhaohao
	 * @date 2020年1月15日 下午3:59:21 
	 * @param @param scheduleJob
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = { "/quartz/scheduleJob/list" }, method = RequestMethod.GET)
	public FansResp list(@SpringQueryMap ScheduleJob scheduleJob);
	
	/**
	 * 
	 * @Title：create  
	 * @Description: 添加定时任务
	 * @author: fanhaohao
	 * @date 2020年1月15日 下午4:27:17 
	 * @param @param scheduleJob
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/quartz/scheduleJob/create", method = RequestMethod.POST)
	public FansResp create(@RequestParam("scheduleJob") Map<String, Object> scheduleJob);
	
	/**
	 * 
	 * @Title：update  
	 * @Description: 修改定时任务
	 * @author: fanhaohao
	 * @date 2020年1月15日 下午4:28:53 
	 * @param @param scheduleJob
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/quartz/scheduleJob/update", method = RequestMethod.PUT)
	public FansResp update(@RequestParam("scheduleJob") Map<String, Object> scheduleJob);
	
	/**
	 * 
	 * @Title：delete  
	 * @Description: 定时任务删除
	 * @author: fanhaohao
	 * @date 2020年1月16日 上午9:45:05 
	 * @param @param ids
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/quartz/scheduleJob/delete/{ids}", method = RequestMethod.DELETE)
	public FansResp delete(@PathVariable(value = "ids") String ids);

	/**
	 * 
	 * @Title：run  
	 * @Description: 执行定时任务
	 * @author: fanhaohao
	 * @date 2020年1月17日 下午1:53:07 
	 * @param @param scheduleJob
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/quartz/scheduleJob/run", method = RequestMethod.POST)
	public FansResp run(@RequestParam("scheduleJob") Map<String, Object> entityMap);

	/**
	 * 
	 * @Title：pause  
	 * @Description: 暂停定时任务
	 * @author: fanhaohao
	 * @date 2020年1月17日 下午2:03:48 
	 * @param @param entityMap
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/quartz/scheduleJob/pause", method = RequestMethod.POST)
	public FansResp pause(@RequestParam("scheduleJob") Map<String, Object> entityMap);
	
	/**
	 * 
	 * @Title：resume  
	 * @Description: 恢复定时任务
	 * @author: fanhaohao
	 * @date 2020年1月17日 下午2:03:53 
	 * @param @param entityMap
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/quartz/scheduleJob/resume", method = RequestMethod.POST)
	public FansResp resume(@RequestParam("scheduleJob") Map<String, Object> entityMap) ;
}
