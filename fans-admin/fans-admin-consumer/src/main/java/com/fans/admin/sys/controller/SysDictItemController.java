package com.fans.admin.sys.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fans.admin.sys.entity.SysDictItem;
import com.fans.admin.sys.feign.SysDictItemService;
import com.fans.common.base.BaseController;
import com.fans.common.utils.FansResp;

/**
 * 
 * @ClassName: SysDictItemController
 * @Description: 基础字典项数据接口
 * @author fanhaohao
 * @date 2020年1月16日 下午5:30:15
 */
@RestController
@RequestMapping("/sys/sysDictItem")
public class SysDictItemController extends BaseController {

    @Autowired
	private SysDictItemService sysDictItemService;
    
    /**
     * 
     * @Title：getById  
     * @Description: 根据id获取基础字典项
     * @author: fanhaohao
     * @date 2020年1月16日 下午5:28:13 
     * @param @param id
     * @param @return 
     * @return FansResp 
     * @throws
     */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public FansResp getById(String id) {
		return sysDictItemService.getById(id);
	}
	
	/**
	 * 
	 * @Title：list  
	 * @Description: 获取基础字典项列表
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:27:47 
	 * @param @param sysDictItem
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public FansResp list(SysDictItem sysDictItem) {
		return sysDictItemService.list(sysDictItem);
	}
	
	/**
	 * 
	 * @Title：create  
	 * @Description: 添加基础字典项
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:21:26 
	 * @param @param sysDictItem
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public FansResp create(@RequestBody Map<String, Object> sysDictItem){
		return sysDictItemService.create(sysDictItem);
	}

	/**
	 * 
	 * @Title：update  
	 * @Description: 修改基础字典项
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:22:46 
	 * @param @param sysDictItem
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public FansResp update(@RequestBody Map<String, Object> sysDictItem){
		return sysDictItemService.update(sysDictItem);
	}
	
	/**
	 * 
	 * @Title：delete  
	 * @Description: 删除基础字典项
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:22:39 
	 * @param @param ids
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public FansResp delete(@PathVariable(value = "ids") String ids){
		return sysDictItemService.delete(ids);
	}
	

}
