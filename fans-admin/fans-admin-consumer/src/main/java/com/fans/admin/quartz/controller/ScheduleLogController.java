package com.fans.admin.quartz.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fans.admin.quartz.entity.ScheduleLog;
import com.fans.admin.quartz.feign.ScheduleLogService;
import com.fans.common.base.BaseController;
import com.fans.common.utils.FansResp;

/**
 * 
 * @ClassName: ScheduleLogController
 * @Description: 定时任务执行日志数据接口
 * @author fanhaohao
 * @date 2020年1月16日 下午5:29:48
 */
@RestController
@RequestMapping("/quartz/scheduleLog")
public class ScheduleLogController extends BaseController {

    @Autowired
	private ScheduleLogService scheduleLogService;
    
    /**
     * 
     * @Title：getById  
     * @Description: 根据id获取定时任务执行日志
     * @author: fanhaohao
     * @date 2020年1月16日 下午5:27:31 
     * @param @param id
     * @param @return 
     * @return FansResp 
     * @throws
     */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public FansResp getById(String id) {
		return scheduleLogService.getById(id);
	}
	
	/**
	 * 
	 * @Title：list  
	 * @Description: 获取定时任务执行日志列表
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:27:47 
	 * @param @param scheduleLog
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public FansResp list(ScheduleLog scheduleLog) {
		return scheduleLogService.list(scheduleLog);
	}
	
	/**
	 * 
	 * @Title：delete  
	 * @Description: 删除定时任务执行日志
	 * @author: fanhaohao
	 * @date 2020年1月16日 下午4:22:39 
	 * @param @param ids
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public FansResp delete(@PathVariable(value = "ids") String ids){
		return scheduleLogService.delete(ids);
	}
	
	/**
	 * 
	 * @Title：deleteAll  
	 * @Description: 清除所有定时任务执行日志
	 * @author: fanhaohao
	 * @date 2020年1月17日 上午10:09:19 
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	@RequestMapping(value = "/deleteAll", method = RequestMethod.DELETE)
	public FansResp deleteAll(){
		return scheduleLogService.deleteAll();
	}

}
