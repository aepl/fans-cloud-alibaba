package com.fans.admin.sys.feign.fallback;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.fans.admin.sys.feign.SysRoleMenuService;
import com.fans.common.utils.FansResp;

/**
 * @ClassName: SysRoleMenuGssServiceFallbackImpl
 * @Description: 系统_角色资源数据服务的fallback
 * @author fanhaohao
 * @date 2020年1月30日 上午11:18:18
 */
@Service
public class SysRoleMenuServiceFallbackImpl implements SysRoleMenuService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public FansResp getRoleMenu(String roleId) {
		logger.error("根据根据角色id获得所有资源菜单与角色所拥有的资源列表{}异常参数:{}", "getRoleMenu", roleId);
		return FansResp.error();
	}

	@Override
	public FansResp saveRoleMenu(Map<String, Object> entityMap) {
		logger.error("保存角色资源菜单{}异常参数:{}", "saveRoleMenu", JSON.toJSONString(entityMap));
		return FansResp.error();
	}

}
