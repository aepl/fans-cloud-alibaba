package com.fans.admin.quartz.feign.fallback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fans.admin.quartz.entity.ScheduleLog;
import com.fans.admin.quartz.feign.ScheduleLogService;
import com.fans.common.utils.FansResp;
import com.fans.common.utils.JSON;

/**
 * @ClassName: ScheduleLogServiceFallbackImpl
 * @Description: 定时任务执行日志数据服务的fallback
 * @author fanhaohao
 * @date 2020年1月30日 上午11:18:18
 */
@Service
public class ScheduleLogServiceFallbackImpl implements ScheduleLogService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public FansResp getById(String id) {
		logger.error("根据id获取定时任务执行日志{}异常，参数:{}", "getById", id);
		return FansResp.error();
	}

	@Override
	public FansResp list(ScheduleLog scheduleLog) {
		logger.error("获取定时任务执行日志列表{}异常，参数:{}", "list", JSON.toJSONString(scheduleLog));
		return FansResp.error();
	}

	@Override
	public FansResp delete(String ids) {
		logger.error("根据ids删除定时任务执行日志{}异常，参数:{}", "delete", ids);
		return FansResp.error();
	}

	@Override
	public FansResp deleteAll() {
		logger.error("清除所有定时任务执行日志{}异常", "delete");
		return FansResp.error();
	}

}
