package com.fans.admin.sys.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fans.common.base.BaseEntity;
import com.fans.common.utils.BlankUtils;

/**
 * @description: 用户登录日志
 * @date: 2020年01月11 10:01:55
 * @author: fanhaohao
 * @version: 1.0
 */
@TableName(value="sys_login_log",resultMap="BaseResultMap")
public class SysLoginLog extends BaseEntity<SysLoginLog> {

    private static final long serialVersionUID = 1L;

    /**
     * 操作类型（1,登录;2,登出）
     */
    @TableField("oper_type")
    private String operType;
    /**
     * 登录结果类型(1,成功；2失败)
     */
    @TableField("result_type")
    private String resultType;
    /**
     * 操作IP地址
     */
    @TableField("remote_addr")
    private String remoteAddr;
    /**
     * 用户代理
     */
    @TableField("user_agent")
    private String userAgent;
    /**
     * 操作时间（登录/退出的时间）
     */
    @TableField("oper_time")
    private Date operTime;
    /**
     * 地点
     */
    private String address;
    /**
     * 浏览器
     */
    @TableField("browser_type")
    private String browserType;
    /**
     * 登录类型
     */
    @TableField("login_type")
    private String loginType;
    /**
     * 设备
     */
    private String equipment;
    /**
     * 登录帐号
     */
    @TableField("logon_id")
    private String logonId;

	/**
	 * 操作时间
	 */
	@TableField(exist = false)
	private String opDate;

	/**
	 * 操作开始时间
	 */
	@TableField(exist = false)
	private String opStartDate;

	/**
	 * 操作结束时间
	 */
	@TableField(exist = false)
	private String opEndDate;

	public String getOperType() {
		return operType;
	}

	public void setOperType(String operType) {
		this.operType = operType;
	}

	public String getResultType() {
		return resultType;
	}

	public void setResultType(String resultType) {
		this.resultType = resultType;
	}

	public String getRemoteAddr() {
		return remoteAddr;
	}

	public void setRemoteAddr(String remoteAddr) {
		this.remoteAddr = remoteAddr;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public Date getOperTime() {
		return operTime;
	}

	public void setOperTime(Date operTime) {
		this.operTime = operTime;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBrowserType() {
		return browserType;
	}

	public void setBrowserType(String browserType) {
		this.browserType = browserType;
	}

	public String getLoginType() {
		return loginType;
	}

	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}

	public String getEquipment() {
		return equipment;
	}

	public void setEquipment(String equipment) {
		this.equipment = equipment;
	}

	public String getLogonId() {
		return logonId;
	}

	public void setLogonId(String logonId) {
		this.logonId = logonId;
	}

	public String getOpDate() {
		return opDate;
	}

	public void setOpDate(String opDate) {
		this.opDate = opDate;
		if (BlankUtils.isNotBlank(this.opDate)) {
			String[] opDateArr = opDate.split(",");
			if (BlankUtils.isNotBlank(opDateArr)) {
				this.setOpStartDate(opDateArr[0] + " 00:00:00");
				if (opDateArr.length == 2) {
					this.setOpEndDate(opDateArr[1] + " 23:59:59");
				}
			}
		}

	}

	public String getOpStartDate() {
		return opStartDate;
	}

	public void setOpStartDate(String opStartDate) {
		this.opStartDate = opStartDate;
	}

	public String getOpEndDate() {
		return opEndDate;
	}

	public void setOpEndDate(String opEndDate) {
		this.opEndDate = opEndDate;
	}

}
