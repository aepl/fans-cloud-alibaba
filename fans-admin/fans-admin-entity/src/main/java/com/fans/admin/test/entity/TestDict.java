package com.fans.admin.test.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fans.common.base.BaseEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @description: 测试实体类
 * @date: 2020-04-15
 * @author: fanhaohao
 * @version: 1.0
 * @copyright: 版权所有 fans (c)2020 
 */ 
@ApiModel(value = "测试")
@TableName(value="fans_test_dict",resultMap="TestDictResultMap")
public class TestDict extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
	@TableField("code")
	@ApiModelProperty("编号")
    private String code;
    /**
     * 名称
     */
	@TableField("name")
	@ApiModelProperty("名称")
    private String name;
    /**
     * 父节点
     */
	@TableField("parent_id")
	@ApiModelProperty("父节点")
    private Long parentId;
    /**
     * 排序
     */
	@TableField("weight")
	@ApiModelProperty("排序")
    private Integer weight;



	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}


}
