package com.fans.admin.sys.vo;
import java.util.HashSet;
import java.util.List;

/**
 * @ClassName: RoleMenuPerms
 * @Description: 角色菜单权限
 * @author fanhaohao
 * @date 2019年12月15日 下午5:32:07
 */
public class RoleMenuPerms {
	private HashSet<String> permsList;
    private List<Menu> menuList;

    public HashSet<String> getPermsList() {
        return permsList;
    }

    public void setPermsList(HashSet<String> permsList) {
        this.permsList = permsList;
    }

    public List<Menu> getMenuList() {
        return menuList;
    }

    public void setMenuList(List<Menu> menuList) {
        this.menuList = menuList;
    }

}
