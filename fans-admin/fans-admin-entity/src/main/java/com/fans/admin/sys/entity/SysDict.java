package com.fans.admin.sys.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fans.common.base.BaseEntity;

/**
 * @description: 基础字典类型
 * @date: 2019年12月30 18:18:58
 * @author: fanhaohao
 * @version: 1.0
 */
@TableName(value="sys_dict",resultMap="BaseResultMap")
public class SysDict extends BaseEntity<SysDict> {

    private static final long serialVersionUID = 1L;

    /**
     * 数据字典编码
     */
    @TableField("dict_code")
    private String dictCode;
    /**
     * 数据字典名称
     */
    @TableField("dict_name")
    private String dictName;
    /**
     * 数据字典显示顺序
     */
    @TableField("show_order")
    private BigDecimal showOrder;

	public String getDictCode() {
		return dictCode;
	}

	public void setDictCode(String dictCode) {
		this.dictCode = dictCode;
	}

	public String getDictName() {
		return dictName;
	}

	public void setDictName(String dictName) {
		this.dictName = dictName;
	}

	public BigDecimal getShowOrder() {
		return showOrder;
	}

	public void setShowOrder(BigDecimal showOrder) {
		this.showOrder = showOrder;
	}


}
