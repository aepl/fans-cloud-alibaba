package com.fans.admin.sys.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fans.common.base.BaseEntity;

/**
 * @description: 系统_角色
 * @date: 2019年12月20 14:32:42
 * @author: fanhaohao
 * @version: 1.0
 */
@TableName(value="sys_role",resultMap="BaseResultMap")
public class SysRole extends BaseEntity<SysRole> {

    private static final long serialVersionUID = 1L;

    /**
     * 角色名称
     */
    private String name;
    /**
     * 角色标识
     */
    private String role;
    /**
     * 角色描述
     */
    private String description;
    /**
     * 角色状态
     */
    private String status;
    /**
     * 是否内置
     */
    @TableField("is_sys")
    private String isSys;
    /**
     * 角色主页面
     */
    private String main;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIsSys() {
		return isSys;
	}

	public void setIsSys(String isSys) {
		this.isSys = isSys;
	}

	public String getMain() {
		return main;
	}

	public void setMain(String main) {
		this.main = main;
	}


}
