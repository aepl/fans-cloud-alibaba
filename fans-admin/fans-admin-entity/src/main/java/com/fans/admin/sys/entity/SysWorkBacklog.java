package com.fans.admin.sys.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fans.common.base.BaseEntity;

/**
 * @description: 待办事项
 * @date: 2019年12月26 14:26:39
 * @author: fanhaohao
 * @version: 1.0
 */
@TableName(value="sys_work_backlog",resultMap="BaseResultMap")
public class SysWorkBacklog extends BaseEntity<SysWorkBacklog> {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    private String name;
    /**
     * 内容
     */
    private String content;
    /**
     * 图片
     */
    private String pictures;
    /**
     * 待办开始时间
     */
    @TableField("start_time")
    private String startTime;
    /**
     * 待办完成时间
     */
    @TableField("finish_time")
    private String finishTime;
    /**
     * 来源模块
     */
    @TableField("from_module")
    private Integer fromModule;
    /**
     * 来源id
     */
    @TableField("from_id")
    private String fromId;
    /**
     * 优先级（0=一般1=重要）
     */
    private String priority;
    /**
     * 待办用户
     */
    @TableField("user_ids")
    private String userIds;
    /**
     * 状态(0=未完成1=已完成)
     */
    private String status;
    /**
     * 提前多少天提醒
     */
    @TableField("inform_days")
    private Integer informDays;
    /**
     * 开启提醒
     */
    @TableField("inform_enable")
    private String informEnable;
    /**
     * 通知渠道
     */
    @TableField("inform_type")
    private String informType;
    /**
     * 通知状态（0=未通知1=已通知2=已提前通知）
     */
    @TableField("inform_status")
    private String informStatus;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getPictures() {
		return pictures;
	}

	public void setPictures(String pictures) {
		this.pictures = pictures;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getFinishTime() {
		return finishTime;
	}

	public void setFinishTime(String finishTime) {
		this.finishTime = finishTime;
	}

	public Integer getFromModule() {
		return fromModule;
	}

	public void setFromModule(Integer fromModule) {
		this.fromModule = fromModule;
	}

	public String getFromId() {
		return fromId;
	}

	public void setFromId(String fromId) {
		this.fromId = fromId;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getUserIds() {
		return userIds;
	}

	public void setUserIds(String userIds) {
		this.userIds = userIds;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getInformDays() {
		return informDays;
	}

	public void setInformDays(Integer informDays) {
		this.informDays = informDays;
	}

	public String getInformEnable() {
		return informEnable;
	}

	public void setInformEnable(String informEnable) {
		this.informEnable = informEnable;
	}

	public String getInformType() {
		return informType;
	}

	public void setInformType(String informType) {
		this.informType = informType;
	}

	public String getInformStatus() {
		return informStatus;
	}

	public void setInformStatus(String informStatus) {
		this.informStatus = informStatus;
	}


}
