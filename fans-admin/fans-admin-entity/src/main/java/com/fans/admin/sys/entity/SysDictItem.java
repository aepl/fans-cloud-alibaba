package com.fans.admin.sys.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fans.common.base.BaseEntity;

/**
 * @description: 字典项
 * @date: 2019年12月30 18:18:58
 * @author: fanhaohao
 * @version: 1.0
 */
@TableName(value="sys_dict_item",resultMap="BaseResultMap")
public class SysDictItem extends BaseEntity<SysDictItem> {

    private static final long serialVersionUID = 1L;

    /**
     * 数据字典ID
     */
    @TableField("dict_id")
    private String dictId;
    /**
     * 数据字典项编码
     */
    @TableField("item_code")
    private String itemCode;
    /**
     * 数据字典项值
     */
    @TableField("item_value")
    private String itemValue;
    /**
     * 字典项显示顺序
     */
    @TableField("show_order")
    private BigDecimal showOrder;

	public String getDictId() {
		return dictId;
	}

	public void setDictId(String dictId) {
		this.dictId = dictId;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemValue() {
		return itemValue;
	}

	public void setItemValue(String itemValue) {
		this.itemValue = itemValue;
	}

	public BigDecimal getShowOrder() {
		return showOrder;
	}

	public void setShowOrder(BigDecimal showOrder) {
		this.showOrder = showOrder;
	}


}
