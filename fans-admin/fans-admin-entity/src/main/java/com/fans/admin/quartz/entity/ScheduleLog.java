package com.fans.admin.quartz.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fans.common.base.BaseEntity;

/**
 * @description: 定时任务执行日志表
 * @date: 2020年01月14 16:55:11
 * @author: fanhaohao
 * @version: 1.0
 */
@TableName(value="quartz_schedule_log",resultMap="BaseResultMap")
public class ScheduleLog extends BaseEntity<ScheduleLog> {

    private static final long serialVersionUID = 1L;

    /**
     * 任务表ID
     */
    @TableField("schedule_job_id")
    private String scheduleJobId;
    /**
     * 执行时间（创建时间）
     */
    @TableField("exec_time")
    private Date execTime;
    /**
     * 执行结束时间
     */
    @TableField("end_time")
    private Date endTime;
    /**
     * 状态（SUCCESS、FAIL等）
     */
    private String status;
    /**
     * 异常文本
     */
    @TableField("exception_stack_trace")
    private String exceptionStackTrace;

	public String getScheduleJobId() {
		return scheduleJobId;
	}

	public void setScheduleJobId(String scheduleJobId) {
		this.scheduleJobId = scheduleJobId;
	}

	public Date getExecTime() {
		return execTime;
	}

	public void setExecTime(Date execTime) {
		this.execTime = execTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getExceptionStackTrace() {
		return exceptionStackTrace;
	}

	public void setExceptionStackTrace(String exceptionStackTrace) {
		this.exceptionStackTrace = exceptionStackTrace;
	}


}
