package com.fans.admin.quartz.constant;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName: Constant
 * @Description: 常量
 * @author fanhaohao
 * @date 2020年1月16日 下午3:25:09
 */
public class QtzConstant {
    
	/**
	 * @ClassName: ScheduleStatus
	 * @Description: 定时任务状态
	 * @author fanhaohao
	 * @date 2020年1月16日 下午3:25:53
	 */
    public enum ScheduleStatus {
        /**
         * 正常
         */
		NORMAL("0"),
        /**
         * 暂停
         */
		PAUSE("1");

		private String value;

		private ScheduleStatus(String value) {
            this.value = value;
        }
        
		public String getValue() {
            return value;
        }
    }

	/**
	 * @ClassName: ScheduleLogStatusEnum
	 * @Description: 日志状态 status（对应字典 EXECUTION_STATE 1，成功；2，失败）
	 * @author fanhaohao
	 * @date 2020年1月16日 下午5:28:12
	 */
	public enum ScheduleLogStatusEnum {
		/**
		 * 成功
		 */
		SUCCESS("1", "成功"),

		/**
		 * 失败
		 */
		FAIL("2", "失败");

		private final String code;
		private final String label;

		private ScheduleLogStatusEnum(final String code, final String label) {
			this.code = code;
			this.label = label;
		}

		/**
		 * 获取 code
		 * 
		 * @return code code
		 */
		public String getCode() {
			return code;
		}

		/**
		 * 获取 label
		 * 
		 * @return label label
		 */
		public String getLabel() {
			return label;
		}

		/**
		 * 将枚举转换为Map
		 * 
		 * @return
		 */
		public static Map<String, String> getMap() {
			Map<String, String> map = new HashMap<String, String>();
			ScheduleLogStatusEnum[] enums = ScheduleLogStatusEnum.values();
			for (ScheduleLogStatusEnum e : enums) {
				map.put(e.toString(), e.getLabel());
			}
			return map;
		}
	}
	
	
	/**
	 * @ClassName: JobStatusEnum
	 * @Description: 任务执行状态 （对应字典 TASK_EXECUTION_STATE ）
	 * @author fanhaohao
	 * @date 2020年1月16日 下午5:32:15
	 * 
	 *       None：Trigger已经完成，且不会在执行，或者找不到该触发器，或者Trigger已经被删除 NORMAL:正常状态
	 *       PAUSED：暂停状态 COMPLETE：触发器完成，但是任务可能还正在执行中 BLOCKED：线程阻塞状态 ERROR：出现错误
	 */
	public enum JobStatusEnum {
		/**
		 * 正常
		 */
		NORMAL("1","正常"),
		/**
		 * 未启动
		 */
		NOSTART("2","未启动"),
		/**
		 * 阻塞
		 */
		BLOCKED("3","阻塞"),
		/**
		 * 暂停
		 */
		PAUSED("4","暂停"),
		/**
		 * 触发器完成
		 */
		COMPLETE("5","触发器完成"),
		/**
		 * 错误
		 */
		ERROR("6","错误"),
		/**
		 * 已经完成
		 */
		None("7","已经完成"),
		/**
		 * 正常执行 
		 */
		ACQUIRED("7","正常执行");
		
		private final String code;
		private final String label;
		
		private JobStatusEnum(final String code,final String label) {
			this.code = code;
			this.label = label;
		}
		
		/**
		 * 获取 code
		 * @return code code
		 */
		public String getCode() {
			return code;
		}

		/**
		 * 获取 label
		 * @return label label
		 */
		public String getLabel() {
			return label;
		}
		
	}

	
	/**
	 * @ClassName: ScheduleJobStatusEnum
	 * @Description: 任务执行状态 status（对应字典 TASK_EXECUTION_STATE
	 *               1，正常；2，未启动；3，阻塞；4，异常）
	 * @author fanhaohao
	 * @date 2020年1月16日 下午5:35:06
	 */
	public enum ScheduleJobStatusEnum {
		PAUSED("5","暂停"),
		/**
		 * 异常
		 */
		EXCEPTION("4","异常"),
		/**
		 * 等待执行
		 */
		WAITTING("3","阻塞"),
		/**
		 * 未启动
		 */
		NOSTART("2","未启动"),
		/**
		 * 正常
		 */
		RUNNING("1","正常");
		
		private final String code;
		private final String label;
		
		private ScheduleJobStatusEnum(final String code,final String label) {
			this.code = code;
			this.label = label;
		}

		/**
		 * 获取 code
		 * @return code code
		 */
		public String getCode() {
			return code;
		}
		
		/**
		 * 获取 label
		 * @return label label
		 */
		public String getLabel() {
			return label;
		}
	}

	/**
	 * 
	 * @ClassName: ScheduleStatusEnum
	 * @Description: 任务状态0：禁用；1：启用；2：删除 数据库对应字段 job_status 字典 QUARTZ_TASK_STATE
	 * @author fanhaohao
	 * @date 2020年1月16日 下午5:36:53
	 */
	public enum ScheduleStatusEnum {
		/**
		 * 启用
		 */
		USABLE("1", "启用"),

		/**
		 * 禁用
		 */
		DISABLE("0", "禁用"),
		/**
		 * 删除
		 */
		DELETE("2", "删除");

		private final String code;
		private final String label;

		private ScheduleStatusEnum(final String code, final String label) {
			this.code = code;
			this.label = label;
		}

		/**
		 * 获取 code
		 * 
		 * @return code code
		 */
		public String getCode() {
			return code;
		}
		
		/**
		 * 获取 label
		 * 
		 * @return label label
		 */
		public String getLabel() {
			return label;
		}

		/**
		 * 将枚举转换为Map
		 * 
		 * @return
		 */
		public static Map<String, String> getMap() {
			Map<String, String> map = new HashMap<String, String>();
			ScheduleStatusEnum[] enums = ScheduleStatusEnum.values();
			for (ScheduleStatusEnum e : enums) {
				map.put(e.toString(), e.getLabel());
			}
			return map;
		}
	}
}
