package com.fans.admin.sys.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fans.common.base.BaseEntity;
import com.fans.common.utils.BlankUtils;

/**
 * @description:
 * @date: 2019年12月28 09:27:28
 * @author: fanhaohao
 * @version: 1.0
 */
@TableName(value="sys_log",resultMap="BaseResultMap")
public class SysLog extends BaseEntity<SysLog> {

    private static final long serialVersionUID = 1L;

    /**
     * 用户操作
     */
    private String operation;
    /**
     * 日志内容
     */
    private String content;
	/**
	 * 日志类型（1：正常日志；2：错误日志）
	 */
	private String type;
	/**
	 * 操作的方式
	 */
	private String method;
	/**
	 * 操作者IP
	 */
	private String ip;

	/**
	 * 创建人名称
	 */
	@TableField(exist = false)
	private String creatorName;
	
	/**
	 * 操作时间
	 */
	@TableField(exist = false)
	private String opDate;
	
	/**
	 * 操作开始时间
	 */
	@TableField(exist = false)
	private String opStartDate;
	
	/**
	 * 操作结束时间
	 */
	@TableField(exist = false)
	private String opEndDate;
	

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCreatorName() {
		return creatorName;
	}

	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}

	public String getOpDate() {
		return opDate;
	}

	public void setOpDate(String opDate) {
		this.opDate = opDate;
		if(BlankUtils.isNotBlank(this.opDate)){
			String[] opDateArr = opDate.split(",");
			if (BlankUtils.isNotBlank(opDateArr)) {
				this.setOpStartDate(opDateArr[0] + " 00:00:00");
				if (opDateArr.length == 2) {
					this.setOpEndDate(opDateArr[1] + " 23:59:59");
				}
			}
		}
		
	}

	public String getOpStartDate() {
		return opStartDate;
	}

	public void setOpStartDate(String opStartDate) {
		this.opStartDate = opStartDate;
	}

	public String getOpEndDate() {
		return opEndDate;
	}

	public void setOpEndDate(String opEndDate) {
		this.opEndDate = opEndDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}
	
	

}
