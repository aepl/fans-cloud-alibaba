package com.fans.admin.sys.service.impl;

import java.net.SocketException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fans.admin.sys.dao.SysLoginLogMapper;
import com.fans.admin.sys.entity.SysLoginLog;
import com.fans.admin.sys.service.ISysLoginLogService;
import com.fans.common.utils.BlankUtils;
import com.fans.common.utils.BrowserUtils;
import com.fans.common.utils.DeviceUtils;
import com.fans.common.utils.StringUtils;
import com.fans.common.utils.jwt.TokenUtil;

/**
 * @description: 用户登录日志服务实现类
 * @date: 2020年01月11 10:01:55
 * @author: fanhaohao
 * @version: 1.0
 */
@Service
public class SysLoginLogServiceImpl extends ServiceImpl<SysLoginLogMapper,SysLoginLog> implements ISysLoginLogService {

	@Autowired
    private SysLoginLogMapper sysLoginLogMapper;
    
	/**
	 * @Title：list  
	 * @Description: 用户登录日志分页查询
	 * @author: fanhaohao
	 * @date 2020年01月11 10:01:55
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	@Override
	public List<Map<String, Object>> list(Page<SysLoginLog> page,Map<String, Object> map){
		return sysLoginLogMapper.list(page,map);
	}
	
	/**
	 * @Title：deleteBatchByIdLogic  
	 * @Description: 用户登录日志根据id逻辑删除
	 * @author: fanhaohao
	 * @date 2020年01月11 10:01:55
	 * @param @param id 
	 * @return void 
	 * @throws
	 */
	@Override
	public void deleteBatchByIdLogic(Object id){
		SysLoginLog sysLoginLog = new SysLoginLog();
		sysLoginLog.setId(String.valueOf(id));
		sysLoginLog.preUpdate();
		sysLoginLog.setUpdateBy(TokenUtil.getUserId());
		sysLoginLog.setDelFlag("1");
		this.updateById(sysLoginLog);
	}
	
	/**
	 * @Title：deleteBatchByIdsLogic    
	 * @Description: 用户登录日志逻辑批量删除
	 * @author: fanhaohao
	 * @date 2020年01月11 10:01:55
	 * @param @param ids 
	 * @return void 
	 * @throws
	 */
	@Override
	public void deleteBatchByIdsLogic(List<Object> ids) {
		List<SysLoginLog> list = new ArrayList<>();
		for (Object id : ids) {
			SysLoginLog sysLoginLog = new SysLoginLog();
			sysLoginLog.setId(String.valueOf(id));
			sysLoginLog.preUpdate();
			sysLoginLog.setUpdateBy(TokenUtil.getUserId());
			sysLoginLog.setDelFlag("1");
			list.add(sysLoginLog);
		}
		this.updateBatchById(list);
	}

	/**
	 * @Title：saveLoginLog  
	 * @Description:用户登录日志保存
	 * @author: fanhaohao
	 * @date 2020年1月11日 上午9:52:24 
	 * @param @param request
	 * @param @param logonId
	 * @param @param operType
	 * @param @param resultType
	 * @param @param msg 
	 * @return void 
	 * @throws
	 */
	@Override
	public void saveLoginLog(HttpServletRequest request,String logonId, String operType, String resultType, String msg) {
		if (BlankUtils.isNotBlank(logonId)) {
			SysLoginLog sysLoginLog = new SysLoginLog();
			sysLoginLog.setOperType(operType);
			sysLoginLog.setResultType(resultType);
			sysLoginLog.setRemarks(msg);
			// 获取浏览器类型
			sysLoginLog.setBrowserType(BrowserUtils.checkBrowse(request));
			sysLoginLog.setOperTime(new Date());
			// 获取设备类型以及设备
			if (DeviceUtils.isMobileDevice(request)) {
				sysLoginLog.setLoginType("手机登录");
				sysLoginLog.setEquipment("手机");
			} else {
				sysLoginLog.setLoginType("电脑登录");
				sysLoginLog.setEquipment("电脑");
			}
			// 获取ip地址
			String ip = StringUtils.getRemoteAddr(request);
			if ("0:0:0:0:0:0:0:1".equals(ip)) {
				try {
					ip = StringUtils.getRealIp();
				} catch (SocketException e) {
					e.printStackTrace();
				}
			}
			sysLoginLog.setRemoteAddr(ip);
			// 获取用户代理
			sysLoginLog.setUserAgent(BrowserUtils.checkBrowse(request));
			sysLoginLog.preInsert();
			sysLoginLog.setLogonId(logonId);
			super.insert(sysLoginLog);
		}
	}
	
	/**
	 * @Title：getAllIds  
	 * @Description: 获取所有的日志id
	 * @author: fanhaohao
	 * @date 2020年1月3日 下午4:42:24 
	 * @param @return 
	 * @return List<String> 
	 * @throws
	 */
	@Override
	public List<String> getAllIds() {
		return sysLoginLogMapper.getAllIds();
	}
}
