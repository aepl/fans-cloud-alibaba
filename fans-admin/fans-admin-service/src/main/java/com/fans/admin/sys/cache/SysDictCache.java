package com.fans.admin.sys.cache;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fans.admin.sys.service.ISysDictService;
import com.fans.common.utils.BlankUtils;
import com.fans.common.utils.SpringUtil;

/**
 * @ClassName: SysDictCache  
 * @Description: 
 * @author fanhaohao
 * @date 2019年1月8日 上午11:00:03
 */
/**
 * 
 * @ClassName: SysDictCache
 * @Description: 字典缓存类
 * @author fanhaohao
 * @date 2020年1月17日 上午9:17:29
 */
public class SysDictCache {
	
	/**
	 * @Title：getDictList  
	 * @Description: 根据code获取字典缓存list
	 * @author: fanhaohao
	 * @date 2019年1月8日 上午11:01:44 
	 * @param @param code
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public static List<Map<String, Object>> getDictList(String code){
		ISysDictService sysDictService = SpringUtil.getBean(ISysDictService.class);
		return sysDictService.selectDictListBycode(code);
	}
	
	/**
	 * @Title：getDictMap  
	 * @Description: 根据code获取字典缓存Map
	 * @author: fanhaohao
	 * @date 2019年1月8日 上午11:03:20 
	 * @param @param code
	 * @param @return 
	 * @return Map<String,Object> 
	 * @throws
	 */
	public static Map<String, Object> getDictMap(String code){
		Map<String, Object> rt = new HashMap<String, Object>();
		List<Map<String, Object>> dictList = getDictList(code);
		if (BlankUtils.isNotBlank(dictList)) {
			for (Map<String, Object> dl : dictList) {
				String itemCode = String.valueOf(dl.get("itemCode"));
				String itemValue = String.valueOf(dl.get("itemValue"));
				rt.put(itemCode, itemValue);
			}
		}
		return rt;
	}
	
	/**
	 * 
	 * @Title：getDictMapList
	 * @Description: 根据code List获取字典缓存Maps
	 * @author: fanhaohao
	 * @date 2020年1月15日 上午10:26:50 
	 * @param @param codes 逗号分隔（多个code拼接）
	 * @param @return 
	 * @return Map<String,Object> 
	 * @throws
	 */
	public static Map<String, List<Map<String, Object>>> getDictMapList(String codes) {
		Map<String, List<Map<String, Object>>> rt = new HashMap<String, List<Map<String, Object>>>();
		if(BlankUtils.isNotBlank(codes)){
			String[] codesArr = codes.split(",");
			for(String code:codesArr){
				List<Map<String, Object>> dictList = getDictList(code);
				rt.put(code , dictList);
			}
		}
		return rt;
	}
}
