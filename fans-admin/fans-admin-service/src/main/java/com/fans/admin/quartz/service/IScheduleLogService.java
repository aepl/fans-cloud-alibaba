package com.fans.admin.quartz.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.fans.admin.quartz.entity.ScheduleLog;
 
/**
 * @description: 定时任务执行日志表服务类
 * @date: 2020年01月14 16:55:11
 * @author: fanhaohao
 * @version: 1.0
 */
public interface IScheduleLogService extends IService<ScheduleLog> {

	/**
	 * @Title：list  
	 * @Description: 定时任务执行日志表分页查询
	 * @author: fanhaohao
	 * @date 2020年01月14 16:55:11
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public List<Map<String, Object>> list(Page<ScheduleLog> page,Map<String, Object> map);
	
	/**
	 * @Title：deleteBatchByIdLogic  
	 * @Description: 定时任务执行日志表根据id逻辑删除
	 * @author: fanhaohao
	 * @date 2020年01月14 16:55:11
	 * @param @param id 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByIdLogic(Object id);
	
	/**
	 * @Title：deleteBatchByIdsLogic  
	 * @Description: 定时任务执行日志表根据ids逻辑批量删除
	 * @author: fanhaohao
	 * @date 2020年01月14 16:55:11
	 * @param @param ids 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByIdsLogic(List<Object> ids);
}
