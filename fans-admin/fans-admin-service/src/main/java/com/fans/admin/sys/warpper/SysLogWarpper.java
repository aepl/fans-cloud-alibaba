package com.fans.admin.sys.warpper;

import java.util.Map;

import com.fans.common.base.BaseControllerWarpper;

 /**
 * @ClassName: SysLogWarpper
 * @description:
 * @date: 2019年12月28 09:27:28
 * @author: fanhaohao
 * @version: 1.0
 */
public class SysLogWarpper extends BaseControllerWarpper {

	public SysLogWarpper(Object obj) {
		super(obj);
    }
    
	/**
	 * 进行字段转换
	 */
	@Override
	protected void warpTheMap(Map<String, Object> map) {
		
	}

}
