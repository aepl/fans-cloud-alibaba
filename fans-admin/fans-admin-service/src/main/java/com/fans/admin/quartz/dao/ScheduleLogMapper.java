package com.fans.admin.quartz.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.quartz.entity.ScheduleLog;

/**
 * @description: 定时任务执行日志表Mapper接口
 * @date: 2020年01月14 16:55:11
 * @author: fanhaohao
 * @version: 1.0
 */
public interface ScheduleLogMapper extends BaseMapper<ScheduleLog> {

	/**
	 * 
	 * @Title：list  
	 * @Description: 定时任务执行日志表分页查询
	 * @author: fanhaohao
	 * @date 2020年1月17日 上午9:33:40 
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public List<Map<String, Object>> list(@Param("page") Page<ScheduleLog> page, Map<String, Object> map);

}