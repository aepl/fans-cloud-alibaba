package com.fans.admin.quartz.qz;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fans.admin.quartz.entity.ScheduleJob;
import com.fans.common.utils.BlankUtils;
@Component
public class ExceptionTest {
	private Logger logger = LoggerFactory.getLogger(ExceptionTest.class);

	public void execute(ScheduleJob job) throws Exception {
		logger.info("=============================测试定时任务【" + job.getJobName() + "】起来了!===========================");
		if(BlankUtils.isNotBlank(job)){
			logger.info("=============================测试定时任务【" + job.getJobName() + "】执行中===========================");
			throw new Exception();
		}
		logger.info("=============================测试定时任务【" + job.getJobName() + "】结束了!===========================");
	}
}
