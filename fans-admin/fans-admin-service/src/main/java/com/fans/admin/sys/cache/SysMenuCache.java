package com.fans.admin.sys.cache;

import java.util.List;
import java.util.Map;

import com.fans.admin.sys.service.ISysMenuService;
import com.fans.common.utils.SpringUtil;

/**
 * @ClassName: SysMenuCache  
 * @Description: 按钮（menu）缓存 
 * @author fanhaohao
 * @date 2019年1月10日 上午9:41:10
 */
public class SysMenuCache {
	/**
	 * @Title：getAllMenuList  
	 * @Description: 获取所有的按钮节点
	 * @author: fanhaohao
	 * @date 2019年1月8日 上午11:01:44 
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public static List<Map<String, Object>> getAllMenuList() {
		ISysMenuService sysMenuService = SpringUtil.getBean(ISysMenuService.class);
		return sysMenuService.listAll();
	}
}
