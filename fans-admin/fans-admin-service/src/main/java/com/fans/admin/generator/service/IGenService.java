package com.fans.admin.generator.service;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.generator.entity.TableInfo;

/**
 * 
 * @ClassName: IGenService
 * @Description: 代码生成 服务层
 * @author fanhaohao
 * @date 2020年1月17日 下午2:49:57
 */
public interface IGenService
{
    /**
     * 查询数据库表信息
     * 
     * @param tableInfo 表信息
     * @return 数据库表列表
     */
	public List<TableInfo> selectTableList(Page<TableInfo> page, TableInfo tableInfo);

    /**
     * 生成代码
     * 
     * @param tableName 表名称
     * @return 数据
     */
    public byte[] generatorCode(String tableName);

    /**
     * 批量生成代码
     * 
     * @param tableNames 表数组
     * @return 数据
     */
    public byte[] generatorCode(String[] tableNames);
}
