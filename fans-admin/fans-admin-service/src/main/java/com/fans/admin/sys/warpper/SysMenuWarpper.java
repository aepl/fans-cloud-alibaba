package com.fans.admin.sys.warpper;

import java.util.Map;

import com.fans.common.base.BaseControllerWarpper;

 /**
 * @ClassName: SysMenuWarpper
 * @description: 系统_资源
 * @date: 2019年12月19 14:53:59
 * @author: fanhaohao
 * @version: 1.0
 */
public class SysMenuWarpper extends BaseControllerWarpper {

	public SysMenuWarpper(Object obj) {
		super(obj);
    }
    
	/**
	 * 进行字段转换
	 */
	@Override
	protected void warpTheMap(Map<String, Object> map) {
		
	}

}
