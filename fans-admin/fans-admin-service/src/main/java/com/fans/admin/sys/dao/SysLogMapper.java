package com.fans.admin.sys.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.sys.entity.SysLog;

/**
 * @description: Mapper接口
 * @date: 2019年12月28 09:27:28
 * @author: fanhaohao
 * @version: 1.0
 */
public interface SysLogMapper extends BaseMapper<SysLog> {

	/**
	 * @Title：list  
	 * @Description: 分页查询
	 * @author: fanhaohao
	 * @date 2019年12月28 09:27:28
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public List<Map<String, Object>> list(@Param("page") Page<SysLog> page, Map<String, Object> map);
	
	/**
	 * @Title：getAllIds  
	 * @Description: 获取所有的日志id
	 * @author: fanhaohao
	 * @date 2019年1月3日 下午4:42:24 
	 * @param @return 
	 * @return List<String> 
	 * @throws
	 */
	public List<String> getAllIds();

}