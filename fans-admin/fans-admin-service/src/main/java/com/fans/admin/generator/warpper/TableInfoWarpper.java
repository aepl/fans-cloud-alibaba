package com.fans.admin.generator.warpper;

import java.util.Map;

import com.fans.common.base.BaseControllerWarpper;

 /**
 * @ClassName: TableInfoWarpper
 * @description: 表信息
 * @date: 2019年12月26 14:40:15
 * @author: fanhaohao
 * @version: 1.0
 */
public class TableInfoWarpper extends BaseControllerWarpper {

	public TableInfoWarpper(Object obj) {
		super(obj);
    }
    
	/**
	 * 进行字段转换
	 */
	@Override
	protected void warpTheMap(Map<String, Object> map) {
		
	}

}
