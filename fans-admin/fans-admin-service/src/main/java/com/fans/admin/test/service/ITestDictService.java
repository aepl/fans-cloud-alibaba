package com.fans.admin.test.service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.test.entity.TestDict;
import com.baomidou.mybatisplus.service.IService;
 

/**
 * @description: 测试服务类
 * @date: 2020-04-15
 * @author: fanhaohao
 * @version: 1.0
 * @copyright: 版权所有 fans (c)2020 
 */
public interface ITestDictService extends IService<TestDict> {

	/**
	 * @Title：list  
	 * @Description: 测试分页查询
	 * @author: fanhaohao
	 * @date 2020-04-15
	 * @param page
	 * @param map
	 * @return List<Map<String,Object>> 
	 */
	public List<Map<String, Object>> list(Page<TestDict> page,Map<String, Object> map);
	
	/**
	 * @Title：deleteBatchByIdLogic  
	 * @Description: 测试根据id逻辑删除
	 * @author: fanhaohao
	 * @date 2020-04-15
	 * @param id 
	 * @return void 
	 */
	public void deleteBatchByIdLogic(Object id);
	
	/**
	 * @Title：deleteBatchByIdsLogic  
	 * @Description: 测试根据ids逻辑批量删除
	 * @author: fanhaohao
	 * @date 2020-04-15
	 * @param ids 
	 * @return void 
	 */
	public void deleteBatchByIdsLogic(List<Object> ids);
}
