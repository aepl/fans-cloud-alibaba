package com.fans.admin.quartz.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.quartz.entity.ScheduleJob;


/**
 * @description: 定时任务表Mapper接口
 * @date: 2020年01月14 16:55:11
 * @author: fanhaohao
 * @version: 1.0
 */
public interface ScheduleJobMapper extends BaseMapper<ScheduleJob> {

	/**
	 * 
	 * @Title：list  
	 * @Description: 定时任务表分页查询 
	 * @author: fanhaohao
	 * @date 2020年1月17日 上午9:33:56 
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public List<Map<String, Object>> list(@Param("page") Page<ScheduleJob> page, Map<String, Object> map);
	
	/**
	 * 
	 * @Title：listByIds  
	 * @Description: 根据id list集合来查询
	 * @author: fanhaohao
	 * @date 2020年1月17日 上午9:34:05 
	 * @param @param scheduleJobIds
	 * @param @return 
	 * @return List<ScheduleJob> 
	 * @throws
	 */
	public List<ScheduleJob> listByIds(@Param("scheduleJobIds") List<String> scheduleJobIds);

}