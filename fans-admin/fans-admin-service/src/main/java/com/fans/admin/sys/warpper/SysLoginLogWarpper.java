package com.fans.admin.sys.warpper;

import java.util.Map;

import com.fans.common.base.BaseControllerWarpper;

 /**
 * @ClassName: SysLoginLogWarpper
 * @description: 用户登录日志
 * @date: 2020年01月10 16:53:50
 * @author: fanhaohao
 * @version: 1.0
 */
public class SysLoginLogWarpper extends BaseControllerWarpper {

	public SysLoginLogWarpper(Object obj) {
		super(obj);
    }
    
	/**
	 * 进行字段转换
	 */
	@Override
	protected void warpTheMap(Map<String, Object> map) {
		
	}

}
