package com.fans.admin.test.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import com.fans.admin.test.entity.TestDict;
import com.fans.admin.test.dao.TestDictMapper;
import com.fans.admin.test.service.ITestDictService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fans.common.utils.jwt.TokenUtil;

/**
 * @description: 测试服务实现类
 * @date: 2020-04-15
 * @author: fanhaohao
 * @version: 1.0
 * @copyright: 版权所有 fans (c)2020 
 */ 
@Service
public class TestDictServiceImpl extends ServiceImpl<TestDictMapper,TestDict> implements ITestDictService {

	@Autowired
    private TestDictMapper testDictMapper;
    
	/**
	 * @Title：list  
	 * @Description: 测试分页查询
	 * @author: fanhaohao
	 * @date 2020-04-15
	 * @param @param page
	 * @param @param map
	 * @return List<Map<String,Object>> 
	 */
	@Override
	public List<Map<String, Object>> list(Page<TestDict> page,Map<String, Object> map){
		return testDictMapper.list(page,map);
	}
	
	/**
	 * @Title：deleteBatchByIdLogic  
	 * @Description: 测试根据id逻辑删除
	 * @author: fanhaohao
	 * @date 2020-04-15
	 * @param @param id 
	 * @return void 
	 */
	@Override
	public void deleteBatchByIdLogic(Object id){
		TestDict testDict = new TestDict();
		testDict.setId(String.valueOf(id));
		testDict.preUpdate();
		testDict.setUpdateBy(TokenUtil.getUserId());
		testDict.setDelFlag("1");
		this.updateById(testDict);
	}
	
	/**
	 * @Title：deleteBatchByIdsLogic    
	 * @Description: 测试逻辑批量删除
	 * @author: fanhaohao
	 * @date 2020-04-15
	 * @param @param ids 
	 * @return void 
	 */
	@Override
	public void deleteBatchByIdsLogic(List<Object> ids) {
		List<TestDict> list = new ArrayList<>();
		for (Object id : ids) {
			TestDict testDict = new TestDict();
			testDict.setId(String.valueOf(id));
			testDict.preUpdate();
			testDict.setUpdateBy(TokenUtil.getUserId());
			testDict.setDelFlag("1");
			list.add(testDict);
		}
		this.updateBatchById(list);
	}
}
