package com.fans.admin.quartz.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * #============================================================================
 * # Configure JobStore # Using Spring datasource in quartzJobsConfig.xml #
 * Spring uses LocalDataSourceJobStore extension of JobStoreCMT
 * #============================================================================
 * org.quartz.jobStore.useProperties=true org.quartz.jobStore.tablePrefix=QRTZ_
 * ## \u901a\u8fc7\u8bbe\u7f6e"org.quartz.jobStore.isClustered"\u5c5e\u6027\
 * u4e3a"true"\u6765\u6fc0\u6d3b\u96c6\u7fa4\u7279\u6027
 * org.quartz.jobStore.isClustered=false # 10 mins
 * org.quartz.jobStore.clusterCheckinInterval=5000
 * org.quartz.jobStore.misfireThreshold=60000
 * org.quartz.jobStore.txIsolationLevelReadCommitted=true
 * 
 * # Change this to match your DB vendor
 * org.quartz.jobStore.class=org.quartz.impl.jdbcjobstore.JobStoreTX
 * org.quartz.jobStore.driverDelegateClass=org.quartz.impl.jdbcjobstore.
 * StdJDBCDelegate
 * 
 * @author fnahaohao
 *
 */
@Component
@ConfigurationProperties(prefix = "org.quartz.job-store")
@PropertySource("classpath:quartz.properties")  
public class JobStoreProperties {
	
	private boolean  useProperties=true;
	private String   tablePrefix="QRTZ_";
	private boolean  isClustered=false;
	private String  clusterCheckinInterval="5000";
	private String  misfireThreshold="60000";
	private boolean txIsolationLevelReadCommitted=true;
	private String jobStoreClass="org.quartz.impl.jdbcjobstore.JobStoreTX";
	private String driverDelegateClass="org.quartz.impl.jdbcjobstore.StdJDBCDelegate";
    private String maxMisfiresToHandleAtATime="1";
	public boolean isUseProperties() {
		return useProperties;
	}
	public void setUseProperties(boolean useProperties) {
		this.useProperties = useProperties;
	}
	public String getTablePrefix() {
		return tablePrefix;
	}
	public void setTablePrefix(String tablePrefix) {
		this.tablePrefix = tablePrefix;
	}
	public boolean isClustered() {
		return isClustered;
	}
	public void setClustered(boolean isClustered) {
		this.isClustered = isClustered;
	}
	public String getClusterCheckinInterval() {
		return clusterCheckinInterval;
	}
	public void setClusterCheckinInterval(String clusterCheckinInterval) {
		this.clusterCheckinInterval = clusterCheckinInterval;
	}
	public String getMisfireThreshold() {
		return misfireThreshold;
	}
	public void setMisfireThreshold(String misfireThreshold) {
		this.misfireThreshold = misfireThreshold;
	}
	public boolean isTxIsolationLevelReadCommitted() {
		return txIsolationLevelReadCommitted;
	}
	public void setTxIsolationLevelReadCommitted(boolean txIsolationLevelReadCommitted) {
		this.txIsolationLevelReadCommitted = txIsolationLevelReadCommitted;
	}
	public String getJobStoreClass() {
		return jobStoreClass;
	}
	public void setJobStoreClass(String jobStoreClass) {
		this.jobStoreClass = jobStoreClass;
	}
	public String getDriverDelegateClass() {
		return driverDelegateClass;
	}
	public void setDriverDelegateClass(String driverDelegateClass) {
		this.driverDelegateClass = driverDelegateClass;
	}
	public String getMaxMisfiresToHandleAtATime() {
		return maxMisfiresToHandleAtATime;
	}
	public void setMaxMisfiresToHandleAtATime(String maxMisfiresToHandleAtATime) {
		this.maxMisfiresToHandleAtATime = maxMisfiresToHandleAtATime;
	}
	
	
	
	


}
