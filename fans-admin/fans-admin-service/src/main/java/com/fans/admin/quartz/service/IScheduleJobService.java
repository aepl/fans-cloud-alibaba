package com.fans.admin.quartz.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.fans.admin.quartz.entity.ScheduleJob;
 
/**
 * @description: 定时任务表服务类
 * @date: 2020年01月14 16:55:11
 * @author: fanhaohao
 * @version: 1.0
 */
public interface IScheduleJobService extends IService<ScheduleJob> {

	/**
	 * @Title：list  
	 * @Description: 定时任务表分页查询
	 * @author: fanhaohao
	 * @date 2020年01月14 16:55:11
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public List<Map<String, Object>> list(Page<ScheduleJob> page,Map<String, Object> map);
	
	/**
	 * @Title：deleteBatchByIdLogic  
	 * @Description: 定时任务表根据id逻辑删除
	 * @author: fanhaohao
	 * @date 2020年01月14 16:55:11
	 * @param @param id 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByIdLogic(Object id);
	
	/**
	 * @Title：deleteBatchByIdsLogic  
	 * @Description: 定时任务表根据ids逻辑批量删除
	 * @author: fanhaohao
	 * @date 2020年01月14 16:55:11
	 * @param @param ids 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByIdsLogic(List<String> ids);
	
	/**
	 * @Title：findList 
	 * @Description: 查询
	 * @author: fanhaohao
	 * @date 2020年1月10日 上午9:02:05 
	 * @param @return 
	 * @return List<ScheduleJob> 
	 * @throws
	 */
	public List<ScheduleJob> findList(ScheduleJob scheduleJob);

	/**
	 * @Title：listByIds  
	 * @Description: 根据id list集合来查询
	 * @author: fanhaohao
	 * @date 2020年1月15日 上午9:23:04 
	 * @param @param idArr
	 * @param @return 
	 * @return List<ScheduleJob> 
	 * @throws
	 */
	public List<ScheduleJob> listByIds(List<String> idArr);
	
	/**===========================================业务操作====================================================**/
	/**
	 * 判断是否存在任务
	 * 
	 * @param scheduler
	 * @param jobId
	 * @return
	 */
	public Boolean isExistScheduler(Scheduler scheduler, String jobId);

	/**
	 * 修改状态
	 * 
	 * @param scheduleJobIds
	 * @param status
	 */
	public void updateStatus(List<String> scheduleJobIds, String status) throws SchedulerException;
	
	/**
	 * 添加或者修改任务信息
	 * @param jobGroup
	 * @param jobName
	 * @param cronExpression
	 * @param isStartJob 是否启动任务
	 * @return
	 */
	public ScheduleJob createOrUpdate(HttpServletRequest request, ScheduleJob scheduleJob) throws Exception;

	/**
	 * 计划任务信息列表
	 */
	public List<ScheduleJob> planScheduleListJson() throws SchedulerException;

	/**
	 * 运行中任务信息列表
	 */
	public List<ScheduleJob> runningScheduleListJson() throws SchedulerException;

	/**
	 * 创建任务
	 */
	public void createScheduleJob(List<String> scheduleJobIds) throws SchedulerException;

	/**
	 * 暂停任务
	 */
	public void pauseScheduleJob(List<String> scheduleJobIds) throws SchedulerException;

	/**
	 * 恢复任务
	 */
	public void resumeScheduleJob(List<String> scheduleJobIds) throws SchedulerException;

	/**
	 * 删除任务
	 */
	public void deleteScheduleJob(List<String> scheduleJobIds) throws SchedulerException;
	
	/**
	 * 立即运行任务
	 */
	public void triggerScheduleJob(List<String> scheduleJobIds) throws SchedulerException;
	/**
	 * 修改表达式
	 * @param scheduleJobId
	 * @throws SchedulerException
	 */
	public void rescheduleScheduleJob(HttpServletRequest request, List<String> scheduleJobIds, String cronExpression)
			throws SchedulerException;
	
	/**
	 * 通过表达式计算时间结果
	 * @param cronExpression
	 * @throws SchedulerException
	 */
	public Date getCronExpressionResult(Date startTime, String cronExpression) throws SchedulerException;

}
