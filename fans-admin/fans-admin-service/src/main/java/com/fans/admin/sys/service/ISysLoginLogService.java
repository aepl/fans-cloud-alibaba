package com.fans.admin.sys.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.fans.admin.sys.entity.SysLoginLog;
 
/**
 * @description: 用户登录日志服务类
 * @date: 2020年01月11 10:01:55
 * @author: fanhaohao
 * @version: 1.0
 */
public interface ISysLoginLogService extends IService<SysLoginLog> {

	/**
	 * @Title：list  
	 * @Description: 用户登录日志分页查询
	 * @author: fanhaohao
	 * @date 2020年01月11 10:01:55
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public List<Map<String, Object>> list(Page<SysLoginLog> page,Map<String, Object> map);
	
	/**
	 * @Title：deleteBatchByIdLogic  
	 * @Description: 用户登录日志根据id逻辑删除
	 * @author: fanhaohao
	 * @date 2020年01月11 10:01:55
	 * @param @param id 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByIdLogic(Object id);
	
	/**
	 * @Title：deleteBatchByIdsLogic  
	 * @Description: 用户登录日志根据ids逻辑批量删除
	 * @author: fanhaohao
	 * @date 2020年01月11 10:01:55
	 * @param @param ids 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByIdsLogic(List<Object> ids);
	
	/**
	 * @Title：saveLoginLog  
	 * @Description:用户登录日志保存
	 * @author: fanhaohao
	 * @date 2020年1月11日 上午9:52:24 
	 * @param @param request
	 * @param @param logonId
	 * @param @param operType
	 * @param @param resultType
	 * @param @param msg 
	 * @return void 
	 * @throws
	 */
	public void saveLoginLog(HttpServletRequest request, String logonId, String operType,String resultType, String msg);
	
	/**
	 * @Title：getAllIds  
	 * @Description: 获取所有的日志id
	 * @author: fanhaohao
	 * @date 2020年1月3日 下午4:42:24 
	 * @param @return 
	 * @return List<String> 
	 * @throws
	 */
	public List<String> getAllIds();
}
