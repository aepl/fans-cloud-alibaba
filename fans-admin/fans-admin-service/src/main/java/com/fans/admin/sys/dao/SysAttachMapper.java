package com.fans.admin.sys.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.sys.entity.SysAttach;

/**
 * 
 * @ClassName: SysAttachMapper  
 * @Description: 系统_附件Mapper接口 
 * @author fanhaohao
 * @date 2020年1月17日 上午9:18:02
 */
public interface SysAttachMapper extends BaseMapper<SysAttach> {

	/**
	 * 
	 * @Title：list  
	 * @Description: 系统_附件分页查询
	 * @author: fanhaohao
	 * @date 2020年1月17日 上午9:18:11 
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public List<Map<String, Object>> list(@Param("page") Page<SysAttach> page, Map<String, Object> map);

}