package com.fans.admin.sys.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.fans.admin.sys.entity.SysUser;
 
/**
 * @description: 系统_用户服务类
 * @date: 2019年12月19 17:17:39
 * @author: fanhaohao
 * @version: 1.0
 */
public interface ISysUserService extends IService<SysUser> {

	/**
	 * @Title：list  
	 * @Description: 系统_用户分页查询
	 * @author: fanhaohao
	 * @date 2019年12月19 17:17:39
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public List<Map<String, Object>> list(Page<SysUser> page,Map<String, Object> map);
	
	/**
	 * @Title：updateLoginResult  
	 * @Description: 根据登录结果更新登录次数时间信息
	 * @author: fanhaohao
	 * @date 2019年12月20日 上午11:38:55 
	 * @param @param username
	 * @param @param bool 
	 * @return void 
	 * @throws
	 */
    void updateLoginResult(String username, Boolean bool);

    /**
     * @Title：selectByUsername  
     * @Description: 根据名字来查询用户信息
     * @author: fanhaohao
     * @date 2019年12月20日 下午2:49:06 
     * @param @param username
     * @param @return 
     * @return SysUser 
     * @throws
     */
	public SysUser selectByUsername(String username);

	/**
	 * @Title：resetPwd  
	 * @Description: 重置用户密码
	 * @author: fanhaohao
	 * @date 2019年12月25日 下午3:53:48 
	 * @param @param id 
	 * @return void 
	 * @throws
	 */
	public void resetPwd(String id);

	/**
	 * @Title：updatePwd  
	 * @Description: 用户修改密码
	 * @author: fanhaohao
	 * @date 2019年12月25日 下午5:32:31 
	 * @param @param userId
	 * @param @param newpwd 
	 * @return void 
	 * @throws
	 */
	public void updatePwd(String userId, String newpwd);

	/**
	 * @Title：saveUserInfo  
	 * @Description: 保存用户信息
	 * @author: fanhaohao
	 * @date 2019年12月26日 下午4:51:51 
	 * @param @param sysUser 
	 * @return void 
	 * @throws
	 */
	public void saveUserInfo(SysUser sysUser);

	/**
	 * @Title：updateUserInfo  
	 * @Description: 更新用户信息
	 * @author: fanhaohao
	 * @date 2019年12月26日 下午4:53:13 
	 * @param @param sysUser 
	 * @return void 
	 * @throws
	 */
	public void updateUserInfo(SysUser sysUser);

	/**
	 * @Title：deleteBatchByIdLogic  
	 * @Description: 系统_用户根据id逻辑删除
	 * @author: fanhaohao
	 * @date 2019年12月27 10:20:42
	 * @param @param id 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByIdLogic(Object id);
	
	/**
	 * @Title：deleteBatchByIdsLogic  
	 * @Description: 系统_用户根据ids逻辑批量删除
	 * @author: fanhaohao
	 * @date 2019年12月27 10:20:42
	 * @param @param ids 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByIdsLogic(List<Object> ids);

	/**
	 * 根据username和password查询
	 * @param username
	 * @param password
	 * @return
	 */
	public SysUser getByUsernameAndPassword(String username, String password);

	/**
	 * 根据email和password查询
	 * @param email
	 * @param password
	 * @return
	 */
	public SysUser getByEmailAndPassword(String email, String password);

	/**
	 * 根据mobile和password查询
	 * @param mobile
	 * @param password
	 * @return
	 */
	public SysUser getByMobileAndPassword(String mobile, String password);

	/**
	 * 
	 * @Title：getLoginDatas  
	 * @Description: 获取登录后的信息
	 * @author: fanhaohao
	 * @date 2020年1月13日 下午2:48:39 
	 * @param @param username
	 * @param @param menuPerms
	 * @param @return 
	 * @return Map<String,Object> 
	 * @throws
	 */
	public Map<String, Object> getLoginDatas(String username, boolean menuPerms);
}
