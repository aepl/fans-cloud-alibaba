package com.fans.admin.generator.util;

import java.util.Properties;

import org.apache.velocity.app.Velocity;

import com.fans.common.constant.SysConstant;

/**
 * 
 * @ClassName: VelocityInitializer
 * @Description: VelocityEngine工厂
 * @author fanhaohao
 * @date 2020年1月17日 下午2:31:14
 */
public class VelocityInitializer
{
	/**
	 * 
	 * @Title：initVelocity  
	 * @Description: 初始化vm方法
	 * @author: fanhaohao
	 * @date 2020年1月17日 下午2:45:20 
	 * @param  
	 * @return void 
	 * @throws
	 */
    public static void initVelocity()
    {
        Properties p = new Properties();
        try
        {
            // 加载classpath目录下的vm文件
            p.setProperty("file.resource.loader.class",
                    "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
            // 定义字符集
			p.setProperty(Velocity.ENCODING_DEFAULT, SysConstant.UTF8);
			p.setProperty(Velocity.OUTPUT_ENCODING, SysConstant.UTF8);
            // 初始化Velocity引擎，指定配置Properties
            Velocity.init(p);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }
}
