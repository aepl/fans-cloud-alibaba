package com.fans.admin.sys.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.fans.admin.sys.entity.SysWorkBacklog;
 
/**
 * @description: 待办事项服务类
 * @date: 2019年12月26 14:26:39
 * @author: fanhaohao
 * @version: 1.0
 */
public interface ISysWorkBacklogService extends IService<SysWorkBacklog> {

	/**
	 * @Title：list  
	 * @Description: 待办事项分页查询
	 * @author: fanhaohao
	 * @date 2019年12月26 14:26:39
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public List<Map<String, Object>> list(Page<SysWorkBacklog> page,Map<String, Object> map);
	
	/**
	 * @Title：deleteBatchByIdLogic  
	 * @Description: 待办事项根据id逻辑删除
	 * @author: fanhaohao
	 * @date 2019年12月27 10:20:42
	 * @param @param id 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByIdLogic(Object id);
	
	/**
	 * @Title：deleteBatchByIdsLogic  
	 * @Description: 待办事项根据ids逻辑批量删除
	 * @author: fanhaohao
	 * @date 2019年12月27 10:20:42
	 * @param @param ids 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByIdsLogic(List<Object> ids);
}
