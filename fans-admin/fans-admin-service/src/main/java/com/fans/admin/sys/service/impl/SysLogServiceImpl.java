package com.fans.admin.sys.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fans.admin.sys.dao.SysLogMapper;
import com.fans.admin.sys.entity.SysLog;
import com.fans.admin.sys.service.ISysLogService;
import com.fans.common.utils.jwt.TokenUtil;

/**
 * @description: 服务实现类
 * @date: 2019年12月28 09:27:28
 * @author: fanhaohao
 * @version: 1.0
 */
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper,SysLog> implements ISysLogService {

	@Autowired
    private SysLogMapper sysLogMapper;
    
	/**
	 * @Title：list  
	 * @Description: 分页查询
	 * @author: fanhaohao
	 * @date 2019年12月28 09:27:28
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	@Override
	public List<Map<String, Object>> list(Page<SysLog> page,Map<String, Object> map){
		return sysLogMapper.list(page,map);
	}
	
	/**
	 * @Title：deleteBatchByIdLogic  
	 * @Description: 根据id逻辑删除
	 * @author: fanhaohao
	 * @date 2019年12月28 09:27:28
	 * @param @param id 
	 * @return void 
	 * @throws
	 */
	@Override
	public void deleteBatchByIdLogic(Object id){
		SysLog sysLog = new SysLog();
		sysLog.setId(String.valueOf(id));
		sysLog.preUpdate();
		sysLog.setUpdateBy(TokenUtil.getUserId());
		sysLog.setDelFlag("1");
		this.updateById(sysLog);
	}
	
	/**
	 * @Title：deleteBatchByIdsLogic    
	 * @Description: 逻辑批量删除
	 * @author: fanhaohao
	 * @date 2019年12月28 09:27:28
	 * @param @param ids 
	 * @return void 
	 * @throws
	 */
	@Override
	public void deleteBatchByIdsLogic(List<String> ids) {
		List<SysLog> list = new ArrayList<>();
		for (Object id : ids) {
			SysLog sysLog = new SysLog();
			sysLog.setId(String.valueOf(id));
			sysLog.preUpdate();
			sysLog.setUpdateBy(TokenUtil.getUserId());
			sysLog.setDelFlag("1");
			list.add(sysLog);
		}
		this.updateBatchById(list);
	}
	
	/**
	 * @Title：getAllIds  
	 * @Description: 获取所有的日志id
	 * @author: fanhaohao
	 * @date 2019年12月3日 下午4:42:24 
	 * @param @return 
	 * @return List<String> 
	 * @throws
	 */
	@Override
	public List<String> getAllIds() {
		return sysLogMapper.getAllIds();
	}
}
