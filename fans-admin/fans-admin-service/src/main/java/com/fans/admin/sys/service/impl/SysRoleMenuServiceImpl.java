package com.fans.admin.sys.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fans.admin.sys.dao.SysRoleMenuMapper;
import com.fans.admin.sys.entity.SysMenu;
import com.fans.admin.sys.entity.SysRoleMenu;
import com.fans.admin.sys.service.ISysRoleMenuService;
import com.fans.admin.sys.vo.Menu;
import com.fans.admin.sys.vo.RoleMenuPerms;
import com.fans.common.utils.BlankUtils;
import com.fans.common.utils.jwt.TokenUtil;

/**
 * @description: 系统_角色资源服务实现类
 * @date: 2019年12月19 15:26:08
 * @author: fanhaohao
 * @version: 1.0
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper,SysRoleMenu> implements ISysRoleMenuService {

	@Autowired
	private SysRoleMenuMapper sysRoleMenuMapper;
    
	/**
	 * @Title：list  
	 * @Description: 系统_角色资源分页查询
	 * @author: fanhaohao
	 * @date 2019年12月19 15:26:08
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	@Override
	public List<Map<String, Object>> list(Page<SysRoleMenu> page,Map<String, Object> map){
		return sysRoleMenuMapper.list(page, map);
	}
	
	/**
	 * @Title：deleteBatchByIdLogic  
	 * @Description: 系统_角色资源根据id逻辑删除
	 * @author: fanhaohao
	 * @date 2019年12月27 10:31:34
	 * @param @param id 
	 * @return void 
	 * @throws
	 */
	@Override
	public void deleteBatchByIdLogic(Object id){
		SysRoleMenu sysRoleMenu = new SysRoleMenu();
		sysRoleMenu.setId(String.valueOf(id));
		sysRoleMenu.preUpdate();
		sysRoleMenu.setUpdateBy(TokenUtil.getUserId());
		sysRoleMenu.setDelFlag("1");
		this.updateById(sysRoleMenu);
	}
	
	/**
	 * @Title：deleteBatchByIdsLogic    
	 * @Description: 系统_角色资源逻辑批量删除
	 * @author: fanhaohao
	 * @date 2019年12月27 10:31:34
	 * @param @param ids 
	 * @return void 
	 * @throws
	 */
	@Override
	public void deleteBatchByIdsLogic(List<Object> ids) {
		List<SysRoleMenu> list = new ArrayList<>();
		for (Object id : ids) {
			SysRoleMenu sysRoleMenu = new SysRoleMenu();
			sysRoleMenu.setId(String.valueOf(id));
			sysRoleMenu.preUpdate();
			sysRoleMenu.setUpdateBy(TokenUtil.getUserId());
			sysRoleMenu.setDelFlag("1");
			list.add(sysRoleMenu);
		}
		this.updateBatchById(list);
	}
	
	/**
     * @Title：getRoleMenuList  
     * @Description: 根据角色ids和菜单类型来获取菜单列表
     * @author: fanhaohao
     * @date 2019年12月20日 下午3:02:20 
     * @param @param roleIds
     * @param @param menuType
     * @param @return 
     * @return List<SysMenu> 
     * @throws
     */
	@Override
	public List<SysMenu> getRoleMenuList(String roleIds, String menuType) {
		return baseMapper.listByRoleId(roleIds, menuType);
	}

	/**
	 * @Title：getRoleMenuAndPerms  
	 * @Description: 根据角色id获得权限集合和菜单列表
	 * @author: fanhaohao
	 * @date 2019年12月20日 下午2:58:14 
	 * @param @param roleIds
	 * @param @return 
	 * @return RoleMenuPerms 
	 * @throws
	 */
	@Override
	public RoleMenuPerms getRoleMenuAndPerms(String roleIds) {
		RoleMenuPerms result = new RoleMenuPerms();
		HashSet<String> permissionList = new HashSet<>();
		List<Menu> menuList = new ArrayList<>();
		List<SysMenu> sysMenuList = getRoleMenuList(roleIds, null);
		for (SysMenu sysMenu : sysMenuList) {
			if (sysMenu.getMenuType() != null) {
				if ("1".equals(sysMenu.getMenuType())) {
					Menu menu = new Menu();
					menu.setId(sysMenu.getId());
					menu.setIcon(sysMenu.getIcon());
					menu.setParentId(sysMenu.getParentId());
					menu.setName(sysMenu.getIdentity());
					menu.setTitle(sysMenu.getName());
					menu.setUrl(sysMenu.getUrl());
					menu.setOutLink(sysMenu.getOutLink());
					menu.setDisplayMode(sysMenu.getDisplayMode());
					menuList.add(menu);
				} else if ("2".equals(sysMenu.getMenuType())) {
					permissionList.add(sysMenu.getIdentity());
				}
			}
		}
		result.setMenuList(menuList);
		result.setPermsList(permissionList);
		return result;
	}

	/**
	 * @Title：getMenuIdsByRoleId  
	 * @Description: 根据 角色id来查询menu id的list集合
	 * @author: fanhaohao
	 * @date 2019年12月27日 下午3:57:38 
	 * @param @param roleId
	 * @param @return 
	 * @return List<String> 
	 * @throws
	 */
	@Override
	public List<String> getMenuIdsByRoleId(String roleId) {
		return baseMapper.getMenuIdsByRoleId(roleId);
	}
	
	/**
	 * @Title：saveRoleMenu  
	 * @Description: 保存角色资源菜单
	 * @author: fanhaohao
	 * @date 2019年12月27日 下午4:57:28 
	 * @param @param menuIds
	 * @param @param roleId 
	 * @return void 
	 * @throws
	 */
	@Override
	public void saveRoleMenu(String menuIds, String roleId) {
		List<String> menuIdList = this.getMenuIdsByRoleId(roleId);
		if (BlankUtils.isNotBlank(menuIds)) {
			List<SysRoleMenu> insertEntityList = new ArrayList<>();
			String[] menuIdArray = menuIds.split(",");
			for (String ma : menuIdArray) {
				if (!menuIdList.contains(ma)) {
					SysRoleMenu roleMenu = new SysRoleMenu();
					roleMenu.setRoleId(roleId);
					roleMenu.setMenuId(ma);
					roleMenu.preInsert();
					roleMenu.setCreateBy(TokenUtil.getUserId());
					insertEntityList.add(roleMenu);
				} else {
					menuIdList.remove(ma);
				}
			}
			if (BlankUtils.isNotBlank(insertEntityList)) {
				this.insertBatch(insertEntityList);
			}
			if (BlankUtils.isNotBlank(menuIdList)) {
				this.deleteBatchByMenuIds(menuIdList, roleId);
			}
		} else {// 直接清空该角色对应的角色按钮信息
			if (BlankUtils.isNotBlank(menuIdList)) {
				this.deleteBatchByMenuIds(menuIdList, roleId);
			}
		}
	}

	/**
	 * @Title：deleteBatchByMenuIds  
	 * @Description: 根据menuIdList来删除 角色按钮记录
	 * @author: fanhaohao
	 * @date 2019年12月27日 下午5:16:17 
	 * @param @param menuIdList
	 * @param @param roleId 
	 * @return void 
	 * @throws
	 */
	private void deleteBatchByMenuIds(List<String> menuIdList, String roleId) {
		sysRoleMenuMapper.deleteBatchByMenuIds(menuIdList, roleId);
	}

}
