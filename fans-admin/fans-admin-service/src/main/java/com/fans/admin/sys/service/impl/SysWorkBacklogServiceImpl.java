package com.fans.admin.sys.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fans.admin.sys.dao.SysWorkBacklogMapper;
import com.fans.admin.sys.entity.SysWorkBacklog;
import com.fans.admin.sys.service.ISysWorkBacklogService;
import com.fans.common.utils.jwt.TokenUtil;

/**
 * @description: 待办事项服务实现类
 * @date: 2019年12月27 10:31:34
 * @author: fanhaohao
 * @version: 1.0
 */
@Service
public class SysWorkBacklogServiceImpl extends ServiceImpl<SysWorkBacklogMapper,SysWorkBacklog> implements ISysWorkBacklogService {

	@Autowired
    private SysWorkBacklogMapper sysWorkBacklogMapper;
    
	/**
	 * @Title：list  
	 * @Description: 待办事项分页查询
	 * @author: fanhaohao
	 * @date 2019年12月27 10:31:34
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	@Override
	public List<Map<String, Object>> list(Page<SysWorkBacklog> page,Map<String, Object> map){
		return sysWorkBacklogMapper.list(page,map);
	}
	
	/**
	 * @Title：deleteBatchByIdLogic  
	 * @Description: 待办事项根据id逻辑删除
	 * @author: fanhaohao
	 * @date 2019年12月27 10:31:34
	 * @param @param id 
	 * @return void 
	 * @throws
	 */
	@Override
	public void deleteBatchByIdLogic(Object id){
		SysWorkBacklog sysWorkBacklog = new SysWorkBacklog();
		sysWorkBacklog.setId(String.valueOf(id));
		sysWorkBacklog.preUpdate();
		sysWorkBacklog.setUpdateBy(TokenUtil.getUserId());
		sysWorkBacklog.setDelFlag("1");
		this.updateById(sysWorkBacklog);
	}
	
	/**
	 * @Title：deleteBatchByIdsLogic    
	 * @Description: 待办事项逻辑批量删除
	 * @author: fanhaohao
	 * @date 2019年12月27 10:31:34
	 * @param @param ids 
	 * @return void 
	 * @throws
	 */
	@Override
	public void deleteBatchByIdsLogic(List<Object> ids) {
		List<SysWorkBacklog> list = new ArrayList<>();
		for (Object id : ids) {
			SysWorkBacklog sysWorkBacklog = new SysWorkBacklog();
			sysWorkBacklog.setId(String.valueOf(id));
			sysWorkBacklog.preUpdate();
			sysWorkBacklog.setUpdateBy(TokenUtil.getUserId());
			sysWorkBacklog.setDelFlag("1");
			list.add(sysWorkBacklog);
		}
		this.updateBatchById(list);
	}
}
