package com.fans.admin.test.warpper;

import java.util.Map;

import com.fans.common.base.BaseControllerWarpper;

 /**
  * @description: 测试Warpper
  * @date: 2020-04-15
  * @author: fanhaohao
  * @version: 1.0
  * @copyright: 版权所有 fans (c)2020 
  */ 
public class TestDictWarpper extends BaseControllerWarpper {

	public TestDictWarpper(Object obj) {
		super(obj);
    }
    
	/**
	 * 进行字段转换
	 */
	@Override
	protected void warpTheMap(Map<String, Object> map) {
		
	}

}
