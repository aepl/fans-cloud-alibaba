package com.fans.admin.sys.warpper;

import java.util.Map;

import com.fans.common.base.BaseControllerWarpper;

 /**
 * @ClassName: SysDictItemWarpper
 * @description: 字典
 * @date: 2019年12月30 18:18:58
 * @author: fanhaohao
 * @version: 1.0
 */
public class SysDictItemWarpper extends BaseControllerWarpper {

	public SysDictItemWarpper(Object obj) {
		super(obj);
    }
    
	/**
	 * 进行字段转换
	 */
	@Override
	protected void warpTheMap(Map<String, Object> map) {
		
	}

}
