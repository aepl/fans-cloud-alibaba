package com.fans.admin.sys.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fans.admin.sys.dao.SysMenuMapper;
import com.fans.admin.sys.entity.SysMenu;
import com.fans.admin.sys.service.ISysMenuService;
import com.fans.common.utils.jwt.TokenUtil;
import com.fans.common.vo.PageFactory;
import com.fans.common.vo.PageVo;

/**
 * @description: 系统_资源服务实现类
 * @date: 2019年12月27 10:31:34
 * @author: fanhaohao
 * @version: 1.0
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper,SysMenu> implements ISysMenuService {

	@Autowired
    private SysMenuMapper sysMenuMapper;
    
	/**
	 * @Title：list  
	 * @Description: 系统_资源分页查询
	 * @author: fanhaohao
	 * @date 2019年12月27 10:31:34
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	@Override
	public List<Map<String, Object>> list(Page<SysMenu> page,Map<String, Object> map){
		return sysMenuMapper.list(page,map);
	}
	
	/**
	 * @Title：listAll  
	 * @Description: 查询所有
	 * @author: fanhaohao
	 * @date 2019年1月10日 上午9:02:05 
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	@Override
	@Cacheable(value = { "sys:menu:allList" }, key = "'sys:menu:allList'")
	public List<Map<String, Object>> listAll() {
		PageVo pv = new PageVo();
		pv.setSortField("weight");
		pv.setSortOrder("asc");
		Page<SysMenu> page = new PageFactory<SysMenu>().page(pv);
		return sysMenuMapper.list(page, null);
	}
	
	/**
	 * @Title：deleteBatchByIdLogic  
	 * @Description: 系统_资源根据id逻辑删除
	 * @author: fanhaohao
	 * @date 2019年12月27 10:31:34
	 * @param @param id 
	 * @return void 
	 * @throws
	 */
	@Override
	@CacheEvict(value = { "sys:menu:allList" }, allEntries = true, beforeInvocation = true)
	public void deleteBatchByIdLogic(Object id){
		SysMenu sysMenu = new SysMenu();
		sysMenu.setId(String.valueOf(id));
		sysMenu.preUpdate();
		sysMenu.setUpdateBy(TokenUtil.getUserId());
		sysMenu.setDelFlag("1");
		this.updateById(sysMenu);
	}
	
	/**
	 * @Title：deleteBatchByIdsLogic    
	 * @Description: 系统_资源逻辑批量删除
	 * @author: fanhaohao
	 * @date 2019年12月27 10:31:34
	 * @param @param ids 
	 * @return void 
	 * @throws
	 */
	@Override
	@CacheEvict(value = { "sys:menu:allList" }, allEntries = true, beforeInvocation = true)
	public void deleteBatchByIdsLogic(List<Object> ids) {
		List<SysMenu> list = new ArrayList<>();
		for (Object id : ids) {
			SysMenu sysMenu = new SysMenu();
			sysMenu.setId(String.valueOf(id));
			sysMenu.preUpdate();
			sysMenu.setUpdateBy(TokenUtil.getUserId());
			sysMenu.setDelFlag("1");
			list.add(sysMenu);
		}
		this.updateBatchById(list);
	}

	/**
	 * @Title：insert
	 * @Description: 系统_资源添加
	 * @author: fanhaohao
	 * @date 2019年12月30 18:18:58
	 * @param @return 
	 * @throws
	 */
	@Override
	@CacheEvict(value = { "sys:menu:allList" }, allEntries = true, beforeInvocation = true)
	public boolean insert(SysMenu sysMenu){
		return super.insert(sysMenu);
	}

	/**
	 * @Title：updateById
	 * @Description: 系统_资源更新
	 * @author: fanhaohao
	 * @date 2019年12月30 18:18:58
	 * @param @return 
	 * @throws
	 */
	@Override
	@CacheEvict(value = { "sys:menu:allList" }, allEntries = true, beforeInvocation = true)
	public boolean updateById(SysMenu sysMenu){
		return super.updateById(sysMenu);
	}
}
