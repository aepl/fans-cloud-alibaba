package com.fans.admin.sys.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.sys.entity.SysMenu;
import com.fans.admin.sys.entity.SysRoleMenu;

/**
 * @description: 系统_角色资源Mapper接口
 * @date: 2019年12月19 15:26:08
 * @author: fanhaohao
 * @version: 1.0
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

	/**
	 * @Title：list  
	 * @Description: 系统_角色资源分页查询
	 * @author: fanhaohao
	 * @date 2019年12月19 15:26:08
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public List<Map<String, Object>> list(@Param("page") Page<SysRoleMenu> page, Map<String, Object> map);
	/**
     * @Title：listByRoleId
     * @Description: 根据角色ids和菜单类型来获取菜单列表
     * @author: fanhaohao
     * @date 2019年12月20日 下午3:02:20 
     * @param @param roleIds
     * @param @param menuType
     * @param @return 
     * @return List<SysMenu> 
     * @throws
     */
	public List<SysMenu> listByRoleId(@Param("roleIds") String roleIds, @Param("menuType") String menuType);

	/**
	 * @Title：getMenuIdsByRoleId  
	 * @Description: 根据 角色id来查询menu id的list集合
	 * @author: fanhaohao
	 * @date 2019年12月27日 下午3:57:38 
	 * @param @param roleId
	 * @param @return 
	 * @return List<String> 
	 * @throws
	 */
	public List<String> getMenuIdsByRoleId(@Param("roleId") String roleId);
	
	/**
	 * @Title：deleteBatchByMenuIds  
	 * @Description: 根据menuIdList来删除 角色按钮记录
	 * @author: fanhaohao
	 * @date 2019年12月27日 下午5:16:17 
	 * @param @param menuIdList
	 * @param @param roleId 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByMenuIds(@Param("menuIdList") List<String> menuIdList, @Param("roleId") String roleId);

}