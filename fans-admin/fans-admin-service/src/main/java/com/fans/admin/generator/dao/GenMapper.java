package com.fans.admin.generator.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.generator.entity.ColumnInfo;
import com.fans.admin.generator.entity.TableInfo;

/**
 * 
 * @ClassName: GenMapper
 * @Description: 代码生成 数据层
 * @author fanhaohao
 * @date 2020年1月17日 下午2:48:35
 */
public interface GenMapper extends BaseMapper<TableInfo>
{
    /**
	 * 查询数据库表信息
	 * 
	 * @param tableInfo 表信息
	 *            
	 * @return 数据库表列表
	 */
	public List<TableInfo> selectTableList(@Param("page") Page<TableInfo> page, TableInfo tableInfo);

    /**
     * 根据表名称查询信息
     * 
     * @param tableName 表名称
     * @return 表信息
     */
    public TableInfo selectTableByName(String tableName);

    /**
     * 根据表名称查询列信息
     * 
     * @param tableName 表名称
     * @return 列信息
     */
    public List<ColumnInfo> selectTableColumnsByName(String tableName);
}
