package com.fans.admin.generator.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "gen")
public class GenProperties {
	// 作者
	private String author = "fanhaohao";
	// 默认生成包路径 system 需改成自己的模块名称 如com.fans.admin.test
	private String packageName = "com.fans.admin";
	// 自动去除表前缀，默认是true
	private Boolean autoRemovePre = true;
	// 表前缀(类名不会包含表前缀)
	private String tablePrefix = "fans_";
	// 版本
	private String version = "1.0";
	// 版权
	private String copyright = "版权所有 fans (c)2020";
	// 是否开启缓存
	private Boolean enableCache = false;

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public Boolean getAutoRemovePre() {
		return autoRemovePre;
	}

	public void setAutoRemovePre(Boolean autoRemovePre) {
		this.autoRemovePre = autoRemovePre;
	}

	public String getTablePrefix() {
		return tablePrefix;
	}

	public void setTablePrefix(String tablePrefix) {
		this.tablePrefix = tablePrefix;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getCopyright() {
		return copyright;
	}

	public void setCopyright(String copyright) {
		this.copyright = copyright;
	}

	public Boolean getEnableCache() {
		return enableCache;
	}

	public void setEnableCache(Boolean enableCache) {
		this.enableCache = enableCache;
	}

}
