package com.fans.admin.sys.warpper;

import java.util.Map;

import com.fans.common.base.BaseControllerWarpper;

 /**
 * @ClassName: SysAttachWarpper
 * @description: 系统_附件
 * @date: 2019年12月26 14:40:15
 * @author: fanhaohao
 * @version: 1.0
 */
public class SysAttachWarpper extends BaseControllerWarpper {

	public SysAttachWarpper(Object obj) {
		super(obj);
    }
    
	/**
	 * 进行字段转换
	 */
	@Override
	protected void warpTheMap(Map<String, Object> map) {
		
	}

}
