package com.fans.admin.sys.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fans.admin.sys.dao.SysRoleMapper;
import com.fans.admin.sys.entity.SysRole;
import com.fans.admin.sys.service.ISysRoleService;
import com.fans.common.utils.jwt.TokenUtil;
import com.fans.common.vo.PageFactory;
import com.fans.common.vo.PageVo;

/**
 * @description: 系统_角色服务实现类
 * @date: 2019年12月27 10:31:34
 * @author: fanhaohao
 * @version: 1.0
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper,SysRole> implements ISysRoleService {

	@Autowired
    private SysRoleMapper sysRoleMapper;
    
	/**
	 * @Title：list  
	 * @Description: 系统_角色分页查询
	 * @author: fanhaohao
	 * @date 2019年12月27 10:31:34
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	@Override
	public List<Map<String, Object>> list(Page<SysRole> page,Map<String, Object> map){
		return sysRoleMapper.list(page,map);
	}
	
	/**
	 * @Title：listAll  
	 * @Description: 查询所有
	 * @author: fanhaohao
	 * @date 2019年1月10日 上午9:02:05 
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	@Override
	@Cacheable(value = { "sys:role:allList" }, key = "'sys:role:allList'")
	public List<Map<String, Object>> listAll() {
		PageVo pv = new PageVo();
		Page<SysRole> page = new PageFactory<SysRole>().page(pv);
		return sysRoleMapper.list(page, new HashMap<String, Object>());
	}
	
	/**
	 * @Title：deleteBatchByIdLogic  
	 * @Description: 系统_角色根据id逻辑删除
	 * @author: fanhaohao
	 * @date 2019年12月27 10:31:34
	 * @param @param id 
	 * @return void 
	 * @throws
	 */
	@Override
	@CacheEvict(value = { "sys:role:allList" }, allEntries = true, beforeInvocation = true)
	public void deleteBatchByIdLogic(Object id){
		SysRole sysRole = new SysRole();
		sysRole.setId(String.valueOf(id));
		sysRole.preUpdate();
		sysRole.setUpdateBy(TokenUtil.getUserId());
		sysRole.setDelFlag("1");
		this.updateById(sysRole);
	}
	
	/**
	 * @Title：deleteBatchByIdsLogic    
	 * @Description: 系统_角色逻辑批量删除
	 * @author: fanhaohao
	 * @date 2019年12月27 10:31:34
	 * @param @param ids 
	 * @return void 
	 * @throws
	 */
	@Override
	@CacheEvict(value = { "sys:role:allList" }, allEntries = true, beforeInvocation = true)
	public void deleteBatchByIdsLogic(List<Object> ids) {
		List<SysRole> list = new ArrayList<>();
		for (Object id : ids) {
			SysRole sysRole = new SysRole();
			sysRole.setId(String.valueOf(id));
			sysRole.preUpdate();
			sysRole.setUpdateBy(TokenUtil.getUserId());
			sysRole.setDelFlag("1");
			list.add(sysRole);
		}
		this.updateBatchById(list);
	}
	
	/**
	 * @Title：insert
	 * @Description: 系统_角色添加
	 * @author: fanhaohao
	 * @date 2019年12月30 18:18:58
	 * @param @return 
	 * @throws
	 */
	@Override
	@CacheEvict(value = { "sys:role:allList" }, allEntries = true, beforeInvocation = true)
	public boolean insert(SysRole sysRole){
		return super.insert(sysRole);
	}

	/**
	 * @Title：updateById
	 * @Description: 系统_角色更新
	 * @author: fanhaohao
	 * @date 2019年12月30 18:18:58
	 * @param @return 
	 * @throws
	 */
	@Override
	@CacheEvict(value = { "sys:role:allList" }, allEntries = true, beforeInvocation = true)
	public boolean updateById(SysRole sysRole){
		return super.updateById(sysRole);
	}
}
