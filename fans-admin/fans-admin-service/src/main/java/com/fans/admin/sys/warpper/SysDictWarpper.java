package com.fans.admin.sys.warpper;

import java.util.Map;

import com.fans.common.base.BaseControllerWarpper;

 /**
 * @ClassName: SysDictWarpper
 * @description: 基础字典类型
 * @date: 2019年12月30 18:18:58
 * @author: fanhaohao
 * @version: 1.0
 */
public class SysDictWarpper extends BaseControllerWarpper {

	public SysDictWarpper(Object obj) {
		super(obj);
    }
    
	/**
	 * 进行字段转换
	 */
	@Override
	protected void warpTheMap(Map<String, Object> map) {
		
	}

}
