package com.fans.admin.sys.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.fans.admin.sys.entity.SysMenu;
 
/**
 * @description: 系统_资源服务类
 * @date: 2019年12月19 15:24:24
 * @author: fanhaohao
 * @version: 1.0
 */
public interface ISysMenuService extends IService<SysMenu> {

	/**
	 * @Title：list  
	 * @Description: 系统_资源分页查询
	 * @author: fanhaohao
	 * @date 2019年12月19 15:24:24
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public List<Map<String, Object>> list(Page<SysMenu> page,Map<String, Object> map);
	
	/**
	 * @Title：deleteBatchByIdLogic  
	 * @Description: 系统_资源根据id逻辑删除
	 * @author: fanhaohao
	 * @date 2019年12月27 10:20:42
	 * @param @param id 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByIdLogic(Object id);
	
	/**
	 * @Title：deleteBatchByIdsLogic  
	 * @Description: 系统_资源根据ids逻辑批量删除
	 * @author: fanhaohao
	 * @date 2019年12月27 10:20:42
	 * @param @param ids 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByIdsLogic(List<Object> ids);

	/**
	 * @Title：listAll  
	 * @Description: 查询所有
	 * @author: fanhaohao
	 * @date 2019年1月10日 上午9:02:05 
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public List<Map<String, Object>> listAll();

	/**
	 * @return 
	 * @Title：insert
	 * @Description: 系统_资源添加
	 * @author: fanhaohao
	 * @date 2019年12月30 18:18:58
	 * @param @return 
	 * @throws
	 */
	@Override
	public boolean insert(SysMenu sysMenu);
	
	/**
	 * @Title：updateById
	 * @Description: 系统_资源更新
	 * @author: fanhaohao
	 * @date 2019年12月30 18:18:58
	 * @param @return 
	 * @throws
	 */
	@Override
	public boolean updateById(SysMenu sysMenu);

}
