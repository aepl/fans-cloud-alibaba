package com.fans.admin.quartz.job;

import com.fans.admin.quartz.entity.ScheduleJob;

public abstract class QuartzJob {

	public void execute(ScheduleJob job) {
		initService();
	}
	
	public abstract void initService() ;
}
