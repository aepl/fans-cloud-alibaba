package com.fans.admin.sys.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fans.admin.sys.dao.SysAttachMapper;
import com.fans.admin.sys.entity.SysAttach;
import com.fans.admin.sys.service.ISysAttachService;
import com.fans.common.utils.jwt.TokenUtil;

/**
 * @description: 系统_附件服务实现类
 * @date: 2019年12月27 10:31:34
 * @author: fanhaohao
 * @version: 1.0
 */
@Service
public class SysAttachServiceImpl extends ServiceImpl<SysAttachMapper,SysAttach> implements ISysAttachService {

	@Autowired
    private SysAttachMapper sysAttachMapper;
    
	/**
	 * @Title：list  
	 * @Description: 系统_附件分页查询
	 * @author: fanhaohao
	 * @date 2019年12月27 10:31:34
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	@Override
	public List<Map<String, Object>> list(Page<SysAttach> page,Map<String, Object> map){
		return sysAttachMapper.list(page,map);
	}
	
	/**
	 * @Title：deleteBatchByIdLogic  
	 * @Description: 系统_附件根据id逻辑删除
	 * @author: fanhaohao
	 * @date 2019年12月27 10:31:34
	 * @param @param id 
	 * @return void 
	 * @throws
	 */
	@Override
	public void deleteBatchByIdLogic(Object id){
		SysAttach sysAttach = new SysAttach();
		sysAttach.setId(String.valueOf(id));
		sysAttach.preUpdate();
		sysAttach.setUpdateBy(TokenUtil.getUserId());
		sysAttach.setDelFlag("1");
		this.updateById(sysAttach);
	}
	
	/**
	 * @Title：deleteBatchByIdsLogic    
	 * @Description: 系统_附件逻辑批量删除
	 * @author: fanhaohao
	 * @date 2019年12月27 10:31:34
	 * @param @param ids 
	 * @return void 
	 * @throws
	 */
	@Override
	public void deleteBatchByIdsLogic(List<Object> ids) {
		List<SysAttach> list = new ArrayList<>();
		for (Object id : ids) {
			SysAttach sysAttach = new SysAttach();
			sysAttach.setId(String.valueOf(id));
			sysAttach.preUpdate();
			sysAttach.setUpdateBy(TokenUtil.getUserId());
			sysAttach.setDelFlag("1");
			list.add(sysAttach);
		}
		this.updateBatchById(list);
	}
}
