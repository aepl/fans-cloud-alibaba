package com.fans.admin.sys.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.fans.admin.sys.entity.SysDictItem;
 
/**
 * @description: 字典服务类
 * @date: 2019年12月30 18:18:58
 * @author: fanhaohao
 * @version: 1.0
 */
public interface ISysDictItemService extends IService<SysDictItem> {

	/**
	 * @Title：list  
	 * @Description: 字典分页查询
	 * @author: fanhaohao
	 * @date 2019年12月30 18:18:58
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public List<Map<String, Object>> list(Page<SysDictItem> page,Map<String, Object> map);
	
	/**
	 * @Title：insert
	 * @Description: 字典添加
	 * @author: fanhaohao
	 * @date 2019年12月30 18:18:58
	 * @param @return 
	 * @throws
	 */
	@Override
	public  boolean insert(SysDictItem sysDictItem);
	
	/**
	 * @Title：updateById
	 * @Description: 字典更新
	 * @author: fanhaohao
	 * @date 2019年12月30 18:18:58
	 * @param @return 
	 * @throws
	 */
	@Override
	public  boolean updateById(SysDictItem sysDictItem);
	
	/**
	 * @Title：deleteBatchByIdLogic  
	 * @Description: 字典根据id逻辑删除
	 * @author: fanhaohao
	 * @date 2019年12月30 18:18:58
	 * @param @param id 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByIdLogic(Object id);
	
	/**
	 * @Title：deleteBatchByIdsLogic  
	 * @Description: 字典根据ids逻辑批量删除
	 * @author: fanhaohao
	 * @date 2019年12月30 18:18:58
	 * @param @param ids 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByIdsLogic(List<Object> ids);
}
