package com.fans.admin.interceptor;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.fans.admin.annotations.RequiredPermission;
import com.fans.common.constant.FansRspCon;
import com.fans.common.constant.SysConstant;
import com.fans.common.redis.util.JedisUtils;
import com.fans.common.utils.BlankUtils;
import com.fans.common.utils.FansResp;
import com.fans.common.utils.WriterUtil;
import com.fans.common.utils.jwt.TokenUtil;

/**
 * @ClassName: SecurityInterceptor
 * @Description: 按钮权限拦截
 * @author fanhaohao
 * @date 2020年4月15日 上午11:45:31
 */
public class RequiredPermissionInterceptor extends HandlerInterceptorAdapter {
 
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		// 验证权限
        if (this.hasPermission(handler)) {
            return true;
        }
		// 返回未授权信息
		WriterUtil.renderString(response, FansResp.error(FansRspCon.NO_PERMISSION.getCode(), FansRspCon.NO_PERMISSION.getMsg()));
        return false;
		
	}
	
	/**
     * 是否有权限
     *
     * @param handler
     * @return
     */
    private boolean hasPermission(Object handler) {
		boolean rtFlag = false;
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            // 获取方法上的注解
            RequiredPermission requiredPermission = handlerMethod.getMethod().getAnnotation(RequiredPermission.class);
            // 如果方法上的注解为空 则获取类的注解
            if (requiredPermission == null) {
                requiredPermission = handlerMethod.getMethod().getDeclaringClass().getAnnotation(RequiredPermission.class);
            }
            // 如果标记了注解，则判断权限
			if (BlankUtils.isNotBlank(requiredPermission) && BlankUtils.isNotBlank(requiredPermission.value())) {
				String permissionValue = requiredPermission.value();
				String key = SysConstant.PERMS_MENU_DATA + TokenUtil.getUserId();
				if (BlankUtils.isNotBlank(JedisUtils.getGlobalObject(key))) {
					Set<String> permsData = (HashSet<String>) JedisUtils.getGlobalObject(key);
					if (permissionValue.contains(",")) {
						String[] permissionValueArr = permissionValue.split(",");
						for (String pa : permissionValueArr) {
							if (permsData.contains(pa)) {
								rtFlag = true;
								break;
							}
						}
					} else {
						if (permsData.contains(permissionValue)) {
							rtFlag = true;
						}
					}
				}
			} else {
				rtFlag = true;
            }
		} else {
			rtFlag = true;
        }
		return rtFlag;
    }

}

