package com.fans.admin.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.fans.common.constant.LogType;

/**
 * @ClassName: Log
 * @Description: 系统日志记录
 * @author fanhaohao
 * @date 2019年12月29日 下午2:24:07
 */
@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Log {
	/**
	 * 操作类型,新增用户?删除用户 ?调用xx服务?使用接口?...
	 * 
	 * @return
	 */
	public String operation();

    /**
	 * 日志级别
	 * 
	 * @return
	 */
	public LogType level() default LogType.INFO;

}