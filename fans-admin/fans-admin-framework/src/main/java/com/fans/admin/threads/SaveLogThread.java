package com.fans.admin.threads;

import javax.servlet.http.HttpServletRequest;

import com.fans.admin.dao.LogDao;
import com.fans.admin.vo.LogVo;
import com.fans.common.utils.SpringUtil;
import com.fans.common.utils.StringUtils;

/**
 * @ClassName: SaveLogThread
 * @Description: 保存日志线程
 * @author fanhaohao
 * @date 2019年1月9日 上午11:35:02
 */
public class SaveLogThread extends Thread {
	private static LogDao logDao = SpringUtil.getBean(LogDao.class);

	private LogVo lv;

	private HttpServletRequest request;

	public SaveLogThread(LogVo lv, HttpServletRequest request) {
		super(SaveLogThread.class.getSimpleName());
		this.lv = lv;
		this.request = request;
	}

	@Override
	public void run() {
		lv.setId(StringUtils.genUUID());
		lv.preInsert();
		// 保存日志信息
		logDao.saveLog(lv);
	}
}
