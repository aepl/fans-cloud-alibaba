package com.fans.admin.dao;

import org.apache.ibatis.annotations.Insert;

import com.fans.admin.vo.LogVo;

/**
 * @ClassName: LogDao
 * @Description: 日志dao
 * @author fanhaohao
 * @date 2018年12月29日 下午2:57:16
 */
public interface LogDao {
    /**
     * 保存日志
     * @param joinPoint
     * @param methodName
     * @param operate
     */
	@Insert("insert into sys_log(id,operation,content,ip,method,type,create_by,create_date,update_by,update_date) values (#{id},#{operation},#{content},#{ip},#{method},#{type},#{createBy},#{createDate},#{updateBy},#{updateDate})")
	void saveLog(LogVo lv);
}
