package com.fans.admin.comfig;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.fans.admin.interceptor.RequiredPermissionInterceptor;

/**
 * 
 * @ClassName: MvcConfig
 * @Description: 权限拦截配置
 * @author fanhaohao
 * @date 2020年4月15日 下午2:29:18
 */
@Configuration
public class MvcConfig extends WebMvcConfigurerAdapter {
	@Bean
	public RequiredPermissionInterceptor requiredPermissionInterceptor() {
		return new RequiredPermissionInterceptor();
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(requiredPermissionInterceptor()).addPathPatterns("/**");
	}

}
